// ============================================================
// Project				-		CityMod
// Description			-		Handles all the Commands
// ============================================================
// Sections
//   1: Client Communication Commands
// 	 2: Player Commands
// 	 3: Administrational Commands
// 	 4: Testing Commands
// ============================================================

// ============================================================
// Section 1 - Client Communication Commands
// ============================================================

function servercmdCM_pingServer(%client, %version, %pass)
{
	if(%pass $= "c64WDkvQ")
		%client.CMVersion = %version;
}

function servercmdCM_craftRecipe(%client, %name)
{
	if($CM::Recipes["CRAFTING", %name] $= "")
		return commandtoclient(%client, 'CM_pushNotifyBox', "Error", "An Error Occured! Report the Error Code: CMD_cR_1");

	%recipe = $CM::Recipes["CRAFTING", %name];

	if(!%client.hasRecipeIngredients(%recipe))
		return commandtoclient(%client, 'CM_pushNotifyBox', "Crafting", "You don't have the necessary Ingredients to craft this Recipe!");

	%client.removeRecipeIngredients(%recipe);
	%client.addInventory(getField(%recipe, 1), 1, "crafted", true);

	commandtoclient(%client, 'CM_pushNotifyBox', "Crafting", "You have crafted the recipe," SPC %name);
}

function servercmdCM_enterStockMarket(%client, %groupname, %stockamount)
{
	if(resolveGroupID(%groupname) $= "-1")
		return commandtoclient(%client, 'CM_pushNotifyBox', "Error", "An Error Occured! Report the Error Code: CMD_eSM_1");

	%group = resolveGroupID(%groupname);

	if(CMGroupData.data[%group].value["Owner"] !$= %client.bl_id)
		return commandtoclient(%client, 'CM_pushNotifyBox', "Organizations", "You don't have sufficient privileges to manage this Organization!");

	if(CMStockMarket.isGroupInStockMarket(%group))
		return commandtoclient(%client, 'CM_pushNotifyBox', "Organizations", CMGroupData.data[%group].value["Title"] SPC "is already in the Stock Market!");

	if(!isInteger(%stockamount))
		return commandtoclient(%client, 'CM_pushNotifyBox', "Organizations", %stockamount SPC "is not a valid amount!");

	if(%stockamount > $CM::Prefs::Group::MaxStockAmount)
		return commandtoclient(%client, 'CM_pushNotifyBox', "Organizations", "You can't have that many Stocks! Maximum Stock Amount:" SPC $CM::Prefs::Group::MaxStockAmount);

	if(CMStockMarket.groupEnter(%group, %stockamount) == true)
		commandtoclient(%client, 'CM_pushNotifyBox', "Organizations", CMGroupData.data[%group].value["Title"] SPC "has successfully entered the Stock Market with" SPC %stockamount SPC "Stocks");
	else
		commandtoclient(%client, 'CM_pushNotifyBox', "Error", "An Error Occured! Report the Error Code: CMD_eSM_2");

	servercmdCM_manageGroup(%client, %groupname);
}

function servercmdCM_leaveStockMarket(%client, %groupname)
{
	if(resolveGroupID(%groupname) $= "-1")
		return commandtoclient(%client, 'CM_pushNotifyBox', "Error", "An Error Occured! Report the Error Code: CMD_lSM_1");

	%group = resolveGroupID(%groupname);

	if(CMGroupData.data[%group].value["Owner"] !$= %client.bl_id)
		return commandtoclient(%client, 'CM_pushNotifyBox', "Organizations", "You don't have sufficient privileges to manage this Organization!");

	if(CMStockMarket.isGroupInStockMarket(%group) == false)
		return commandtoclient(%client, 'CM_pushNotifyBox', "Organizations", CMGroupData.data[%group].value["Title"] SPC "is currently not in the Stock Market!");

	%payout = CMStockMarket.getAvailableStocks(%group) * CMStockMarket.getStockPrice(%group);

	if(CMStockMarket.groupExit(%group) == true)
		commandtoclient(%client, 'CM_pushNotifyBox', "Organizations", CMGroupData.data[%group].value["Title"] SPC "has successfully left the Stock Market, paying out $" @ %payout);
	else
		commandtoclient(%client, 'CM_pushNotifyBox', "Error", "An Error Occured! Report the Error Code: CMD_lSM_2");

	servercmdCM_manageGroup(%client, %groupname);
}

function servercmdCM_buyGroupStock(%client, %groupname, %amount)
{
	if(resolveGroupID(%groupname) $= "-1")
		return commandtoclient(%client, 'CM_pushNotifyBox', "Error", "An Error Occured! Report the Error Code: CMD_bGS_1");

	%group = resolveGroupID(%groupname);

	if(!isInteger(%amount))
		return commandtoclient(%client, 'CM_pushNotifyBox', "Organizations", %amount SPC "is not a valid amount!");

	if(CMStockMarket.getAvailableStocks(%group) <= 0)
		return commandtoclient(%client, 'CM_pushNotifyBox', "Organizations", CMGroupData.data[%group].value["Title"] SPC "does not have any stocks available for purchase!");

	if((CMStockMarket.getAvailableStocks(%group) - %amount) < 0)
		return commandtoclient(%client, 'CM_pushNotifyBox', "Organizations", CMGroupData.data[%group].value["Title"] SPC "does not have this many stocks available for purchase!");

	if(CMPlayerData.data[%client.bl_id].value["Bank"] == 0)
		return commandtoclient(%client, 'CM_pushNotifyBox', "Organizations", "You don't have any money in your Bank to buy stocks!");

	%cost = CMStockMarket.getStockPrice(%group) * %amount;

	if((CMPlayerData.data[%client.bl_id].value["Bank"] - %cost) < 0)
		return commandtoclient(%client, 'CM_pushNotifyBox', "Organizations", "You don't have this much money in your Bank needed to buy stocks!");

	if(CMStockMarket.addStock(%group, %client.bl_id, %amount) == true)
	{
		CMPlayerData.data[%client.bl_id].value["Bank"] -= %cost;
		CMGroupData.data[%group].value["Funds"] += %cost;

		commandtoclient(%client, 'CM_pushNotifyBox', "Organizations", "You have successfully bought" SPC %amount SPC "Stock in" SPC CMGroupData.data[%group].value["Title"] SPC "for $" @ %cost);
	}
	else
		commandtoclient(%client, 'CM_pushNotifyBox', "Error", "An Error Occured! Report the Error Code: CMD_bGS_2");

	servercmdCM_investInGroup(%client, %groupname);
}

function servercmdCM_sellGroupStock(%client, %groupname, %amount)
{
	if(resolveGroupID(%groupname) $= "-1")
		return commandtoclient(%client, 'CM_pushNotifyBox', "Error", "An Error Occured! Report the Error Code: CMD_sGS_1");

	%group = resolveGroupID(%groupname);

	if(!isInteger(%amount))
		return commandtoclient(%client, 'CM_pushNotifyBox', "Organizations", %amount SPC "is not a valid amount!");

	if(CMStockMarket.getUserStockAmount(%group, %client.bl_id) == 0)
		return commandtoclient(%client, 'CM_pushNotifyBox', "Organizations", "You don't have any stocks to sell!");

	if((CMStockMarket.getUserStockAmount(%group, %client.bl_id) - %amount) < 0)
		return commandtoclient(%client, 'CM_pushNotifyBox', "Organizations", "You don't have this many stocks to sell!");

	%value = CMStockMarket.getStockPrice(%group) * %amount;

	if(CMStockMarket.removeStock(%group, %client.bl_id, %amount) == true)
	{
		CMPlayerData.data[%client.bl_id].value["Bank"] += %value;
		CMGroupData.data[%group].value["Funds"] -= %value;

		commandtoclient(%client, 'CM_pushNotifyBox', "Organizations", "You have successfully sold" SPC %amount SPC "Stock in" SPC CMGroupData.data[%group].value["Title"] SPC "for $" @ %value);
	}
	else
		commandtoclient(%client, 'CM_pushNotifyBox', "Error", "An Error Occured! Report the Error Code: CMD_sGS_2");

	servercmdCM_investInGroup(%client, %groupname);
}

function servercmdCM_withdrawGroupFunds(%client, %groupname, %amount)
{
	if(resolveGroupID(%groupname) $= "-1")
		return commandtoclient(%client, 'CM_pushNotifyBox', "Error", "An Error Occured! Report the Error Code: CMD_wGF_1");

	%group = resolveGroupID(%groupname);

	if(CMGroupData.data[%group].value["Owner"] !$= %client.bl_id)
		return commandtoclient(%client, 'CM_pushNotifyBox', "Organizations", "You don't have sufficient privileges to manage this Organization!");

	if(!isInteger(%amount))
		return commandtoclient(%client, 'CM_pushNotifyBox', "Organizations", %amount SPC "is not a valid amount!");

	if(CMGroupData.data[%group].value["Funds"] == 0)
		return commandtoclient(%client, 'CM_pushNotifyBox', "Organizations", "The Organization does not have any money to withdraw!");

	if((CMGroupData.data[%group].value["Funds"] - %amount) < 0)
		return commandtoclient(%client, 'CM_pushNotifyBox', "Organizations", "The Organization does not have this much money to withdraw!");

	if(CMGroupData.data[%group].value["Funds"] == %amount)
	{
		for(%i = 0; %i < getFieldCount(CMGroupData.data[%group].value["Employed"]); %i++)
		{
			%subclient = getWord(getField(CMGroupData.data[%group].value["Employed"], %i), 0);

			if(isObject(findclientbyBL_ID(%subclient)))
				findclientbyBL_ID(%subclient).CMNotify(%groupname, "ALERT:" SPC %client.name SPC "has withdrawn all of" SPC CMGroupData.data[%group].value["Title"] @ "'s Funds!");
		}
	}

	CMGroupData.data[%group].value["Funds"] -= %amount;
	CMPlayerData.data[%client.bl_id].value["Bank"] += %amount;

	commandtoclient(%client, 'CM_pushNotifyBox', "Organizations", "You withdrew $" @ %amount SPC "from" SPC CMGroupData.data[%group].value["Title"] @ "'s Funds");
	servercmdCM_manageGroup(%client, %groupname);
}

function servercmdCM_depositGroupFunds(%client, %groupname, %amount)
{
	if(resolveGroupID(%groupname) $= "-1")
		return commandtoclient(%client, 'CM_pushNotifyBox', "Error", "An Error Occured! Report the Error Code: CMD_dGF_1");

	%group = resolveGroupID(%groupname);

	if(CMGroupData.data[%group].value["Owner"] !$= %client.bl_id)
		return commandtoclient(%client, 'CM_pushNotifyBox', "Organizations", "You don't have sufficient privileges to manage this Organization!");

	if(!isInteger(%amount))
		return commandtoclient(%client, 'CM_pushNotifyBox', "Organizations", %amount SPC "is not a valid amount!");

	if(CMPlayerData.data[%client.bl_id].value["Bank"] == 0)
		return commandtoclient(%client, 'CM_pushNotifyBox', "Organizations", "You don't have any money in your Bank to deposit into the Organization!");

	if((CMPlayerData.data[%client.bl_id].value["Bank"] - %amount) < 0)
		return commandtoclient(%client, 'CM_pushNotifyBox', "Organizations", "You don't have this much money in your Bank to deposit into the Organization!");

	CMGroupData.data[%group].value["Funds"] += %amount;
	CMPlayerData.data[%client.bl_id].value["Bank"] -= %amount;

	commandtoclient(%client, 'CM_pushNotifyBox', "Organizations", "You deposited $" @ %amount SPC "into" SPC CMGroupData.data[%group].value["Title"] @ "'s Funds");
	servercmdCM_manageGroup(%client, %groupname);
}

function servercmdCM_createGroupRank(%client, %groupname, %name, %salary)
{
	if(resolveGroupID(%groupname) $= "-1")
		return commandtoclient(%client, 'CM_pushNotifyBox', "Error", "An Error Occured! Report the Error Code: CMD_cGR_1");

	%group = resolveGroupID(%groupname);

	if(CMGroupData.data[%group].value["Owner"] !$= %client.bl_id)
		return commandtoclient(%client, 'CM_pushNotifyBox', "Organizations", "You don't have sufficient privileges to manage this Organization!");

	if(getFieldCount(CMGroupData.data[%group].value["Ranks"]) == $CM::Prefs::Group::MaxRanks)
		return commandtoclient(%client, 'CM_pushNotifyBox', "Organizations", "This Organization has reached the max amount of Ranks allowed (" @ $CM::Prefs::Group::MaxRanks @ ")");

	if(!isInteger(%salary))
		return commandtoclient(%client, 'CM_pushNotifyBox', "Organizations", %salary SPC "is not a valid amount!");

	if(%salary > $CM::Prefs::Group::MaxSalary)
		return commandtoclient(%client, 'CM_pushNotifyBox', "An Employee's salary cannot be greater than" SPC %salary);

	if(containsBlacklistedWords(%name))
		return commandtoclient(%client, 'CM_pushNotifyBox', "Organizations", "ERROR: findClientByName(" @ %client.name @ ").isMature = FALSE!");

	CMGroupData.data[%group].value["Ranks"] = setField(CMGroupData.data[%group].value["Ranks"], getFieldCount(CMGroupData.data[%group].value["Ranks"]), %name SPC %salary);

	commandtoclient(%client, 'CM_pushNotifyBox', "Organizations", "A new Rank has been successfully created" NL "Name:" SPC %name NL "Salary:" SPC "$" @ %salary);
	commandtoclient(%client, 'CM_clearGroupManagementRanks');

	for(%i = 0; %i < getFieldCount(CMGroupData.data[%group].value["Ranks"]); %i++)
	{
		%found = getField(CMGroupData.data[%group].value["Ranks"], %i);
		%rank = getWord(%found, 0);
		%salary = getWord(%found, 1);
		commandtoclient(%client, 'CM_setGroupManagementRank', %i, %rank, %salary);
	}
}

function servercmdCM_editGroupRank(%client, %groupname, %rankpos, %name, %salary)
{
	if(resolveGroupID(%groupname) $= "-1")
		return commandtoclient(%client, 'CM_pushNotifyBox', "Error", "An Error Occured! Report the Error Code: CMD_eGR_1");

	%group = resolveGroupID(%groupname);

	if(CMGroupData.data[%group].value["Owner"] !$= %client.bl_id)
		return commandtoclient(%client, 'CM_pushNotifyBox', "Organizations", "You don't have sufficient privileges to manage this Organization!");

	if(!isInteger(%salary))
		return commandtoclient(%client, 'CM_pushNotifyBox', "Organizations", %salary SPC "is not a valid amount!");

	if(%salary > $CM::Prefs::Group::MaxSalary)
		return commandtoclient(%client, 'CM_pushNotifyBox', "An Employee's salary cannot be greater than" SPC %salary);

	if(containsBlacklistedWords(%name))
		return commandtoclient(%client, 'CM_pushNotifyBox', "Organizations", "ERROR: findClientByName(" @ %client.name @ ").isMature = FALSE!");

	CMGroupData.data[%group].value["Ranks"] = setField(CMGroupData.data[%group].value["Ranks"], %rankpos, %name SPC %salary);

	commandtoclient(%client, 'CM_pushNotifyBox', "Organizations", "The Rank has been successfully changed" NL "Name:" SPC %name NL "Salary:" SPC "$" @ %salary);
	commandtoclient(%client, 'CM_clearGroupManagementRanks');

	for(%i = 0; %i < getFieldCount(CMGroupData.data[%group].value["Ranks"]); %i++)
	{
		%found = getField(CMGroupData.data[%group].value["Ranks"], %i);
		%rank = getWord(%found, 0);
		%salary = getWord(%found, 1);
		commandtoclient(%client, 'CM_setGroupManagementRank', %i, %rank, %salary);
	}
}

function servercmdCM_deleteGroupRank(%client, %groupname, %rankpos)
{
	if(resolveGroupID(%groupname) $= "-1")
		return commandtoclient(%client, 'CM_pushNotifyBox', "Error", "An Error Occured! Report the Error Code: CMD_eGR_1");

	%group = resolveGroupID(%groupname);

	if(CMGroupData.data[%group].value["Owner"] !$= %client.bl_id)
		return commandtoclient(%client, 'CM_pushNotifyBox', "Organizations", "You don't have sufficient privileges to manage this Organization!");

	for(%i = 0; %i < getFieldCount(CMGroupData.data[%group].value["Employed"]); %i++)
	{
		%found = getField(CMGroupData.data[%group].value["Employed"], %i);
		if(getWord(%found, 1) $= %rankpos)
		{
			CMGroupData.data[%group].value["Employed"] = setField(CMGroupData.data[%group].value["Employed"], %i, %victim SPC %rank);
			break;
		}
	}

	if(containsBlacklistedWords(%name))
		return commandtoclient(%client, 'CM_pushNotifyBox', "Organizations", "ERROR: findClientByName(" @ %client.name @ ").isMature = FALSE!");

	CMGroupData.data[%group].value["Ranks"] = setField(CMGroupData.data[%group].value["Ranks"], %rankpos, %name SPC %salary);

	commandtoclient(%client, 'CM_pushNotifyBox', "Organizations", "The Rank has been successfully changed" NL "Name:" SPC %name NL "Salary:" SPC "$" @ %salary);
	commandtoclient(%client, 'CM_clearGroupManagementRanks');

	for(%i = 0; %i < getFieldCount(CMGroupData.data[%group].value["Ranks"]); %i++)
	{
		%found = getField(CMGroupData.data[%group].value["Ranks"], %i);
		%rank = getWord(%found, 0);
		%salary = getWord(%found, 1);
		commandtoclient(%client, 'CM_setGroupManagementRank', %i, %rank, %salary);
	}
}

function servercmdCM_createGroup(%client, %name, %funds, %starterrank_name, %starterrank_salary)
{
	%id = 1;
	while(CMGroupData.data[%id].value["Title"] !$= "")
	{
		if(%client.bl_id == CMGroupData.data[%id].value["Owner"])
			%owner++;

		if(strLwr(CMGroupData.data[%id].value["Title"]) $= strLwr(%name))
			%taken = true;

		%id++;
	}

	if(%owner >= $CM::Prefs::Group::MaxGroups)
		return commandtoclient(%client, 'CM_pushNotifyBox', "Organizations", "You already own the max amount of Organizations allowed!");

	if(%taken)
		return commandtoclient(%client, 'CM_pushNotifyBox', "Organizations", "This Organization name has already been taken");

	if(containsBlacklistedWords(%name) || containsBlacklistedWords(%starterrank_name))
		return commandtoclient(%client, 'CM_pushNotifyBox', "Organizations", "ERROR: findClientByName(" @ %client.name @ ").isMature = FALSE!");

	if(strLen(%name) > 20)
		return commandtoclient(%client, 'CM_pushNotifyBox', "Organizations", "The Organization's name can't be longer than 20 characters");

	if(!isInteger(%funds))
		return commandtoclient(%client, 'CM_pushNotifyBox', "Organizations", %funds SPC "is not a valid amount!");

	if(!isInteger(%starterrank_salary))
		return commandtoclient(%client, 'CM_pushNotifyBox', "Organizations", %starterrank_salary SPC "is not a valid amount!");

	if(%funds < 1000)
		return commandtoclient(%client, 'CM_pushNotifyBox', "Organizations", "This minimum amount of funds this Organization can start with is $1,000!");

	if((CMPlayerData.data[%client.bl_id].value["Bank"] - %funds) < 0)
		return commandtoclient(%client, 'CM_pushNotifyBox', "Organizations", "You do not have enough money in your Bank to fund this Organization!");

	%group = %id++;
	CMGroupData.addData(%group);

	CMGroupData.data[%group].value["Title"] = %name;
	CMGroupData.data[%group].value["Owner"] = %client.bl_id;
	CMGroupData.data[%group].value["Funds"] = %funds;
	CMGroupData.data[%group].value["Ranks"] = setField(CMGroupData.data[%group].value["Ranks"], 0, %starterrank_name SPC %starterrank_salary);

	CMPlayerData.data[%client.bl_id].value["Bank"] -= %funds;

	commandtoclient(%client, 'CM_pushNotifyBox', "Organizations", "You created the Organization," SPC %name @ "!");
	commandtoclient(%client, 'CM_refreshGroupsList');
}

function servercmdCM_changeGroupMain(%client, %groupname, %title, %owner)
{
	if(resolveGroupID(%groupname) $= "-1")
		return commandtoclient(%client, 'CM_pushNotifyBox', "Error", "An Error Occured! Report the Error Code: CMD_cGM_1");

	%group = resolveGroupID(%groupname);

	if(CMGroupData.data[%group].value["Owner"] !$= %client.bl_id)
		return commandtoclient(%client, 'CM_pushNotifyBox', "Organizations", "You don't have sufficient privileges to manage this Organization!");

	if((strLwr(%title) $= strLwr(CMGroupData.data[%group].value["Title"])) && (%owner == CMGroupData.data[%group].value["Owner"]))
		return commandtoclient(%client, 'CM_pushNotifyBox', "Organizations", "You didn't make any changes!");

	if(strLwr(%title) !$= strLwr(CMGroupData.data[%group].value["Title"]))
	{
		if(containsBlacklistedWords(%title))
			return commandtoclient(%client, 'CM_pushNotifyBox', "Organizations", "ERROR: findClientByName(" @ %client.name @ ").isMature = FALSE!");

		if(strLen(%title) > 20)
			return commandtoclient(%client, 'CM_pushNotifyBox', "Organizations", "The Organization's name can't be longer than 20 characters");

		%oldname = CMGroupData.data[%group].value["Title"];
		CMGroupData.data[%group].value["Title"] = %title;

		for(%i = 0; %i < getFieldCount(CMGroupData.data[%group].value["Employed"]); %i++)
		{
			%subclient = getWord(getField(CMGroupData.data[%group].value["Employed"], %i), 0);

			if(isObject(findclientbyBL_ID(%subclient)))
				findclientbyBL_ID(%subclient).CMNotify(%groupname, %oldname SPC "is now known as" SPC CMGroupData.data[%group].value["Title"]);
		}

		commandtoclient(%client, 'CM_closeGroupManagement');
		commandtoclient(%client, 'CM_closeGUI', "groups");
		commandtoclient(%client, 'CM_pushNotifyBox', "Organizations", %oldname SPC "is now known as" SPC CMGroupData.data[%group].value["Title"]);
	}

	if(strLwr(%owner) !$= strLwr(CMGroupData.data[%group].value["Owner"]))
	{
		if(!hasDataFile(%owner))
			return commandtoclient(%client, 'CM_pushNotifyBox', "Organizations", "The BL_ID" SPC %owner SPC "does not exist!");

		if(CMGroupData.data[%group].value["Owner"] == %client.bl_id)
			return commandtoclient(%client, 'CM_pushNotifyBox', "Organizations", "You can't transfer ownership of this Organization to yourself!");

		if(isObject(findclientbyBL_ID(%victim)))
		{
			commandtoclient(findclientbyBL_ID(%victim), 'CM_pushNotifyBox', "Organizations", "You are now the owner of" SPC CMGroupData.data[%group].value["Title"] @ "!");
			%shownname = findclientbyBL_ID(%victim).name;
		}
		else
			%shownname = "BL_ID" SPC %victim;

		commandtoclient(%client, 'CM_pushNotifyBox', "Organizations", "You transfered ownership of" SPC CMGroupData.data[%group].value["Title"] SPC "to" SPC %shownname);
		commandtoclient(%client, 'CM_closeGroupManagement');
	}
}

function servercmdCM_changeGroupRank(%client, %groupname, %victim, %rank)
{
	if(resolveGroupID(%groupname) $= "-1")
		return commandtoclient(%client, 'CM_pushNotifyBox', "Error", "An Error Occured! Report the Error Code: CMD_cGR_1");

	%group = resolveGroupID(%groupname);

	if(CMGroupData.data[%group].value["Owner"] !$= %client.bl_id)
		return commandtoclient(%client, 'CM_pushNotifyBox', "Organizations", "You don't have sufficient privileges to manage this Organization!");

	if(!in(CMGroupData.data[%group].value["Employed"], %victim))
		return commandtoclient(%client, 'CM_pushNotifyBox', "Organizations", "The BL_ID" SPC %victim SPC "is not in this Organization!");

	if(CMGroupData.data[%group].value["Owner"] == %client.bl_id)
		return commandtoclient(%client, 'CM_pushNotifyBox', "Organizations", "You're the Owner!  You can't change your own rank.");

	for(%i = 0; %i < getFieldCount(CMGroupData.data[%group].value["Employed"]); %i++)
	{
		%found = getField(CMGroupData.data[%group].value["Employed"], %i);
		if(getWord(%found, 0) == %victim)
		{
			CMGroupData.data[%group].value["Employed"] = setField(CMGroupData.data[%group].value["Employed"], %i, %victim SPC %rank);
			break;
		}
	}

	%rankname = getWord(getField(CMGroupData.data[%group].value["Ranks"], %rank), 0);

	if(isObject(findclientbyBL_ID(%victim)))
	{
		findclientbyBL_ID(%victim).CMNotify(%groupname, "Your Rank in" SPC CMGroupData.data[%group].value["Title"] SPC "has been changed to" SPC %rankname);
		%shownname = findclientbyBL_ID(%victim).name;
	}
	else
		%shownname = "BL_ID" SPC %victim;

	commandtoclient(%client, 'CM_pushNotifyBox', "Organizations", "You changed" SPC %shownname @ "'s Rank to" SPC %rankname);
}

function servercmdCM_investInGroup(%client, %groupname)
{
	if(resolveGroupID(%groupname) $= "-1")
		return commandtoclient(%client, 'CM_pushNotifyBox', "Error", "An Error Occured! Report the Error Code: CMD_iIG_1");

	%group = resolveGroupID(%groupname);

	if(CMStockMarket.isGroupInStockMarket(%group) == false)
		return commandtoclient(%client, 'CM_pushNotifyBox', "Organizations", CMGroupData.data[%group].value["Title"] SPC "is currently not in the Stock Market!");

	commandtoclient(%client, 'CM_setInvestmentInfo', CMGroupData.data[%group].value["Title"], CMStockMarket.getUserStockAmount(%group, %client.bl_id), CMStockMarket.getStockPrice(%group));
}

function servercmdCM_manageGroup(%client, %groupname)
{
	if(resolveGroupID(%groupname) $= "-1")
		return commandtoclient(%client, 'CM_pushNotifyBox', "Error", "An Error Occured! Report the Error Code: CMD_mG_1");

	%group = resolveGroupID(%groupname);

	if(CMGroupData.data[%group].value["Owner"] !$= %client.bl_id)
		return commandtoclient(%client, 'CM_pushNotifyBox', "Organizations", "You don't have sufficient privileges to manage this Organization!");

	commandtoclient(%client, 'CM_setGroupManagementInfo', CMGroupData.data[%group].value["Title"], CMGroupData.data[%group].value["Owner"], CMGroupData.data[%group].value["Funds"]);

	for(%i = 0; %i < getFieldCount(CMGroupData.data[%group].value["Ranks"]); %i++)
	{
		%found = getField(CMGroupData.data[%group].value["Ranks"], %i);
		%rank = getWord(%found, 0);
		%salary = getWord(%found, 1);
		commandtoclient(%client, 'CM_setGroupManagementRank', %i, %rank, %salary);
	}

	%stocksleft = 0;
	%shareholders = 0;
	%stockprice = 0;

	if(CMStockMarket.isGroupInStockMarket(%group))
	{
		%stocksleft = CMStockMarket.getAvailableStocks(%group);
		%shareholders = getWordCount(CMStockMarket.getStockOwners(%group));
		%stockprice = CMStockMarket.getStockPrice(%group);
	}

	commandtoclient(%client, 'CM_setGroupStockInfo', %stocksleft, %shareholders, %stockprice);
}

function servercmdCM_kickFromGroup(%client, %groupname, %victimname)
{
	if(resolveGroupID(%groupname) $= "-1")
		return commandtoclient(%client, 'CM_pushNotifyBox', "Error", "An Error Occured! Report the Error Code: CMD_kFG_1");

	%group = resolveGroupID(%groupname);
	%victim = resolvePlayerID(%victimname);

	if(CMGroupData.data[%group].value["Owner"] !$= %client.bl_id)
		return commandtoclient(%client, 'CM_pushNotifyBox', "Organizations", "You don't have sufficient privileges to kick a player from this Organization!");

	if(!in(CMGroupData.data[%group].value["Employed"], %victim))
	{
		if(trim(CMPlayerData.Data[CMGroupData.data[%group].value["Owner"]].Value["Name"]) $= trim(%victimname))
			return commandtoclient(%client, 'CM_pushNotifyBox', "Organizations", "You can't kick the owner of the Organization!");
		else
			return commandtoclient(%client, 'CM_pushNotifyBox', "Organizations", "There is no-one by that BL_ID in this Organization!");
	}

	if(%victim == %client.bl_id)
		return commandtoclient(%client, 'CM_pushNotifyBox', "Organizations", "You can't kick yourself!");

	for(%i = 0; %i < getFieldCount(CMGroupData.data[%group].value["Employed"]); %i++)
	{
		%found = getField(CMGroupData.data[%group].value["Employed"], %i);
		if(getWord(%found, 0) == %victim)
		{
			%rank = getWord(%found, 1);
			break;
		}
	}

	CMGroupData.data[%group].value["Employed"] = removeField(CMGroupData.data[%group].value["Employed"], getIndex(CMGroupData.data[%group].value["Employed"], %victim SPC %rank, "field"));

	if(isObject(findclientbyBL_ID(%victim)))
	{
		findclientbyBL_ID(%victim).CMNotify(%groupname, "You have been kicked from" SPC %groupname @ "!");
		%shownname = findclientbyBL_ID(%victim).name;
	}
	else
		%shownname = "BL_ID" SPC %victim;

	commandtoclient(%client, 'CM_pushNotifyBox', "Organizations", "You kicked" SPC %shownname @ "from the Organization");
}

function servercmdCM_joinGroup(%client, %groupname)
{
	if(resolveGroupID(%groupname) $= "-1")
		return commandtoclient(%client, 'CM_pushNotifyBox', "Error", "An Error Occured! Report the Error Code: CMD_jG_1");

	%group = resolveGroupID(%groupname);

	if(%client.inGroup(%group))
		return commandtoclient(%client, 'CM_pushNotifyBox', "Organizations", "You're already in" SPC %groupname @ "!");

	if(%client.getGroupMembership() !$= "-1")
		return commandtoclient(%client, 'CM_pushNotifyBox', "Organizations", "You're already a part of another group!");

	if(%client.bl_id == CMGroupData.data[%group].value["Owner"])
		return commandtoclient(%client, 'CM_pushNotifyBox', "Organizations", "You're already the owner of" SPC %groupname @ "!");

	if(getWordCount(CMGroupData.data[%group].value["Employed"]) == $CM::Prefs::Group::MaxEmployees)
		return commandtoclient(%client, 'CM_pushNotifyBox', "Organizations", %groupname SPC "already has the max amount of users allowed!");

	for(%i = 0; %i < getFieldCount(CMGroupData.data[%group].value["Employed"]); %i++)
	{
		%subclient = getWord(getField(CMGroupData.data[%group].value["Employed"], %i), 0);

		if(isObject(findclientbyBL_ID(%subclient)))
			findclientbyBL_ID(%subclient).CMNotify(%groupname, %client.name SPC "joined" SPC %groupname);
	}

	CMGroupData.data[%group].value["Employed"] = setField(CMGroupData.data[%group].value["Employed"], getFieldCount(CMGroupData.data[%group].value["Employed"]), %client.bl_id SPC "0");
	commandtoclient(%client, 'CM_pushNotifyBox', "Organizations", "You joined" SPC %groupname @ ".");
}

function servercmdCM_leaveGroup(%client, %groupname)
{
	if(resolveGroupID(%groupname) $= "-1")
		return commandtoclient(%client, 'CM_pushNotifyBox', "Error", "An Error Occured! Report the Error Code: CMD_lG_1");

	%group = resolveGroupID(%groupname);

	if(CMGroupData.data[%group].value["Owner"] == %client.bl_id)
		return commandtoclient(%client, 'CM_pushNotifyBox', "Organizations", "You can't leave your own Organization!" NL "If you really want to disband the Organization, click 'Disband' under Manage.");

	if(!%client.inGroup(%group))
		return commandtoclient(%client, 'CM_pushNotifyBox', "Organizations", "You're not in" SPC %groupname @ "!");

	CMGroupData.data[%group].value["Employed"] = removeField(CMGroupData.data[%group].value["Employed"], getIndex(CMGroupData.data[%group].value["Employed"], %client.bl_id SPC %client.getGroupRank(%group), "field"));
	talk(CMGroupData.data[%group].value["Employed"]);
	talk(%client.bl_id SPC %client.getGroupRank(%group));

	for(%i = 0; %i < getFieldCount(CMGroupData.data[%group].value["Employed"]); %i++)
	{
		%subclient = getWord(getField(CMGroupData.data[%group].value["Employed"], %i), 0);

		if(isObject(findclientbyBL_ID(%subclient)))
			findclientbyBL_ID(%subclient).CMNotify(%groupname, %client.name SPC "left" SPC %groupname);
	}

	commandtoclient(%client, 'CM_pushNotifyBox', "Organizations", "You left" SPC %groupname @ ".");
}

function servercmdCM_dropInventory(%client, %slot)
{
	if(!isObject(%client.player))
		return commandtoclient(%client, 'CM_pushNotifyBox', "Error", "An Error Occured! Report the Error Code: CMD_dI_1");

	%client.removeInventorySlot(%slot, "dropped from your Inventory", true);
	%client.updateInventory();
}

function servercmdCM_depositBank(%client, %amount)
{
	if(!isObject(%client.player))
		return commandtoclient(%client, 'CM_pushNotifyBox', "Error", "An Error Occured! Report the Error Code: CMD_dB_1");

	InitContainerRadiusSearch(%client.player.getPosition(), 6, $TypeMasks::FxBrickObjectType);
	while(%brick = containerSearchNext())
	{
		if(isObject(%brick) && %brick.getDataBlock().CMBrick && (%brick.getDatablock().CMBrickType $= "INTERACTIVE") && (strLwr(%brick.getName()) $= "_bank"))
		{
			%inRange = true;
			break;
		}
	}

	if(%amount $= "-1")
		%amount = CMPlayerData.data[%client.bl_id].value["Money"];

	if(!isInteger(%amount))
		return commandtoclient(%client, 'CM_pushNotifyBox', "Bank", %amount SPC "is not a valid amount!");

	if(!%inRange)
		return commandtoclient(%client, 'CM_pushNotifyBox', "Bank", "You're not within range of a Bank ATM");

	if(CMPlayerData.data[%client.bl_id].value["Money"] == 0)
		return commandtoclient(%client, 'CM_pushNotifyBox', "Bank", "You don't have any money in your Wallet to deposit!");

	if((CMPlayerData.data[%client.bl_id].value["Money"] - %amount) < 0)
		return commandtoclient(%client, 'CM_pushNotifyBox', "Bank", "You don't have this much money in your Wallet to deposit!");

	CMPlayerData.data[%client.bl_id].value["Money"] -= %amount;
	CMPlayerData.data[%client.bl_id].value["Bank"] += %amount;

	commandtoclient(%client, 'CM_pushNotifyBox', "Bank", "You deposited $" @ %amount SPC "into the Bank");
	serverCmdCM_getBankStats(%client);
	%client.UpdateHUD();
}

function servercmdCM_withdrawBank(%client, %amount)
{
	if(!isObject(%client.player))
		return commandtoclient(%client, 'CM_pushNotifyBox', "Error", "An Error Occured! Report the Error Code: CMD_dB_1");

	InitContainerRadiusSearch(%client.player.getPosition(), 6, $TypeMasks::FxBrickObjectType);
	while(%brick = containerSearchNext())
	{
		if(isObject(%brick) && %brick.getDataBlock().CMBrick && (%brick.getDatablock().CMBrickType $= "INTERACTIVE") && (strLwr(%brick.getName()) $= "_bank"))
		{
			%inRange = true;
			break;
		}
	}

	if(%amount $= "-1")
		%amount = CMPlayerData.data[%client.bl_id].value["Bank"];

	if(!isInteger(%amount))
		return commandtoclient(%client, 'CM_pushNotifyBox', "Bank", %amount SPC "is not a valid amount!");

	if(!%inRange)
		return commandtoclient(%client, 'CM_pushNotifyBox', "Bank", "You're not within range of a Bank ATM");

	if(CMPlayerData.data[%client.bl_id].value["Bank"] == 0)
		return commandtoclient(%client, 'CM_pushNotifyBox', "Bank", "You don't have any money in your Bank to withdraw!");

	if((CMPlayerData.data[%client.bl_id].value["Bank"] - %amount) < 0)
		return commandtoclient(%client, 'CM_pushNotifyBox', "Bank", "You don't have this much money in the Bank to withdraw!");

	CMPlayerData.data[%client.bl_id].value["Money"] += %amount;
	CMPlayerData.data[%client.bl_id].value["Bank"] -= %amount;

	commandtoclient(%client, 'CM_pushNotifyBox', "Bank", "You withdrew $" @ %amount SPC "from the Bank");
	serverCmdCM_getBankStats(%client);
	%client.UpdateHUD();
}

function servercmdCM_joinElection(%client)
{
	if(!isObject(CMElectionSO) || CMElectionSO.stage != 1)
		return commandtoclient(%client, 'CM_pushNotifyBox', "Error", "An Error Occured! Report the Error Code: CMD_jE_1");

	if(%client.isBlack())
		return commandToClient(%client, 'CM_pushNotifyBox', "Error", "u is black. pls leave.");

	if(CMPlayerData.data[%client.bl_id].value["Bank"] < CMElectionSO.lowestBid)
		return commandtoclient(%client, 'CM_pushNotifyBox', "Secondary Election", "You don't have enough money to join!" NL "You need atleast $" @ CMElectionSO.lowestBid);

	%canidates = 0;
	for(%i = 0; %i < 7; %i++)
	{
		if(CMElectionSO.scanidate[%i] == -1)
			continue;

		%canidates++;
	}

	if(%canidates == 7)
		return commandtoclient(%client, 'CM_pushNotifyBox', "Secondary Election", "Sorry, the number of available candiditial slots is currently full!");

	CMPlayerData.data[%client.bl_id].value["Bank"] -= CMElectionSO.lowestBid;
	CMElectionSO.lowestBid += 100;

	%nextcanidate = %canidates++;
	CMElectionSO.bid[%nextcanidate] = CMElectionSO.lowestBid - 100;
	CMElectionSO.scanidate[%nextcanidate] = %client.bl_id;

	commandtoclient(%client, 'CM_pushNotifyBox', "Secondary Election", "You are now a canidate in the Secondary Election!");
}

function serverCmdCM_castSecodaryVote(%client, %option)
{
	if(!isObject(CMElectionSO) || CMElectionSO.stage != 1 || %option > 7 || %option < 1)
		return commandtoclient(%client, 'CM_pushNotifyBox', "Error", "An Error Occured! Report the Error Code: CMD_cSV_1");

	if(%client.voted || in(CMElectionSO.voters, %client.bl_id))
	{
		%client.voted = true;
		return commandtoclient(%client, 'CM_pushNotifyBox', "Voting", "You already voted in this election!");
	}

	%client.voted = true;
	CMElectionSO.voters = ltrim(CMElectionSO.voters SPC %client.bl_id);

	if(!%client.isBlack())
		CMElectionSO.svotes[%option]++;

	commandtoclient(%client, 'CM_pushNotifyBox', "Voting", "Thank you for voting!");
}

function serverCmdCM_castPrimaryVote(%client,%option)
{
	if(!isObject(CMElectionSO) || CMElectionSO.stage != 1 || (%option != 1 && %option != 2))
		return commandtoclient(%client, 'CM_pushNotifyBox', "Error", "An Error Occured! Report the Error Code: CMD_cPV_1");

	if(%client.voted || in(CMElectionSO.voters, %client.bl_id))
	{
		%client.voted = true;
		return commandtoclient(%client, 'CM_pushNotifyBox', "Voting", "You already voted in this election!");
	}
	
	%client.voted = true;
	CMElectionSO.voters = ltrim(CMElectionSO.voters SPC %client.bl_id);

	if(!%client.isBlack())
		CMElectionSO.pvotes[%option]++;

	commandtoclient(%client, 'CM_pushNotifyBox', "Voting", "Thank you for voting!");
}

function servercmdCM_reportEvidence(%client, %crime, %id)
{
	if(!isObject(%crime) || !isObject(%client.player))
		return commandtoclient(%client, 'CM_pushNotifyBox', "Error", "An Error Occured! Report the Error Code: CMD_rE_1");

	if(%client.player.evidence $= "")
		return commandtoclient(%client, 'CM_pushNotifyBox', "Police Station", "You don't have any evidence to report!");

	if(getField(%client.player.evidence, 0) !$= %crime.getID())
		return commandtoclient(%client, 'CM_pushNotifyBox', "Police Station", "You don't have any evidence on this case!");

	if(%crime.investigator $= "")
		return commandtoclient(%client, 'CM_pushNotifyBox', "Police Station", "You can't report evidence to a case that does not have an investigator!");

	%evidence_amt = 0;
	for(%i = 0; %i < getWordCount(getField(%client.player.evidence, 1)); %i++)
	{
		%evidence = getWord(getField(%client.player.evidence, 1), i);
		%crime.addEvidence(%evidence);
	}

	%convicting = (%crime.isSolved ? "convicting" : "");

	commandtoclient(%client, 'CM_pushNotifyBox', "Police Station", 
										"You reported" SPC getWordCount(getField(%client.player.evidence, 1)) SPC "pieces of" SPC %convicting SPC "evidence." NL
										(%crime.isSolved ? "This case, Case #" @ %id @ ", is now ready to be solved." : "")
									);

	%client.player.evidence = "";

	commandtoclient(%client, 'CM_clearCasesList');
	servercmdCM_requestCases(%client);
}

function servercmdCM_claimCase(%client, %crime, %id)
{
	if(!isObject(%crime) || !isObject(%client.player))
		return commandtoclient(%client, 'CM_pushNotifyBox', "Error", "An Error Occured! Report the Error Code: CMD_cC_1");

	if(%crime.investigator !$= "")
		return commandtoclient(%client, 'CM_pushNotifyBox', "Police Station", "This case already has an active investigator!");

	if(%client.getSkill("LAW") < 3)
		return commandtoclient(%client, 'CM_pushNotifyBox', "Police Station", "You are not of the sufficient 'Law Skill' level needed to become the investigator of this case.");

	%crime.investigator = %client.bl_id;
	%crime.investigator_name = %client.name;

	commandtoclient(%client, 'CM_clearCasesList');
	servercmdCM_requestCases(%client);
}

function servercmdCM_solveCase(%client, %crime, %id)
{
	if(!isObject(%crime) || !isObject(%client.player))
		return commandtoclient(%client, 'CM_pushNotifyBox', "Error", "An Error Occured! Report the Error Code: CMD_sC_1");

	if(%crime.investigator !$= "")
		return commandtoclient(%client, 'CM_pushNotifyBox', "Police Station", "This case doesn't have an investigator, who is needed to solve it!");

	if(%client.bl_id != %crime.investigator)
		return commandtoclient(%client, 'CM_pushNotifyBox', "Police Station", "You are not the investigator of this case!");

	if(!%crime.isSolved)
		return commandtoclient(%client, 'CM_pushNotifyBox', "Police Station", "This case is not ready to be solved!");

	commandtoclient(%client, 'CM_pushNotifyBox', "Police Station", "The investigation is now over." NL %crime.initiator_name SPC "(BL_ID" SPC %crime.initiator @ ") has been found to be the perpetrator!");

	if(isObject(findclientbyBL_ID(%crime.initiator)))
		findclientbyBL_ID(%crime.initiator).CMNotify("Investigation", "You were convicted of a crime," SPC %crime.type @ "!");

	CMPlayerData.Data[%crime.initiator].Value["Charges"] = ltrim(CMPlayerData.Data[%crime.initiator].Value["Charges"] SPC strUpr(%crime.type));
	%crime.delete();

	commandtoclient(%client, 'CM_clearCasesList');
	servercmdCM_requestCases(%client);
	servercmdCM_requestCriminals(%client);
}

function servercmdCM_executiveDecision(%client, %info)
{
	if(!isObject(%client) || (%info !$= "executiveDecision1"))
		return commandtoclient(%client, 'CM_pushNotifyBox', "Error", "An Error Occured! Report the Error Code: CMD_eD_1");

	%decisionsloop = new fileObject();
	%decisionsloop.openForRead("Add-Ons/Gamemode_CityMod/assets/shapes/Empty.dts");
	
	while(!%decisionsloop.isEOF())
	{
		%decision = %decision SPC %decisionsloop.readLine();
		talk(%decision);
	}

	eval(%decision @ "%decisionsuccess = true;");
	%decisionsloop.close();
	%decisionsloop.delete();

	if(%decisionsuccess == true)
		commandtoclient(%client, 'CM_pushNotifyBox', "Executive Decision", "Executive Decision" NL %decision NL "has been successfully carried out!");
	else
		commandtoclient(%client, 'CM_pushNotifyBox', "Error", "An Error Occured! Report the Error Code: CMD_eD_2");
}

function servercmdCM_governmentDecision(%client, %key, %value)
{
	if(!%client.isMayor())
		return;

	if((CMCityData.def[%key] $= "") || !CMCityData.changable[%key])
		return commandtoclient(%client, 'CM_pushNotifyBox', "Error", "An Error Occured! Report the Error Code: CMD_gD_1");

	if(CMCityData.min[%key] <= %value)
		return commandToClient(%client, 'CMNotify', "Government", "That is less than minimum, which is" SPC CMCityData.min[%key]);

	if(CMCityData.max[%key] >= %value)
		return commandToClient(%client, 'CMNotify', "Government", "That is more than max, which is" SPC CMCityData.max[%key]);

	governmentDecision(%key, %value);
}

function servercmdCM_Checkout(%client)
{
	%brick = %client.player.currentCashRegister.getID();

	if(!isObject(%brick) || !%brick.getDatablock().isCashRegister)
		return commandtoclient(%client, 'CM_pushNotifyBox', "Error", "An Error Occured! Report the Error Code: CMD_Ch_1");

	%cost = 0;

	//Accumulate Food Quantities
	for(%a = 1; %a <= 6; %a++)
	{
		if(%client.getInventory(%a) $= "NONE 0")
			continue;

		%foodtype = parseTokenString(firstWord(parseTokenString(getWord(CMPlayerData.data[%client.bl_id].Value["Inventory"], %a - 1))), ".");

		if(strUpr(getWord(%foodtype, 1)) !$= "FOOD")
			continue;

		%foodcart = lTrim(%foodcart SPC getWord(%foodtype, 0));

		%cost += getWord(%brick.foodPricing, resolveFoodIDFromName(getWord(%foodtype, 0)) - 1);
		%client.removeInventorySlot(%a, "removed", false, true);
	}

	%tax = mFloor(%cost * (CMCityData.Data["TaxRate"] / 100));
	%playercost = %cost + %tax;

	if(CMPlayerData.Data[%client.bl_id].Value["Money"] - %playercost < 0)
		return commandtoclient(%client, 'CM_pushNotifyBox', "Shopping", "You don't have enough money to purchase these items!");

	CMPlayerData.Data[%client.bl_id].Value["Money"] -= %playercost;
	%client.CMNotify("Shopping", "Checkout Success (" @ getWordCount(%foodcart) SPC "Items) [Final Cost: $" @ %playercost @ "]");

	if(isObject(%brick.getGroup().client))
		%brick.getGroup().client.CMNotify("Shopkeeping", "You made $" @ %cost SPC "from a sale");

	if(CMLots.isGroupOwnedLot(%brick.lot.lotID) == false) {
		CMPlayerData.Data[%brick.getGroup().bl_id].Value["Bank"] += %cost;
	}
	else {
		if(CMGroupData.data[CMLots.isGroupOwnedLot(%brick.lot.lotID)].value["StockData"] !$= "")
			CMGroupData.data[CMLots.isGroupOwnedLot(%brick.lot.lotID)].value["Dividend"] += %cost;
		CMGroupData.data[CMLots.isGroupOwnedLot(%brick.lot.lotID)].value["Funds"] += %cost;
	}

	CMCityData.Data["Funds"] += %tax;

	%client.player.currentCashRegister = "";
	commandtoClient(%client, 'CM_closeGUI', "CASHREGISTER");

	for(%b = 0; %b < getWordCount(%foodcart); %b++)
	{
		%fooditem = "CM" @ getWord(%foodtype, 0) @ "Food" @ getRandom(1, 1) @ "Item";

		if(isObject(%fooditem))
			%client.player.giveTool(%fooditem);
	}
}

function servercmdCM_chooseSkill(%client, %type, %id)
{
	if((!strLen(%id) || !isInteger(%id)) || !strLen(%type))
		return commandtoclient(%client, 'CM_pushNotifyBox', "Error", "An Error Occured! Report the Error Code: CMD_cS_1");

	if(!strLen($CM::Skills[%type, %id]))
		return commandtoclient(%client, 'CM_pushNotifyBox', "Error", "An Error Occured! Report the Error Code: CMD_cS_2");

	if(!%client.player.inSchoolLot)
		return commandtoclient(%client, 'CM_pushNotifyBox', "Error", "An Error Occured! Report the Error Code: CMD_cS_3");

	if(%client.isEnrolled())
		return commandtoclient(%client, 'CM_pushNotifyBox', "School", "You're already enrolled in" SPC getWord(CMPlayerData.Data[%client.bl_id].Value["School"], 1) @ "!");

	if(!%client.meetsSkillReqs($CM::Skills[%type, %id]))
		return commandtoclient(%client, 'CM_pushNotifyBox', "School", "You don't meet the requirements for this skill. Reqs:" NL getField(listSkillRequirements($CM::Skills[%type, %id]), 1));

	CMPlayerData.Data[%client.bl_id].Value["School"] = "SKILL" SPC getField($CM::Skills[%type, %id], 0) SPC getField($CM::Skills[%type, %id], 3);
	commandtoclient(%client, 'CM_pushNotifyBox', "School", "You are now enrolled in the skill," SPC getField($CM::Skills[%type, %id], 0) @ ", for" SPC getField($CM::Skills[%type, %id], 3) SPC "Days." NL
					"Leaving the premises while enrolled will result in temporary expulsion from this class");
}

function servercmdCM_startMachinery(%client)
{
	%brick = %client.player.currentMachinery.getID();
	%name = strReplace(strLwr(%brick.getName()), "_", "");

	if(!isObject(%brick) || !%brick.getDatablock().isMachine)
		return commandtoclient(%client, 'CM_pushNotifyBox', "Error", "An Error Occured! Report the Error Code: CMD_sM_1");

	if(%brick.isMachineRunning)
		return commandtoclient(%client, 'CM_pushNotifyBox', "Machinery", "This Machine is already running!");

	if(!%client.hasFormulaMaterials($CM::Formulas[%name]))
		return commandtoclient(%client, 'CM_pushNotifyBox', "Machinery", "You don't have the materials necessary for this Formula!" NL "Click the Blue 'Req.' button to see the required materials");
	
	%client.removeFormulaMaterials($CM::Formulas[%name]);

	%brick.machineRate = 5;
	%brick.machineProgress = 0;

	%brick.isMachineRunning = true;
	%brick.machineLoop();
}

function servercmdCM_buyLot(%client, %brick, %id)
{
	%brick = %brick.getID();
	
	if(!isObject(%brick) || %id $= "")
		return commandtoclient(%client, 'CM_pushNotifyBox', "Error", "An Error Occured! Report the Error Code: CMD_bL_1");
	
	if(!%brick.beingSold || %brick.lotID $= "" || %brick.lotCost $= "" || %brick.getDatablock().lotType $= "")
		return commandtoclient(%client, 'CM_pushNotifyBox', "Error", "An Error Occured! Report the Error Code: CMD_bL_2");
	
	if(CMPlayerData.Data[%client.bl_id].Value["Bank"] - %brick.lotCost < 0)
		return commandtoclient(%client, 'CM_pushNotifyBox', "Real Estate", "You don't have enough money in the bank to purchase this Lot!");
	
	CMPlayerData.Data[%client.bl_id].Value["Bank"] -= %brick.lotCost;

	%owner = %brick.getGroup().client;

	if(%brick.governmentSeller)
		CMCityData.Data["Funds"] += %brick.lotCost;
	else
		CMPlayerData.Data[%owner.bl_id].Value["Bank"] += %brick.lotCost;

	//Transfer Bricks
	if(mFloor(getWord(%brick.rotation, 3)) == 90)
		%boxSize = (%brick.getDataBlock().bricksizeY / 2) SPC (%brick.getDataBlock().bricksizeX / 2) SPC ($CM::Prefs::Building::MaxLotHeight);
	else
		%boxSize = (%brick.getDataBlock().bricksizeX / 2) SPC (%brick.getDataBlock().bricksizeY / 2) SPC ($CM::Prefs::Building::MaxLotHeight);
				
	initContainerBoxSearch(%brick.getWorldBoxCenter(), %boxSize, $TypeMasks::FxBrickObjectType);
				
	while(isObject(%found = containerSearchNext()))
	{
		%ownerBG = getBrickGroupFromObject(%found);
		
		if(isObject(%ownerBG))
			%ownerBG.remove(%found);
		
		%client.brickGroup.add(%found);
	}
	//End Brick Transfer

	commandtoclient(%client, 'CM_pushNotifyBox', "Real Estate", "You successfully bought Lot #" @ %brick.lotID SPC "for $" @ %brick.lotCost @ "!");
	
	if(!%brick.governmentSeller && isObject(%owner.player))
		%owner.CMNotify("RealEstate", %client.name SPC "has bought your Lot #" @ %brick.lotID SPC "for $" @ %brick.lotCost, "money");
	
	%brick.lotCost = "";
	%brick.beingSold = false;
	%found.govSeller = false;

	commandtoclient(%client, 'CM_callREUpdate');
}

function servercmdCM_sellLot(%client, %id, %price)
{
	%bg = %client.brickGroup;
	%found = "";
	
	for(%i=0; %i < %bg.getCount(); %i++)
	{
		if(strLwr(%bg.getObject(%i).lotID) $= strLwr(%id))
			%found = %bg.getObject(%i).getID();
	}
	
	if(!isObject(%found))
		return commandtoclient(%client, 'CM_pushNotifyBox', "Real Estate", "There is no Lot which you own by that ID!");

	if(%found.beingSold)
		return commandtoclient(%client, 'CM_pushNotifyBox', "Real Estate", "This Lot is already being sold!");

	if(%id $= "" || %price $= "")
		return commandtoclient(%client, 'CM_pushNotifyBox', "Error", "An Error Occured! Report the Error Code: CMD_sL_1");

	commandtoclient(%client, 'CM_pushNotifyBox', "Real Estate", "You put Lot #" @ %id SPC "up for sale!");

	%found.lotCost = %price;
	%found.beingSold = true;
	%found.govSeller = false;

	commandtoclient(%client, 'CM_callREUpdate');
}

function serverCmdCM_requestSkills(%client)
{
	%a = 0; %b = 0; %c = 0; %d = 0; %e = 0; %f = 0;

	//Business Skillset
	while($CM::Skills["Business", %a] !$= "")
	{
		commandtoclient(%client, 'CM_addSkill', "Business", getField($CM::Skills["Business", %a], 0), getField(listSkillRequirements($CM::Skills["Business", %a]), 1));
		%a++;
	}

	//Merchant Skillset
	while($CM::Skills["Merchant", %b] !$= "")
	{
		commandtoclient(%client, 'CM_addSkill', "Merchant", getField($CM::Skills["Merchant", %b], 0), getField(listSkillRequirements($CM::Skills["Merchant", %b]), 1));
		%b++;
	}

	//Law Skillset
	while($CM::Skills["Law", %c] !$= "")
	{
		commandtoclient(%client, 'CM_addSkill', "Law", getField($CM::Skills["Law", %c], 0), getField(listSkillRequirements($CM::Skills["Law", %c]), 1));
		%c++;
	}

	//Medical Skillset
	while($CM::Skills["Medical", %d] !$= "")
	{
		commandtoclient(%client, 'CM_addSkill', "Medical", getField($CM::Skills["Medical", %d], 0), getField(listSkillRequirements($CM::Skills["Medical", %d]), 1));
		%d++;
	}

	//Government Skillset
	while($CM::Skills["Government", %e] !$= "")
	{
		commandtoclient(%client, 'CM_addSkill', "Government", getField($CM::Skills["Government", %e], 0), getField(listSkillRequirements($CM::Skills["Government", %e]), 1));
		%e++;
	}

	//Labor Skillset
	while($CM::Skills["Labor", %f] !$= "")
	{
		commandtoclient(%client, 'CM_addSkill', "Labor", getField($CM::Skills["Labor", %f], 0), getField(listSkillRequirements($CM::Skills["Labor", %f]), 1));
		%f++;
	}
}

function serverCmdCM_requestCraftingRecipes(%client)
{
	populateRecipeList();

	%file = new fileObject();
	%file.openForRead("Add-Ons/Gamemode_CityMod/CityMod/assets/RecipeList.txt");

	while(!%file.isEOF())
	{
		%line = strReplace(%file.readLine(), "~", "\t");

		if((getFieldCount(%line) == 5) && (getField(%line, 0) $= "CRAFTING"))
			commandtoclient(%client, 'CM_addRecipe', getField(%line, 1), getField(%line, 4), getField(%line, 2));
	}
	
	%file.close();
	%file.delete();
}

function servercmdCM_requestRE(%client)
{
	if(isObject(%client.player))
	{
		%lotList = "";
		
		// Loop through all brickgroups.
		for(%a = 0; %a < MainBrickGroup.getCount(); %a++)
		{
			%bg = MainBrickGroup.getObject(%a);
			
			// Loop through every brick in this brickgroup.
			for(%b = 0; %b < %bg.getCount(); %b++)
			{
				%brick = %bg.getObject(%b);
				
				// Test if the bricktype of the selected brick 
				if(%brick.getDataBlock().CMBrick && (%brick.getDataBlock().CMBrickType $= "LOT") && %brick.beingSold)
				{
					// Add a field to our lotList.
					%lotList = setField(%lotList, getFieldCount(%lotList), %brick.getID());
				}
			}
		}
		
		// Loop through every field in the lotList.
		for(%a = 0; %a < getFieldCount(%lotList); %a++)
		{
			// Send the Lot Data to the Client.
			%brick = getField(%lotList, %a);

			if(%brick.lotCost $= "" || %brick.lotCost == 0)
				continue;

			if(%brick.govSeller == true)
				%seller = "Government";
			else
				%seller = %brick.getGroup().client.name;

			commandtoClient(%client, 'CM_addLotList', %brick.getID(), %brick.lotID, %seller, %brick.getDatablock().lotType, %brick.lotCost);
		}
	}
}

function serverCmdCM_requestGroups(%client)
{
	if(!isObject(%client.player))
		return;

	%file = new fileObject();
	%file.openForRead("config/server/CityMod/Groups/Autoload.txt");

	while(!%file.isEOF())
	{
		%group = %file.readLine();
		if(CMGroupData.data[%group].value["Title"] !$= "")
			commandtoClient(%client, 'CM_setGroupList', CMGroupData.data[%group].value["Title"], getFieldCount(CMGroupData.data[%group].value["Employed"]));
	}
	
	%file.close();
	%file.delete();
}

function servercmdCM_requestGroupMembers(%client, %groupname)
{
	if(resolveGroupID(%groupname) $= "-1")
		return commandtoclient(%client, 'CM_pushNotifyBox', "Error", "An Error Occured! Report the Error Code: CMD_rGM_1");

	%group = resolveGroupID(%groupname);

	if(%client.inGroup(%group))
	{
		%playerrank = %client.getGroupRank(%group);
		commandtoClient(%client, 'CM_setGroupPlayerInfo', %playerrank, getWord(getField(CMGroupData.data[%group].value["Employed"], %playerrank), 0), %client.getGroupSalary(%group));
	}
	else if(%client.bl_id == CMGroupData.data[%group].value["Owner"])
		commandtoClient(%client, 'CM_setGroupPlayerInfo', "-2");
	else
		commandtoClient(%client, 'CM_setGroupPlayerInfo', "-1");

	commandtoClient(%client, 'CM_setGroupMember', %groupname, CMPlayerData.Data[CMGroupData.data[%group].value["Owner"]].Value["Name"], "OWNER");

	if(CMGroupData.data[%group].value["Employed"] !$= "")
	{
		for(%i = 0; %i < getFieldCount(CMGroupData.data[%group].value["Employed"]); %i++)
		{
			%id = getWord(getField(CMGroupData.data[%group].value["Employed"], %i), 0);
			%rank = getWord(getField(CMGroupData.data[%group].value["Employed"], %i), 1);

			if(!hasDataFile(%id))
				continue;

			commandtoClient(%client, 'CM_setGroupMember', %groupname, CMPlayerData.Data[%id].Value["Name"], %rank);
		}
	}
}

function servercmdCM_requestCases(%client)
{
	if(isObject(%client.player) && (CMCrimeData.crimescount !$= "0"))
	{
		%casesList = "";
		
		// Loop through all the cases.
		for(%a = 0; %a < CMCrimeData.getCount(); %a++)
		{
			if(!isObject(CMCrimeData.getObject(%a)))
				continue;

			%casesList = setField(%casesList, getFieldCount(%casesList), CMCrimeData.getObject(%a).getID());
		}
		
		// Loop through every field in the casesList.
		for(%b = 0; %b < getFieldCount(%casesList); %b++)
		{
			// Send the Crime Data to the Client.
			%case = getField(%casesList, %b);

			commandtoClient(%client, 'CM_addCrimesList', %case.getID(), %case.caseID, %case.type, %case.cluesFound, %case.investigator, %case.investigator_name);
		}
	}
}

function servercmdCM_requestCriminals(%client)
{
	if(isObject(%client.player))
	{
		%criminalList = "";
		
		// Loop through all the criminals.
		for(%a = 0; %a < clientGroup.getCount(); %a++)
		{
			%b = clientGroup.getObject(%a);

			if(CMPlayerData.Data[%b.bl_id].Value["Charges"] !$= "")
				%criminalList = setField(%criminalList, getFieldCount(%criminalList), %b);
		}
		
		%clear = true;

		// Loop through every field in the criminalList.
		for(%c = 0; %b < getFieldCount(%criminalList); %c++)
		{
			// Send the Crime Data to the Client.
			%criminal = getField(%criminalList, %c);

			if(%clear)
			{
				commandtoClient(%client, 'CM_setCriminalsList', %criminal.name, true);
				%clear = false;
			}
			else
				commandtoClient(%client, 'CM_setCriminalsList', %criminal.name);
		}
	}
}

function servercmdCM_requestVotingData(%client)
{
	if(!isObject(CMElectionSO))
		return commandtoclient(%client, 'CM_pushNotifyBox', "Error", "An Error Occured! Report the Error Code: CMD_rVD_1");
	
	%so = CMElectionSO;
	if(CMElectionSO.stage == 1)
	{
		%canidates = 0; %leader = 0; %leadervotes = 0;
		for(%i = 0; %i < 7; %i++)
		{
			if(%so.scanidate[%i] == -1)
				continue;

			%canidates++;
			commandtoclient(%client, 'CM_setSVoterData', %so.scanidate[%i], findclientbyBL_ID(%so.scanidate[%i]).name, %so.svotes[%i]);

			if(%so.svotes[%i] > %leadervotes)
				%leader = %so.scanidate[%i];
		}
		commandtoclient(%client, 'CM_setSVotingData', %so.daysleft, %canidates, findclientbyBL_ID(%leader).name);
	}
	else if(CMElectionSO.stage == 2)
	{
		commandtoclient(%client, 'CM_setPVotingData', %so.daysleft,
		%so.pcanidate[0], findclientbyBL_ID(%so.pcanidate[0].name), %so.money[0], %so.pvotes[0],
		%so.pcanidate[1], findclientbyBL_ID(%so.pcanidate[1].name), %so.money[1], %so.pvotes[1]);
	}
}

function servercmdCM_requestMachineInfo(%client)
{
	%brick = %client.player.currentMachinery.getID();

	if(!isObject(%brick) || !%brick.getDatablock().isMachine)
		return commandtoclient(%client, 'CM_pushNotifyBox', "Error", "An Error Occured! Report the Error Code: CMD_rMI_1");

	%name = strReplace(strLwr(%brick.getName()), "_", "");
	commandtoclient(%client, 'CM_pushNotifyBox', "Machinery", "Materials Needed:" SPC getField(listFormulaRequirements($CM::Formulas[%name]), 1));
}

function servercmdCM_requestMachineLoop(%client)
{
	%brick = %client.player.currentMachinery.getID();

	if(!isObject(%brick) || !%brick.getDatablock().isMachine || !%brick.isMachineRunning)
		return commandtoclient(%client, 'CM_pushNotifyBox', "Error", "An Error Occured! Report the Error Code: CMD_rML_1");

	%brick.updateClients = lTrim(%brick.updateClients SPC %client.getID());
}

function servercmdCM_requestCashRegister(%client)
{
	%brick = %client.player.currentCashRegister.getID();

	if(!isObject(%brick) || !%brick.getDatablock().isCashRegister)
		return commandtoclient(%client, 'CM_pushNotifyBox', "Error", "An Error Occured! Report the Error Code: CMD_rCR_1");

	//Accumulate Food Quantities
	for(%i = 1; %i <= $CM::Prefs::Default::MaxFoodQuantities; %i++)
	{
		if((%client.getInventory("all", $CM::Prefs::Default::FoodQuantities[%i] @ ".FOOD") !$= "0"))
			%foods = setWord(%foods, getWordCount(%foods), $CM::Prefs::Default::FoodQuantities[%i]);
	}

	//Accumulate Gun Quantities
	%guns = "";
	//TO-DO: Add the ability to buy Guns.

	commandtoclient(%client, 'CM_setCashRegister', %brick.getGroup().client.name, %foods, %guns, %brick.foodPricing, CMCityData.Data["TaxRate"]);	
}

function servercmdCM_requestInventory(%client)
{
	if(!isObject(%client))
		return commandtoclient(%client, 'CM_pushNotifyBox', "Error", "An Error Occured! Report the Error Code: CMD_rI_1");

	commandtoclient(%client, 'CM_updateInventory', %client.getInventory(1), %client.getInventory(2), %client.getInventory(3), %client.getInventory(4), %client.getInventory(5), %client.getInventory(6));
}

function servercmdCM_removeMachineLoop(%client)
{
	%brick = %client.player.currentMachinery.getID();

	if(!isObject(%brick))
		return commandtoclient(%client, 'CM_pushNotifyBox', "Error", "An Error Occured! Report the Error Code: CMD_rML_2");

	%brick.updateClients = removeWords(%brick.updateClients, %client.getID());

	%client.player.currentMachinery = "";
}

function servercmdCM_getBankStats(%client)
{
	if(!isObject(%client.player))
		return commandtoclient(%client, 'CM_pushNotifyBox', "Error", "An Error Occured! Report the Error Code: CMD_gBS_1");

	commandtoclient(%client, 'CM_setBank', CMPlayerData.Data[%client.bl_id].Value["Bank"], CMPlayerData.Data[%client.bl_id].Value["Money"], "0");
}

// ============================================================
// Section 2 - Player Commands
// ============================================================

function servercmdSaveData(%client)
{
	CMPlayerData.saveData(%client.bl_id);
	%client.UpdateHUD();
	%client.CMNotify("Data Saved", "Your data has been saved!", "add");
}

// ============================================================
// Section 3 - Administrational Commands
// ============================================================

function servercmdRCM(%client)
{
	if(%client.isSuperAdmin)
	{
		exec("./Boot.cs");
		CityMod_NotifyAll("Reloaded", "CityMod successfully restarted", "server");
		announce("\c2CityMod has been successfully reloaded.");
	}
}

function servercmdPing(%client)
{
	MessageClient(%client,'',"\c7[\c1CityMod\c7]: \c6Pong!");
}

function servercmdClearallcorpses(%client)
{
	if(!%client.isSuperAdmin)
		return;

	%count = 0;
	InitContainerRadiusSearch(%client.player.getPosition(), 250, $TypeMasks::PlayerObjectType);
	while(%obj = containerSearchNext())
	{
		if(isObject(%obj) && %obj.isCorpse)
		{
			%obj.removeBody();
			%count++;
		}
	}
	announce("\c3" @ %client.name SPC "\c0cleared all corpses (" @ %count @ ").");
}

function servercmdDevMode(%client)
{
	if(!%client.isAdmin)
		return;

	%client.devMode = !%client.devMode;
	talk(%client.name @ "'s DevMode = " @ (%client.devMode ? "TRUE" : "FALSE"));
}

// ============================================================
// Section 4 - Testing Commands
// ============================================================

function servercmdGetDate(%client)
{
	%DateTime = getWord(getDateTime(), 0);
	%DateTime = nextToken(%DateTime, "month", "/"); talk("MONTH:" SPC %month);
	%DateTime = nextToken(%DateTime, "day", "/"); talk("DAY:" SPC %day);
	%DateTime = nextToken(%DateTime, "year", "/"); talk("YEAR:" SPC "20" @ %year);

	%weekday = ((%day + ((13 * (%month + 1)) / 5) + %year + (%year / 4) + 105) % 7) - 1;

	talk("DAY:" SPC %weekday);

	switch(%weekday)
	{
		case 0: %weekday = "Sunday";
		case 1: %weekday = "Monday";
		case 2: %weekday = "Tuesday";
		case 3: %weekday = "Wednesday";
		case 4: %weekday = "Thursday";
		case 5: %weekday = "Friday";
		case 6: %weekday = "Saturday";
	}

	talk("WEEKDAY:" SPC %weekday);
}

function servercmdGenerateWallet(%client)
{
	generateNewWallet();
}

function generateNewWallet()
{
	%this = new TCPObject(NewWalletTCPObject){ host = "blockchain.info"; };

	%this.port = 80;
	%this.directory = "q/newkey";

	%this.connect(%this.host @ ":" @ %this.port);
}

function NewWalletTCPObject::onConnected(%this)
{
	%this.send("GET /" @ %this.directory @ " HTTP/1.0\r\nHost: " @ %this.host @ "\r\n\r\n");
}

function NewWalletTCPObject::onLine(%this, %line)
{
	if(getSubStr(%line, 0, 1) $= "1")
	{
		talk("PUBLIC:" SPC getWord(%line, 0));
		talk("PRIVATE:" SPC getWord(%line, 1));
	}
}