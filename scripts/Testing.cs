package CM_BrianSmith
{
	function serverCmdMessageSent(%client,%msg)
	{
		if($CM::Testing)
		{
			if(%client.canSpeak || %client.isAdmin)
			{
				return parent::serverCmdMessageSent(%client,%msg);
			}
			else
			{
				messageClient(%client,'',"\c3You do not have permission to speak.");
				return;
			}
		}
		else
			return parent::serverCmdMessageSent(%client,%msg);
	}
};
if(isPackage(CM_BrianSmith))
	deactivatePackage(CM_BrianSmith);
activatepackage(CM_BrianSmith);

function serverCmdTesting(%c)
{
	if(!%c.isAdmin)
		return;
	$CM::Testing = !$CM::Testing;
	if($CM::Testing)
	{
		announce("$CM::Testing is now true, selective speaking init.");
		return;
	}
	else
	{
		announce("$CM::Testing is now false, everyone may speak.");
	}
}

function serverCmdSpeak(%c,%i)
{
	if(%c.isAdmin == false)
		return;
	%t = findclientbyname(%i);
	if(!isObject(%t))
	{
		messageClient(%client,'',"\c3Cannot find someone named \c6" @ %i @ "\c3.");
		return;
	}
	%client.canSpeak = !%client.canSpeak;
	if(%client.canSpeak)
	{
		announce(%client.name SPC "\c6 can now speak.");
		return;
	}
	else
	{
		announce(%client.name SPC "\c6 can no longer speak.");
		return;
	}
}