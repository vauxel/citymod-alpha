function CMSaveBricks(%name, %desc, %events, %ownership)
{
	%path = "saves/" @ %name @ ".bls";
	if(!isWriteableFileName(%path))
	{
		error("Cannot save to file: ", %path);
		return;
	}
	%file = new FileObject();
	%file.openForWrite(%path);
	%file.writeLine("This is a Blockland save file.  You probably shouldn't modify it cause you'll screw it up.");
	%file.writeLine("1"); // What does this mean?
	%file.writeLine(%desc);
	for(%i=0;%i<64;%i++)
		%file.writeLine(getColorIDTable(%i));
	%bricks = 0;
	for(%i=0;%i<mainBrickGroup.getCount();%i++)
		%bricks += mainBrickGroup.getObject(%i).getCount();
	%file.writeLine("Linecount " @ %bricks);
	for(%d=0;%d<2;%d++)
	{
		for(%i=0;%i<mainBrickGroup.getCount();%i++)
		{
			%group = mainBrickGroup.getObject(%i);
			for(%a=0;%a<%group.getCount();%a++)
			{
				%brick = %group.getObject(%a);
				if(!(%d ^ %brick.isBasePlate()))
					continue;
				if(%brick.getDataBlock().hasPrint)
				{
					%texture = getPrintTexture(%brick.getPrintId());
					%path = filePath(%texture);
					%underscorePos = strPos(%path, "_");
					%name = getSubStr(%path, %underscorePos + 1, strPos(%path, "_", 14) - 14) @ "/" @ fileBase(%texture);
					if($printNameTable[%name] !$= "")
					{
						%print = %name;
					}
				}
				%file.writeLine(%brick.getDataBlock().uiName @ "\" " @ %brick.getPosition() SPC %brick.getAngleID() SPC %brick.isBasePlate() SPC %brick.getColorID() SPC %print SPC %brick.getColorFXID() SPC %brick.getShapeFXID() SPC %brick.isRayCasting() SPC %brick.isColliding() SPC %brick.isRendering());
				if(%ownership && %brick.isBasePlate() && !$Server::LAN)
					%file.writeLine("+-OWNER " @ getBrickGroupFromObject(%brick).bl_id);
				if(%events)
				{
					if(%brick.getName() !$= "")
						%file.writeLine("+-NTOBJECTNAME " @ %brick.getName());
					for(%b=0;%b<%brick.numEvents;%b++)
					{
						%targetClass = %brick.eventTargetIdx[%b] >= 0 ? getWord(getField($InputEvent_TargetListfxDTSBrick_[%brick.eventInputIdx[%b]], %brick.eventTargetIdx[%b]), 1) : "fxDtsBrick";
						%paramList = $OutputEvent_parameterList[%targetClass, %brick.eventOutputIdx[%b]];
						%params = "";
						for(%c=0;%c<4;%c++)
						{
							if(firstWord(getField(%paramList, %c)) $= "dataBlock" && isObject(%brick.eventOutputParameter[%b, %c + 1]))
								%params = %params TAB %brick.eventOutputParameter[%b, %c + 1];
							else
								%params = %params TAB %brick.eventOutputParameter[%b, %c + 1];
						}
						%file.writeLine("+-EVENT" TAB %b TAB %brick.eventEnabled[%b] TAB %brick.eventInput[%b] TAB %brick.eventDelay[%b] TAB %brick.eventTarget[%b] TAB %brick.eventNT[%b] TAB %brick.eventOutput[%b] @ %params);
					}
				}
				if(isObject(%brick.emitter))
					%file.writeLine("+-EMITTER " @ %brick.emitter.emitter.uiName @ "\" " @ %brick.emitterDirection);
				if(%brick.getLightID() >= 0)
					%file.writeLine("+-LIGHT " @ %brick.getLightID().getDataBlock().uiName @ "\" "); // Not sure if something else comes after the name
				if(isObject(%brick.item))
					%file.writeLine("+-ITEM " @ %brick.item.getDataBlock().uiName @ "\" " @ %brick.itemPosition SPC %brick.itemDirection SPC %brick.itemRespawnTime);
				if(isObject(%brick.audioEmitter))
					%file.writeLine("+-AUDIOEMITTER " @ %brick.audioEmitter.getProfileID().uiName @ "\" "); // Not sure if something else comes after the name
				if(isObject(%brick.vehicleSpawnMarker))
					%file.writeLine("+-VEHICLE " @ %brick.vehicleSpawnMarker.uiName @ "\" " @ %brick.reColorVehicle);
				if(%brick.lotID !$= "")
					%file.writeLine("+-LOTID " @ %brick.lotID);
			}
		}
	}
	%file.close();
	%file.delete();
	messageAll('',"Bricks have been successfully saved.");
}

function serverDirectSaveFileLoad(%filename, %colorMethod, %dirName, %ownership, %silent)
{
	echo("Direct load " @ %filename @ ", " @ %colorMethod @ ", " @ %dirName @ ", " @ %ownership @ ", " @ %silent);

	if (!isFile(%filename))
	{
		MessageAll('', "ERROR: File \"" @ %filename @ "\" not found.  If you are seeing this, you broke something.");
		return;
	}

	$LoadingBricks_ColorMethod = %colorMethod;
	$LoadingBricks_DirName = %dirName;
	$LoadingBricks_Ownership = %ownership;

	if ($LoadingBricks_Ownership $= "")
		$LoadingBricks_Ownership = 1;

	calcSaveOffset();

	if ($loadingBricks_Client && $LoadingBricks_Client != 1)
	{
		MessageAll('', "Load interrupted by host.");

		if (isObject($LoadBrick_FileObj))
		{
			$LoadBrick_FileObj.close();
			$LoadBrick_FileObj.delete();
		}
	}

	$LoadingBricks_Client = findLocalClient();
	if ($LoadingBricks_Client)
	{
		if ($LoadingBricks_Ownership == 2)
			$LoadingBricks_BrickGroup = BrickGroup_888888;
		else
			$LoadingBricks_BrickGroup = $LoadingBricks_Client.brickGroup;
	}
	else
	{
		if ($Server::LAN)
		{			
			if ($LoadingBricks_Ownership == 2)
				$LoadingBricks_BrickGroup = BrickGroup_888888;
			else
				$LoadingBricks_BrickGroup = BrickGroup_999999;
		}
		else
		{
			if ($LoadingBricks_Ownership == 2)
				$LoadingBricks_BrickGroup = BrickGroup_888888;
			else
				$LoadingBricks_BrickGroup = "BrickGroup_" @ getNumKeyID();
		}

		$LoadingBricks_BrickGroup.isAdmin = 1;
		$LoadingBricks_BrickGroup.brickGroup = $LoadingBricks_BrickGroup;
		$LoadingBricks_Client = $LoadingBricks_BrickGroup;
	}

	$LoadingBricks_Silent = %silent;

	if (!$LoadingBricks_Silent)
		MessageAll('MsgUploadStart', "Loading bricks. Please wait.");

	$LoadingBricks_StartTime = getSimTime();

	ServerLoadSaveFile_Start(%filename);
}

function calcSaveOffset()
{
	%offset["Bedroom"] = "50 -80 286.4";
	%offset["Kitchen"] = "-380 0 119.2";
	%offset["Slate"] = "0 0 0";

	$LoadingBricks_PositionOffset = VectorSub("0 0 0", %offset[$LoadingBricks_DirName]);
	$LoadingBricks_PositionOffset = VectorAdd($LoadingBricks_PositionOffset, $LoadOffset);
}

function ServerLoadSaveFile_Start(%filename)
{
	echo("LOADING BRICKS: " @ %filename @ " (ColorMethod " @ $LoadingBricks_ColorMethod @ ")");

	if ($Game::MissionCleaningUp)
	{
		echo("LOADING CANCELED: Mission cleanup in progress");
		return;
	}

	$Server_LoadFileObj = new FileObject(){};

	if (isFile(%filename))
		$Server_LoadFileObj.openForRead(%filename);
	else
		$Server_LoadFileObj.openForRead("base/server/temp/temp.bls");

	if ($UINameTableCreated == 0)
		createUINameTable();

	$LastLoadedBrick = 0;
	$Load_failureCount = 0;
	$Load_brickCount = 0;

	$Server_LoadFileObj.readLine();
	%lineCount = $Server_LoadFileObj.readLine();

	for (%i = 0; %i < %lineCount; %i++)
		$Server_LoadFileObj.readLine();

	if (isEventPending($LoadSaveFile_Tick_Schedule))
		cancel($LoadSaveFile_Tick_Schedule);

	ServerLoadSaveFile_ProcessColorData();
	ServerLoadSaveFile_Tick();
	stopRaytracer();
}

function ServerLoadSaveFile_ProcessColorData()
{
	%colorCount = -1;

	for (%i = 0; %i < 64; %i++)
		if (getWord(getColorIDTable(%i), 3) > 0.001)
			%colorCount++;

	if ($LoadingBricks_ColorMethod != 0)
	{
		if ($LoadingBricks_ColorMethod == 1)
		{
			%divCount = 0;

			for (%i = 0; %i < 16; %i++)
			{
				if (getSprayCanDivisionSlot(%i) == 0)
					break;

				%divCount++;
			}
		}
		else if ($LoadingBricks_ColorMethod == 2)
		{
			%colorCount = -1;
			%divCount = -1;
		}
	}

	for (%i = 0; %i < 64; %i++)
	{
		%color = $Server_LoadFileObj.readLine();

		%red = getWord(%color, 0);
		%green = getWord(%color, 1);
		%blue = getWord(%color, 1);
		%alpha = getWord(%color, 3);

		if ($LoadingBricks_ColorMethod == 0)
		{
			if (%alpha >= 0.0001)
			{
				%match = 0;

				for (%j = 0; %j < 64; %j++)
				{
					if (colorMatch(getColorIDTable(%j), %color))
					{
						$colorTranslation[%i] = %j;
						%match = 1;
						break;
					}
				}

				if (%match == 0)
					error("ERROR: ServerLoadSaveFile_ProcessColorData() - color method 0 specified but match not found for color " @ %color);
			}
		}
		else if ($LoadingBricks_ColorMethod == 1)
		{
			if (%alpha >= 0.0001)
			{
				%match = 0;

				for (%j = 0; %j < 64; %j++)
				{
					if (colorMatch(getColorIDTable(%j), %color))
					{
						$colorTranslation[%i] = %j;
						%match = 1;
						break;
					}
				}

				if (%match == 0)
				{
					setSprayCanColor(%colorCount++, %color);
					$colorTranslation[%i] = %colorCount;
				}
			}
		}
		else if ($LoadingBricks_ColorMethod == 2)
		{
			setSprayCanColor(%colorCount++, %color);
			$colorTranslation[%i] = %i;
		}
		else if ($LoadingBricks_ColorMethod == 3)
		{
			if (%alpha > 0.0001)
			{
				%minDiff = 99999;
				%matchIdx = -1;

				for (%j = 0; %j < 64; %j++)
				{
					%checkColor = getColorIDTable(%j);
					%checkRed = getWord(%checkColor, 0);
					%checkGreen = getWord(%checkColor, 1);
					%checkBlue = getWord(%checkColor, 2);
					%checkAlpha = getWord(%checkColor, 3);

					%diff = 0;

					%diff += mAbs(mAbs(%checkRed) - mAbs(%red));
					%diff += mAbs(mAbs(%checkGreen) - mAbs(%green));
					%diff += mAbs(mAbs(%checkBlue) - mAbs(%blue));

					if ((%checkAlpha > 0.99 && %alpha < 0.99) || (%checkAlpha < 0.99 && %alpha > 0.99))
						%diff += 1000;
					else
						%diff += mAbs(mAbs(%checkAlpha) - mAbs(%alpha)) * 0.5;

					if (%diff < %minDiff)
					{
						%minDiff = %diff;
						%matchIdx = %j;
					}
				}

				if (%matchIdx == -1)
					error("ERROR - LoadBricks() - Nearest match failed - wtf.");
				else
					$colorTranslation[%i] = %matchIdx;
			}
		}
	}

	if ($LoadingBricks_ColorMethod == 1)
	{
		echo("  setting spraycan division at ", %divCount, " ", %colorCount);
		setSprayCanDivision(%divCount, %colorCount, "File");
	}

	if ($LoadingBricks_ColorMethod != 0 && $LoadingBricks_ColorMethod != 3)
	{
		$maxSprayColors = %colorCount;

		for (%clientIndex = 0; %clientIndex < ClientGroup.getCount(); %clientIndex++)
		{
			%cl = ClientGroup.getObject(%clientIndex);
			%cl.transmitStaticBrickData();
			%cl.transmitDataBlocks(1);
			commandToClient(%cl, 'PlayGui_LoadPaint');
		}
	}
}

function colorMatch(%colorA, %colorB)
{
	for (%i = 0; %i < 4; %i++)
		if (mAbs(getWord(%colorA, %i) - getWord(%colorB, %i)) > 0.005)
			return 0;

	return 1;
}

function ServerLoadSaveFile_Tick()
{
	if (isObject(ServerConnection))
		if (!ServerConnection.isLocal())
			return;

	%line = $Server_LoadFileObj.readLine();

	if (trim(%line) $= "")
		return;

	%firstWord = getWord(%line, 0);

	if (%firstWord $= "+-EVENT")
	{
		if (isObject($LastLoadedBrick))
		{
			%idx = getField(%line, 1);
			%enabled = getField(%line, 2);
			%inputName = getField(%line, 3);
			%delay = getField(%line, 4);
			%targetName = getField(%line, 5);
			%NT = getField(%line, 6);
			%outputName = getField(%line, 7);
			%par1 = getField(%line, 8);
			%par2 = getField(%line, 9);
			%par3 = getField(%line, 10);
			%par4 = getField(%line, 11);

			%inputEventIdx = inputEvent_GetInputEventIdx(%inputName);
			%targetIdx = inputEvent_GetTargetIndex("fxDTSBrick", %inputEventIdx, %targetName);

			if (%targetName == -1)
				%targetClass = "fxDTSBrick";
			else
			{
				%field = getField($InputEvent_TargetList["fxDTSBrick", %inputEventIdx], %targetIdx);
				%targetClass = getWord(%field, 1);
			}

			%outputEventIdx = outputEvent_GetOutputEventIdx(%targetClass, %outputName);
			%NTNameIdx = -1;

			if ($LoadingBricks_Client == $LoadingBricks_BrickGroup)
			{
				for (%j = 0; %j < 4; %j++)
				{
					%field = getField($OutputEvent_parameterList[%targetClass, %outputEventIdx], %j);
					%dataType = getWord(%field, 0);

					if (%dataType $= "datablock" && %par[%j + 1] != -1 && !isObject(%par[%j + 1]))
						warn("WARNING: could not find datablock for event " @ %outputName @ " -> " @ %par[%j + 1]);
				}
			}

			$LoadingBricks_Client.wrenchBrick = $LastLoadedBrick;
			serverCmdAddEvent($LoadingBricks_Client, %enabled, %inputEventIdx, %delay, %targetIdx, %NTNameIdx, %outputEventIdx, %par1, %par2, %par3, %par4);
			$LastLoadedBrick.eventNT[$LastLoadedBrick.numEvents - 1] = %NT;
		}
	}
	else if (%firstWord $= "+-NTOBJECTNAME")
	{
		if (isObject($LastLoadedBrick))
		{
			%name = getWord(%line, 1);
			$LastLoadedBrick.setNTObjectName(%name);
		}
	}
	else if (%firstWord $= "+-LIGHT")
	{
		if (isObject($LastLoadedBrick))
		{
			%line = getSubStr(%line, 8, strlen(%line) - 8);

			%pos = strpos(%line, "\"");
			%dbName = getSubStr(%line, 0, %pos);
			%db = $uiNameTable_Lights[%dbName];

			if ($LoadingBricks_Client == $LoadingBricks_BrickGroup)
				if (!isObject(%db))
					warn("WARNING: could not find light datablock for uiname \"" @ %dbName @ "\"");

			if (!isObject(%db))
				%db = $uiNameTable_Lights["Player\'s Light"];

			if ((strlen(%line) - %pos - 2) >= 0)
			{
				%line = getSubStr(%line, %pos + 2, strlen(%line) - %pos - 2);
				%enabled = %line;

				if (%enabled $= "")
					%enabled = 1;
			}
			else
				%enabled = 1;

			%quotaObject = getQuotaObjectFromBrick($LastLoadedBrick);
			setCurrentQuotaObject(%quotaObject);

			$LastLoadedBrick.setLight(%db);
			if (isObject($LastLoadedBrick.light))
				$LastLoadedBrick.light.setEnable(%enabled);

			clearCurrentQuotaObject();
		}
	}
	else if (%firstWord $= "+-EMITTER")
	{
		if (isObject($LastLoadedBrick))
		{
			%line = getSubStr(%line, 10, strlen(%line) - 10);
			%pos = strpos(%line, "\"");
			%dbName = getSubStr(%line, 0, %pos);

			if (%dbName $= "NONE")
				%db = 0;
			else
				%db = $uiNameTable_Emitters[%dbName];

			if ($LoadingBricks_Client == $LoadingBricks_BrickGroup)
				if (%db $= "")
					warn("WARNING: could not find emitter datablock for uiname \"" @ %dbName @ "\"");

			%line = getSubStr(%line, %pos + 2, strlen(%line) - %pos - 2);
			%dir = getWord(%line, 0);

			%quotaObject = getQuotaObjectFromBrick($LastLoadedBrick);
			setCurrentQuotaObject(%quotaObject);

			if (isObject(%db))
				$LastLoadedBrick.setEmitter(%db);

			$LastLoadedBrick.setEmitterDirection(%dir);

			clearCurrentQuotaObject();
		}
	}
	else if (%firstWord $= "+-ITEM")
	{
		if (isObject($LastLoadedBrick))
		{
			%line = getSubStr(%line, 7, strlen(%line) - 7);
			%pos = strpos(%line, "\"");
			%dbName = getSubStr(%line, 0, %pos);

			if (%dbName $= "NONE")
				%db = 0;
			else
				%db = $uiNameTable_Items[%dbName];

			if ($LoadingBricks_Client == $LoadingBricks_BrickGroup)
				if (%dbName !$= "NONE" && !isObject(%db))
					warn("WARNING: could not find item datablock for uiname \"" @ %dbName @ "\"");

			%line = getSubStr(%line, %pos + 2, strlen(%line) - %pos - 2);
			%pos = getWord(%line, 0);
			%dir = getWord(%line, 1);
			%respawnTime = getWord(%line, 2);

			%quotaObject = getQuotaObjectFromBrick($LastLoadedBrick);
			setCurrentQuotaObject(%quotaObject);

			if (isObject(%db))
				$LastLoadedBrick.setItem(%db);

			$LastLoadedBrick.setItemDirection(%dir);
			$LastLoadedBrick.setItemPosition(%pos);
			$LastLoadedBrick.setItemRespawntime(%respawnTime);

			clearCurrentQuotaObject();
		}
	}
	else if (%firstWord $= "+-AUDIOEMITTER")
	{
		if (isObject($LastLoadedBrick))
		{
			%line = getSubStr(%line, 15, strlen(%line) - 15);
			%pos = strpos(%line, "\"");
			%dbName = getSubStr(%line, 0, %pos);

			%db = $uiNameTable_Music[%dbName];

			if ($LoadingBricks_Client == $LoadingBricks_BrickGroup)
				if (!isObject(%db))
					warn("WARNING: could not find music datablock for uiname \"" @ %dbName @ "\"");

			%quotaObject = getQuotaObjectFromBrick($LastLoadedBrick);
			setCurrentQuotaObject(%quotaObject);

			$LastLoadedBrick.setSound(%db);

			clearCurrentQuotaObject();
		}
	}
	else if (%firstWord $= "+-VEHICLE")
	{
		if (isObject($LastLoadedBrick))
		{
			%line = getSubStr(%line, 10, strlen(%line) - 10);
			%pos = strpos(%line, "\"");
			%dbName = getSubStr(%line, "0", %pos);

			%db = $uiNameTable_Vehicle[%dbName];

			if ($LoadingBricks_Client == $LoadingBricks_BrickGroup)
				if (!isObject(%db))
					warn("WARNING: could not find vehicle datablock for uiname \"" @ %dbName @ "\"");

			%line = getSubStr(%line, %pos + 2, strlen(%line) - %pos - 2);
			%recolorVehicle = getWord(%line, 0);

			%quotaObject = getQuotaObjectFromBrick($LastLoadedBrick);
			setCurrentQuotaObject(%quotaObject);

			$LastLoadedBrick.setVehicle(%db);
			$LastLoadedBrick.setReColorVehicle(%recolorVehicle);

			clearCurrentQuotaObject();
		}
	}
	else if (%firstWord $= "+-LOTID") {
		if(isObject($LastLoadedBrick)) {
			%line = getSubStr(%line, 8, strlen(%line) - 8);
			%line = trim(%line);
			$LastLoadedBrick.lotID = %line;
		}
	}
	else if (%firstWord $= "Linecount")
	{
		if (isObject(ProgressGui))
		{
			Progress_Bar.total = getWord(%line, 1);
			Progress_Bar.setValue(0);
			Progress_Bar.count = 0;
			Canvas.popDialog(ProgressGui);
			Progress_Window.setText("Loading Progress");
			Progress_Text.setText("Loading...");
		}
	}
	else if (%firstWord $= "+-OWNER")
	{
		if (isObject($LastLoadedBrick))
		{
			if ($LoadingBricks_Ownership == 1)
			{
				%ownerBLID = mAbs(mFloor(getWord(%line, 1)));
				%oldGroup = $LastLoadedBrick.getGroup();

				if ($Server::LAN)
					$LastLoadedBrick.bl_id = %ownerBLID;
				else
				{
					if (%ownerBLID != 999999)
					{
						%ownerBrickGroup = "BrickGroup_" @ %ownerBLID;

						if (isObject(%ownerBrickGroup))
							%ownerBrickGroup = %ownerBrickGroup.getId();
						else
						{
							%ownerBrickGroup = new SimGroup("BrickGroup_" @ %ownerBLID){};
							%ownerBrickGroup.client = 0;
							%ownerBrickGroup.name = "\c1BL_ID: " @ %ownerBLID @ "\c1\c0";
							%ownerBrickGroup.bl_id = %ownerBLID;
							mainBrickGroup.add(%ownerBrickGroup);
						}

						if (isObject(%ownerBrickGroup))
							%ownerBrickGroup.add($LastLoadedBrick);

						if (isObject(brickSpawnPointData))
							if ($LastLoadedBrick.getDataBlock().getId() == "brickSpawnPointData".getId())
								if (%ownerBrickGroup != %oldGroup)
								{
									%oldGroup.removeSpawnBrick($LastLoadedBrick);
									%ownerBrickGroup.addSpawnBrick($LastLoadedBrick);
								}
					}
				}

			}
		}
	}
	else
	{
		if (getBrickCount() >= $Pref::Server::BrickLimit)
		{
			MessageAll('', 'Brick limit reached (%1)', $Pref::Server::BrickLimit);
			ServerLoadSaveFile_End();
			return;
		}

		%quotePos = strstr(%line, "\"");

		if (%quotePos <= 0)
		{
			error("ERROR: ServerLoadSaveFile_Tick() - Bad line \"" @ %line @ "\" - expected brick line but found no uiname");
			return;
		}

		%uiName = getSubStr(%line, 0, %quotePos);
		%db = $uiNameTable[%uiName];

		%line = getSubStr(%line, %quotePos + 2, 9999);
		%pos = getWords(%line, 0, 2);
		%angId = getWord(%line, 3);
		%isBaseplate = getWord(%line, 4);
		%colorId = $colorTranslation[mFloor(getWord(%line, 5))];
		%printName = getWord(%line, 6);

		if (strpos(%printName, "/") != -1)
		{
			%printName = fileBase(%printName);
			%aspectRatio = %db.printAspectRatio;
			%printIDName = %aspectRatio @ "/" @ %printName;
			%printId = $printNameTable[%printIDName];

			if (%printId $= "")
				%printId = $printNameTable["Letters/-space"];
		}
		else
			%printId = $printNameTable[%printName];

		%colorFX = getWord(%line, 7);
		%shapeFX = getWord(%line, 8);
		%rayCasting = getWord(%line, 9);
		%collision = getWord(%line, 10);
		%rendering = getWord(%line, 11);

		%pos = VectorAdd(%pos, $LoadingBricks_PositionOffset);

		if (%db)
		{
			%trans = %pos;

			if (%angId == 0)
				%trans = %trans SPC " 1 0 0 0";
			else if (%angId == 1)
				%trans = %trans SPC " 0 0 1" SPC $piOver2;
			else if (%angId == 2)
				%trans = %trans SPC " 0 0 1" SPC $pi;
			else if (%angId == 3)
				%trans = %trans SPC " 0 0 -1" SPC $piOver2;

			%b = new fxDTSBrick()
			{
				datablock = %db;
				angleID = %angId;
				isBaseplate = %isBaseplate;
				colorID = %colorId;
				printID = %printId;
				colorFxID = %colorFX;
				shapeFxID = %shapeFX;
				isPlanted = 1;
			};

			if (isObject($LoadingBricks_BrickGroup))
				$LoadingBricks_BrickGroup.add(%b);
			else
			{
				error("ERROR: ServerLoadSaveFile_Tick() - $LoadingBricks_BrickGroup does not exist!");
				MessageAll('', "ERROR: ServerLoadSaveFile_Tick() - $LoadingBricks_BrickGroup does not exist!");
				%b.delete();
				ServerLoadSaveFile_End();
				return;
			}

			%b.setTransform(%trans);
			%b.trustCheckFinished();
			$LastLoadedBrick = %b;

			%err = %b.plant();

			if (%err == 1 || %err == 3 || %err == 5)
			{
				$Load_failureCount++;
				%b.delete();
				$LastLoadedBrick = 0;
			}
			else
			{
				if (%rayCasting !$= "")
					%b.setRayCasting(%rayCasting);

				if (%collision !$= "")
					%b.setColliding(%collision);

				if (%rendering !$= "")
					%b.setRendering(%rendering);
			}

			if ($LoadingBricks_Ownership && !$Server::LAN)
			{
				%oldGroup = %b.getGroup();
				%ownerGroup = "";

				if (%b.getNumDownBricks())
				{
					%ownerGroup = %b.getDownBrick(0).getGroup();
					%ownerGroup.add(%b);
				}
				else if (%b.getNumUpBricks())
				{
					%ownerGroup = %b.getUpBrick(0).getGroup();
					%ownerGroup.add(%b);
				}

				if (isObject(brickSpawnPointData))
				{
					if (%b.getDataBlock().getId() == brickSpawnPointData.getId())
					{
						if (%ownerGroup > 0 && %ownerGroup != %oldGroup)
						{
							%oldGroup.removeSpawnBrick(%b);
							%ownerGroup.addSpawnBrick(%b);
						}
					}
				}
			}
			else
				$LastLoadedBrick.client = $LoadingBricks_Client;
		}
		else
		{
			if (!$Load_MissingBrickWarned[%uiName])
			{
				warn("WARNING: loadBricks() - DataBlock not found for brick named \"", %uiName, "\"");
				$Load_MissingBrickWarned[%uiName] = 1;
			}

			$LastLoadedBrick = 0;
			$Load_failureCount++;
		}

		$Load_brickCount++;

		if (isObject(ProgressGui))
		{
			Progress_Bar.count++;
			Progress_Bar.setValue((Progress_Bar.count / Progress_Bar.total));

			if ((Progress_Bar.count + 1) == Progress_Bar.total)
				Canvas.popDialog(ProgressGui);
		}
	}

	if (!$Server_LoadFileObj.isEOF())
	{
		if ($Server::ServerType $= "SinglePlayer")
			$LoadSaveFile_Tick_Schedule = schedule(0, 0, "ServerLoadSaveFile_Tick");
		else
			$LoadSaveFile_Tick_Schedule = schedule(0, 0, "ServerLoadSaveFile_Tick");
	}
	else
		ServerLoadSaveFile_End();
}

function ServerLoadSaveFile_End()
{
	if ($GameModeArg !$= "")
	{
		if($LoadingBricks_Client == $LoadingBricks_BrickGroup)
		{
			%count = ClientGroup.getCount();
			for(%i = 0; %i < %count; %i++)
			{
				%client = ClientGroup.getObject(%i);
				%client.hereAtInitialLoad = 1;
			}
		}
	}

	$LoadingBricks_Client = "";
	$LoadingBricks_ColorMethod = "";

	if($Server::LAN == 0 && doesAllowConnections())
		startRaytracer();

	%time = getSimTime() - $LoadingBricks_StartTime;

	if(!$LoadingBricks_Silent)
		MessageAll('MsgProcessComplete', ($Load_brickCount - $Load_failureCount) @ " / " @ $Load_brickCount @ " bricks created in " @ getTimeString(mFloor(%time / 1000 * 100) / 100));

	deleteVariables("$Load_*");
	$LoadingBricks_Silent = 0;

	if(isEventPending($LoadSaveFile_Tick_Schedule))
		cancel($LoadSaveFile_Tick_Schedule);

	if(isEventPending($LoadingBricks_TimeoutSchedule))
		cancel($LoadingBricks_TimeoutSchedule);

	$Server_LoadFileObj.delete();
	$Server_LoadFileObj = 0;
}