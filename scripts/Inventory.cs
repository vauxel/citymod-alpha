// ============================================================
// Project				-		CityMod
// Description			-		Inventory Code
// ============================================================
// Sections
// 	 1: Functions
// ============================================================

// ============================================================
// Section 1 - Functions
// ============================================================

function getInventoryPropertyValue(%properties, %search)
{
	if((%properties $= "") || (%search $= ""))
		return 0;

	%properties = parseTokenString(%properties, ",");

	for(%i = 0; %i < getFieldCount(%properties); %i++)
	{
		%property = parseTokenString(getField(%properties, %i), "=");

		if(strUpr(getWord(%property, 0)) $= strUpr(%search))
			return getWord(%property, 1);
	}

	return 0;
}

function gameConnection::updateInventory(%client)
{
	servercmdCM_requestInventory(%client);
}

function gameConnection::isInventoryFull(%client)
{
	%one = getWord(CMPlayerData.data[%client.bl_id].Value["Inventory"], 0);
	%two = getWord(CMPlayerData.data[%client.bl_id].Value["Inventory"], 1);
	%three = getWord(CMPlayerData.data[%client.bl_id].Value["Inventory"], 2);
	%four = getWord(CMPlayerData.data[%client.bl_id].Value["Inventory"], 3);
	%five = getWord(CMPlayerData.data[%client.bl_id].Value["Inventory"], 4);
	%six = getWord(CMPlayerData.data[%client.bl_id].Value["Inventory"], 5);

	if((%one !$= "NONE:0") && (%two !$= "NONE:0") && (%three !$= "NONE:0") && (%four !$= "NONE:0") && (%five !$= "NONE:0") && (%six !$= "NONE:0"))
		return true;
	else
		return false;
}

//Returns the Value & Type for an Item of a slot in a client's Inventory
function gameConnection::getInventory(%client, %slot, %item)
{
	if(%slot $= "")
		return;

	// Tokenizing system to store
	// a type and value in the same field
	// while having multiple fields in the
	// same string.

	%BackpackType[1] = getWord(parseTokenString(getWord(CMPlayerData.data[%client.bl_id].Value["Inventory"], 0)), 0);
	%BackpackValue[1] = getWord(parseTokenString(getWord(CMPlayerData.data[%client.bl_id].Value["Inventory"], 0)), 1);

	%BackpackType[2] = getWord(parseTokenString(getWord(CMPlayerData.data[%client.bl_id].Value["Inventory"], 1)), 0);
	%BackpackValue[2] = getWord(parseTokenString(getWord(CMPlayerData.data[%client.bl_id].Value["Inventory"], 1)), 1);

	%BackpackType[3] = getWord(parseTokenString(getWord(CMPlayerData.data[%client.bl_id].Value["Inventory"], 2)), 0);
	%BackpackValue[3] = getWord(parseTokenString(getWord(CMPlayerData.data[%client.bl_id].Value["Inventory"], 2)), 1);

	%BackpackType[4] = getWord(parseTokenString(getWord(CMPlayerData.data[%client.bl_id].Value["Inventory"], 3)), 0);
	%BackpackValue[4] = getWord(parseTokenString(getWord(CMPlayerData.data[%client.bl_id].Value["Inventory"], 3)), 1);

	%BackpackType[5] = getWord(parseTokenString(getWord(CMPlayerData.data[%client.bl_id].Value["Inventory"], 4)), 0);
	%BackpackValue[5] = getWord(parseTokenString(getWord(CMPlayerData.data[%client.bl_id].Value["Inventory"], 4)), 1);

	%BackpackType[6] = getWord(parseTokenString(getWord(CMPlayerData.data[%client.bl_id].Value["Inventory"], 5)), 0);
	%BackpackValue[6] = getWord(parseTokenString(getWord(CMPlayerData.data[%client.bl_id].Value["Inventory"], 5)), 1);

	// If the 1st Parameter, %slot, equals
	// "all" then search through the 3 slots
	// and return the slot's item's value 
	// that corresponds to the item name
	// supplied in the 2nd Parameter, %item.
	// If no slot containing the item name
	// given is found then return 0.

	%total = 0;
	if(%slot $= "all")
	{
		%item = strUpr(strReplace(%item, " ", "_"));

		if(%BackPackType[1] $= %item)
		{
			%total += %BackPackValue[1]; 
		}
		if(%BackPackType[2] $= %item)
		{
			%total += %BackPackValue[2]; 
		}
		if(%BackPackType[3] $= %item)
		{
			%total += %BackPackValue[3];
		}
		if(%BackPackType[4] $= %item)
		{
			%total += %BackPackValue[4];
		}
		if(%BackPackType[5] $= %item)
		{
			%total += %BackPackValue[5];
		}
		if(%BackPackType[6] $= %item)
		{
			%total += %BackPackValue[6];
		}

		return %total;
	}
	else
	{
		// Otherwise, if %slot does not equal
		// to "all" then return the type of 
		// item and value of that item contained 
		// in the slot matching the variable %slot.
		// There are 6 slots available.

		if(!isInteger(%slot))
		{
			switch$(%slot)
			{
				case "one": %slot = 1;
				case "two": %slot = 2;
				case "three": %slot = 3;
				case "four": %slot = 4;
				case "five": %slot = 5;
				case "six": %slot = 6;
			}
		}

		if(!isInteger(%slot))
			return 0;

		return %BackPackType[%slot] SPC %BackPackValue[%slot];
	}
}

//Function to sort an "item" into its proper variable position
function gameConnection::addInventory(%client, %item, %amt, %word, %secret, %properties)
{
	if(%item $= "")
		return;

	if(%amt $= "")
		%amt = 1;
	
	if(%word $= "")
		%word = "received";

	%item = strUpr(strReplace(%item, " ", "_"));

	// Tokenizing system to store
	// a type and value in the same field
	// while having multiple fields in the
	// same string.

	%BackpackType[1] = getWord(parseTokenString(getWord(CMPlayerData.data[%client.bl_id].Value["Inventory"], 0)), 0);
	%BackpackValue[1] = getWord(parseTokenString(getWord(CMPlayerData.data[%client.bl_id].Value["Inventory"], 0)), 1);

	%BackpackType[2] = getWord(parseTokenString(getWord(CMPlayerData.data[%client.bl_id].Value["Inventory"], 1)), 0);
	%BackpackValue[2] = getWord(parseTokenString(getWord(CMPlayerData.data[%client.bl_id].Value["Inventory"], 1)), 1);

	%BackpackType[3] = getWord(parseTokenString(getWord(CMPlayerData.data[%client.bl_id].Value["Inventory"], 2)), 0);
	%BackpackValue[3] = getWord(parseTokenString(getWord(CMPlayerData.data[%client.bl_id].Value["Inventory"], 2)), 1);

	%BackpackType[4] = getWord(parseTokenString(getWord(CMPlayerData.data[%client.bl_id].Value["Inventory"], 3)), 0);
	%BackpackValue[4] = getWord(parseTokenString(getWord(CMPlayerData.data[%client.bl_id].Value["Inventory"], 3)), 1);

	%BackpackType[5] = getWord(parseTokenString(getWord(CMPlayerData.data[%client.bl_id].Value["Inventory"], 4)), 0);
	%BackpackValue[5] = getWord(parseTokenString(getWord(CMPlayerData.data[%client.bl_id].Value["Inventory"], 4)), 1);

	%BackpackType[6] = getWord(parseTokenString(getWord(CMPlayerData.data[%client.bl_id].Value["Inventory"], 5)), 0);
	%BackpackValue[6] = getWord(parseTokenString(getWord(CMPlayerData.data[%client.bl_id].Value["Inventory"], 5)), 1);

	if(%client.isInventoryFull())
	{
		//createDroppable(%item, %client.player, %amt);
		%client.CMNotify("Inventory", "Your Inventory is Full!", "information");
		return -1;
	}

	for(%i = 1; %i <= 6; %i++)
	{
		%bp = getWord(parseTokenString(getWord(CMPlayerData.data[%client.bl_id].Value["Inventory"], %i - 1)), 2);
		if(((%BackPackType[%i] $= %item) || (%BackPackType[%i] $= "NONE")) && (%BackpackValue[%i] < $CM::Prefs::Default::MaxInventorySpace) && !getInventoryPropertyValue(%bp, "NOSTACK"))
		{
			%match = %i;
			break;
		}
	}

	if(%amt + %BackPackValue[%match] >= $CM::Prefs::Default::MaxInventorySpace)
		%amt = $CM::Prefs::Default::MaxInventorySpace - %BackPackValue[%match];

	CMPlayerData.data[%client.bl_id].Value["Inventory"] = setWord(
		CMPlayerData.data[%client.bl_id].Value["Inventory"], 
		%match - 1, 
		(%item @ ":" @ (%BackPackValue[%match] + %amt) @ ((%properties !$= "") ? (":" @ strReplace(%properties, " ", ",")) : ""))
	);

	if(!%secret)
		%client.CMNotify("Inventory", "You" SPC %word SPC %amt SPC properText(%item), "add");

	%client.updateInventory();
}

//Same as the above but does the opposite, removes from the inventory
function gameConnection::removeInventory(%client, %item, %amt, %word, %drop, %secret)
{
	if(%item $= "")
		return;

	if(%amt $= "")
		%amt = 1;
	
	if(%word $= "")
		%word = "removed from your Inventory";

	%item = strUpr(strReplace(%item, " ", "_"));
	
	// Tokenizing system to store
	// a type and value in the same field
	// while having multiple fields in the
	// same string.

	%BackpackType[1] = getWord(parseTokenString(getWord(CMPlayerData.data[%client.bl_id].Value["Inventory"], 0)), 0);
	%BackpackValue[1] = getWord(parseTokenString(getWord(CMPlayerData.data[%client.bl_id].Value["Inventory"], 0)), 1);

	%BackpackType[2] = getWord(parseTokenString(getWord(CMPlayerData.data[%client.bl_id].Value["Inventory"], 1)), 0);
	%BackpackValue[2] = getWord(parseTokenString(getWord(CMPlayerData.data[%client.bl_id].Value["Inventory"], 1)), 1);

	%BackpackType[3] = getWord(parseTokenString(getWord(CMPlayerData.data[%client.bl_id].Value["Inventory"], 2)), 0);
	%BackpackValue[3] = getWord(parseTokenString(getWord(CMPlayerData.data[%client.bl_id].Value["Inventory"], 2)), 1);

	%BackpackType[4] = getWord(parseTokenString(getWord(CMPlayerData.data[%client.bl_id].Value["Inventory"], 3)), 0);
	%BackpackValue[4] = getWord(parseTokenString(getWord(CMPlayerData.data[%client.bl_id].Value["Inventory"], 3)), 1);

	%BackpackType[5] = getWord(parseTokenString(getWord(CMPlayerData.data[%client.bl_id].Value["Inventory"], 4)), 0);
	%BackpackValue[5] = getWord(parseTokenString(getWord(CMPlayerData.data[%client.bl_id].Value["Inventory"], 4)), 1);

	%BackpackType[6] = getWord(parseTokenString(getWord(CMPlayerData.data[%client.bl_id].Value["Inventory"], 5)), 0);
	%BackpackValue[6] = getWord(parseTokenString(getWord(CMPlayerData.data[%client.bl_id].Value["Inventory"], 5)), 1);

	%match = "none";

	if((%BackpackType[1] $= "NONE") && (%BackpackType[2] $= "NONE") && (%BackpackType[3] $= "NONE") && 
	(%BackpackType[3] $= "NONE") && (%BackpackType[3] $= "NONE") && (%BackpackType[3] $= "NONE"))
	{
		%match = "empty";
	}
	else
	{
		for(%i = 1; %i <= 6; %i++)
		{
			if((%BackPackType[%i] $= %item) && (%BackpackValue[%i] > 0))
			{
				%match = %i;
				break;
			}
		}
	}
	
	if(isInteger(%match))
	{
		if(%BackPackValue[%match] - %amt <= 0)
			%amt = %BackPackValue[%match];

		CMPlayerData.data[%client.bl_id].Value["Inventory"] = setWord(CMPlayerData.data[%client.bl_id].Value["Inventory"], %match - 1, (%item @ ":" @ (%BackPackValue[%match] - %amt)));

		if(%BackPackValue[%match] - %amt $= "0")
			CMPlayerData.data[%client.bl_id].Value["Inventory"] = setWord(CMPlayerData.data[%client.bl_id].Value["Inventory"], %match - 1, "NONE:0");
	}
	else
	{
		if(%match $= "empty")
			%client.CMNotify("Inventory", "Your Inventory is Empty!", "information");
		else if(%match $= "none")
			%client.CMNotify("Inventory", "You don't have any" SPC properText(%item), "information");

		return -1;
	}

	if(%drop)
		createDroppable(%item, %client.player, %amt);

	if(!%secret)
		%client.CMNotify("Inventory", %amt SPC properText(%item) SPC "was" SPC %word, "information");

	%client.updateInventory();
}

//Drops-Removes a whole slot from a player's inventory
function gameConnection::removeInventorySlot(%client, %slot, %word, %drop, %secret)
{
	if(%slot $= "")
		return;

	if(%word $= "")
		%word = "removed from your Inventory";
	
	// Tokenizing system to store
	// a type and value in the same field
	// while having multiple fields in the
	// same string.

	%BackpackType = getWord(parseTokenString(getWord(CMPlayerData.data[%client.bl_id].Value["Inventory"], %slot - 1)), 0);
	%BackpackValue = getWord(parseTokenString(getWord(CMPlayerData.data[%client.bl_id].Value["Inventory"], %slot - 1)), 1);

	if((%BackpackType $= "NONE") || (%BackpackValue $= "0"))
	{
		%client.CMNotify("Inventory", "This Slot is Empty!", "information");
		return -1;
	}
	
	CMPlayerData.data[%client.bl_id].Value["Inventory"] = setWord(CMPlayerData.data[%client.bl_id].Value["Inventory"], %slot - 1, "NONE:0");

	if(%drop)
		createDroppable(%BackpackType, %client.player, %BackpackValue);

	if(!%secret)
		%client.CMNotify("Inventory", %BackpackValue SPC %BackpackType SPC "was" SPC %word, "information");

	%client.updateInventory();
}

function gameConnection::getInventoryItemProperties(%client, %slot)
{
	if(%slot $= "")
		return;

	%SlotProperties = parseTokenString(getWord(parseTokenString(getWord(CMPlayerData.data[%client.bl_id].Value["Inventory"], 0)), 2), ",");

	if(%SlotProperties $= "")
		return;

	%returnstring = "";

	for(%i = 0; %i < getWordCount(%SlotProperties); %i++)
	{
		%returnstring = setField(%returnstring, %i, parseTokenString(getWord(%SlotProperties, %i), "="));
	}

	return %returnstring;
}