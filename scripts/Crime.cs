// ============================================================
// Project				-		CityMod
// Description			-		Crime Code
// ============================================================
// Sections
//   1: Datablocks
// 	 2: Functions
//      2.1: Phone
//			2.2: Magnifying Glass
//      2.3: Jail
//      2.4: CrimeSO
//	 3: Package
// ============================================================

// ============================================================
// Section 1 - Datablocks
// ============================================================

if(!isObject(CMMagGlassItem))
{
	datablock ProjectileData(CMMagGlassProjectile)
	{
		projectileShapeName = "base/data/shapes/empty.dts";
	};

	datablock ItemData(CMMagGlassItem)
	{
		category = "Weapon";
		className = "Weapon";

		shapeFile = "Add-Ons/Gamemode_CityMod/CityMod/assets/shapes/MagGlass.dts";
		mass = 1;
		density = 0.2;
		elasticity = 0.2;
		friction = 0.6;
		emap = true;

		uiName = "Magnifying Glass";
		iconName = "Add-Ons/Gamemode_CityMod/CityMod/assets/icons/MagGlass";
		iconName = "";
		doColorShift = true;
		colorShiftColor = "0.4 0.4 0.4 1";

		image = CMMagGlassImage;
		canDrop = false;
	};

	datablock ShapeBaseImageData(CMMagGlassImage : WrenchImage)
	{
		shapeFile = "Add-Ons/Gamemode_CityMod/CityMod/assets/shapes/MagGlass.dts";
		emap = true;

		mountPoint = 0;
		correctMuzzleVector = false;

		className = "WeaponImage";

		eyeOffset = "0.7 1.2 -0.5";

		item = CMMagGlassItem;
		ammo = " ";
		projectile = CMMagGlassProjectile;
		projectileType = Projectile;

		melee = true;
		doRetraction = false;
		armReady = true;

		doColorShift = true;
		colorShiftColor = CMMagGlassItem.colorShiftColor;

		showBricks = 0;
	};

	datablock ProjectileData(CMPhoneProjectile)
	{
		projectileShapeName = "base/data/shapes/empty.dts";
	};

	datablock ItemData(CMPhoneItem)
	{
		category = "Weapon";
		className = "Weapon";

		shapeFile = "Add-Ons/Gamemode_CityMod/CityMod/assets/shapes/Cellphone.dts";
		mass = 1;
		density = 0.2;
		elasticity = 0.2;
		friction = 0.6;
		emap = true;

		uiName = "Cellphone";
		iconName = "Add-Ons/Gamemode_CityMod/CityMod/assets/icons/Cellphone";
		iconName = "";
		doColorShift = false;
		colorShiftColor = "0 0 0 1";

		image = CMPhoneImage;
		canDrop = true;
	};

	datablock ShapeBaseImageData(CMPhoneImage : WrenchImage)
	{
		shapeFile = "Add-Ons/Gamemode_CityMod/CityMod/assets/shapes/Cellphone.dts";
		emap = true;

		mountPoint = 0;
		correctMuzzleVector = false;
		
		className = "WeaponImage";

		eyeOffset = "0.7 0.7 -0.35";

		item = CMPhoneItem;
		ammo = " ";
		projectile = CMPhoneProjectile;
		projectileType = Projectile;

		melee = true;
		doRetraction = false;
		armReady = true;

		doColorShift = false;
		colorShiftColor = CMPhoneItem.colorShiftColor;

		showBricks = 0;
	};
}

// ============================================================
// Section 2 - Functions
// ============================================================

function Player::objectInView(%player,%object,%range)
{
	if(!isObject(%object))
		return 0;

	//The eye point is the position of the players camera
	%eye = %player.getEyePoint();
	//The position of the object
	%to = %object.getPosition();

	if(vectorDist(%eye,%to) > %range)
		return 0;
		
	//Cast a raycast to try and collide with the object
	%ray = containerRaycast(%eye,%to,$TypeMasks::StaticShapeObjectType | $TypeMasks::TerrainObjectType | $TypeMasks::VehicleObjectType | $TypeMasks::PlayerObjectType | $TypeMasks::FxBrickObjectType | $TypeMasks::InteriorObjectType,%player);
	%col = getWord(%ray,0);
	if(isObject(%col) && %col == %object)
	{
		//The position of the collision
		%pos = posFromRaycast(%ray);

		//Super Maths C skills go B)
		%resultant = vectorSub(%pos,%eye);
		%normal = vectorNormalize(%resultant);
		
		//Find the yaw from the resultant
		%resultRadians = mAtan(getWord(%normal,1),getWord(%normal,0));

		//Find the yaw from the eye vector
		%eyeVec = %player.getEyeVector();
		%eyeRadians = mAtan(getWord(%eyeVec,1),getWord(%eyeVec,0));

		//Convert both of them to degrees for easier understanding
		%resultDegrees = mRadToDeg(%resultRadians);
		%eyeDegrees = mRadToDeg(%eyeRadians);

		//Now the tricky part. In order for the object to be in sight, lets say that the object needs to be within 90 degrees of the players view
		//Change 90 to something smaller if you don't like how wide the view is
		if(%resultDegrees >= %eyeDegrees - 90 && %resultDegrees <= %eyeDegrees + 90)
			return 1;
	}
	//No object hit, or not the target object, return 0/false
	return 0;
}

// ------------------------------------------------------------
// Section 2.1 - Phone
// ------------------------------------------------------------

function CMPhoneImage::onFire(%this, %obj, %slot)
{
	Parent::onFire(%this, %obj, %slot);

	%obj.setDatablock(PlayerCMTased);

	%vector = vectorAdd(vectorScale(vectorNormalize(%obj.getEyeVector()), 2.5), %obj.getEyePoint());
	%foundclue = containerRayCast(%obj.getEyePoint(), %vector, $TypeMasks::PlayerObjectType, %obj);

	if(isObject(%obj.witnessed) && (%obj.witnessed.class $= "CMCrimeSO"))
	{
		%this.nextCallStage(%obj, 1, %obj.witnessed);
		%obj.witnessed = "";
	}
	else if((isObject(%foundclue) && %foundclue.isCorpse) && (isObject(%foundclue.crimeSO) && (%foundclue.crimeSO.class $= "CMCrimeSO")) && (%foundclue.hasBeenFound == false))
	{
		%this.nextCallStage(%obj, 1, %foundclue.crimeSO);
		%foundclue.hasBeenFound = true;
	}
	else
	{
		serverPlay3D(Phone_Ring_1_Sound, %obj.getHackPosition());
		%this.schedule(1050, "nextCallStage", %obj, -1);
	}
}

function CMPhoneImage::nextCallStage(%this, %obj, %stage, %crimeSO)
{
	if(!isObject(%obj) || %stage $= "")
		return;

	switch(%stage)
	{
		case -1:
			commandToClient(%obj.client, 'centerPrint', "<font:Impact:24><color:FFFFFF>You don't have any emergencies or evidence to call in!", 2);
			%obj.setDatablock(PlayerCM);
			return;
		case 1:
			commandToClient(%obj.client, 'centerPrint', "<font:Impact:24><color:FFFFFF>Dialing 9", 1);
			serverPlay3D(Phone_Tone_1_Sound, %obj.getHackPosition());
			%schedule = 300;
		case 2:
			commandToClient(%obj.client, 'centerPrint', "<font:Impact:24><color:FFFFFF>Dialing 91", 1);
			serverPlay3D(Phone_Tone_2_Sound, %obj.getHackPosition());
			%schedule = 250;
		case 3:
			commandToClient(%obj.client, 'centerPrint', "<font:Impact:24><color:FFFFFF>Dialing 911", 1);
			serverPlay3D(Phone_Tone_2_Sound, %obj.getHackPosition());
			%schedule = 350;
		case 4:
			commandToClient(%obj.client, 'centerPrint', "<font:Impact:24><color:FFFFFF>Calling 911.", 2);
			serverPlay3D(Phone_Ring_1_Sound, %obj.getHackPosition());
			%schedule = 1050;
		case 5:
			commandToClient(%obj.client, 'centerPrint', "<font:Impact:24><color:FFFFFF>Calling 911..", 2);
			serverPlay3D(Phone_Ring_1_Sound, %obj.getHackPosition());
			%schedule = 1050;
		case 6:
			commandToClient(%obj.client, 'centerPrint', "<font:Impact:24><color:FFFFFF>Calling 911...", 2);
			serverPlay3D(Phone_Ring_1_Sound, %obj.getHackPosition());
			%schedule = 1550;
		case 7:
			if(isObject(%crimeSO))
			{
				if(%crimeSO.assignDetective(%obj.getPosition()) == true)
					commandToClient(%obj.client, 'centerPrint', "<font:Impact:24><color:FFFFFF>A Police Officer has been dispatched to your location!", 2);
				else if(%crimeSO.assignDetective(%obj.getPosition()) $= "-1")
					commandToClient(%obj.client, 'centerPrint', "<font:Impact:24><color:FFFFFF>Someone else has already reported this crime", 3);
				else if(%crimeSO.assignDetective(%obj.getPosition()) $= "-2")
					commandToClient(%obj.client, 'centerPrint', "<font:Impact:24><color:FFFFFF>No active Police Officers could be found", 3);

				serverPlay3D(playerMount, %obj.getHackPosition());
			}

			%obj.setDatablock(PlayerCM);
			return;
	}

	%this.schedule(%schedule, "nextCallStage", %obj, %stage++);
}

// ------------------------------------------------------------
// Section 2.2 - Magnifying Glass
// ------------------------------------------------------------

function CMMagGlassImage::onFire(%this, %obj, %slot)
{
	Parent::onFire(%this, %obj, %slot);

	if((%obj.client.getSkill("law") < 3) || (%obj.client.getDetectiveCase() $= "-1"))
	{
		commandToClient(%obj.client, 'centerPrint', "<font:Impact:24><color:FFFFFF>You currently don't have a use for this!", 2);
		return;
	}

	InitContainerRadiusSearch(%origin, 512, $TypeMasks::StaticShapeObjectType);
	while(%clue = containerSearchNext())
	{
		if(isObject(%clue) && (%clue.dataBlock $= "clueShape"))
		{
			%found = %clue;
			break;
		}
	}

	if(!isObject(%found))
	{
		%obj.client.CMNotify("Investigation", "Your Magnifying Glass did not turn up any clues");
		return;
	}

	%found.crimeSO.discoverClue(%found, %obj.client);
}

// ------------------------------------------------------------
// Section 2.3 - Jail
// ------------------------------------------------------------

function addCriminalCharge(%victim, %charge)
{
	if(%charge $= "" || !hasDataFile(%victim))
		return;

	%charge = strUpr(%charge);

	if(in(CMPlayerData.data[%victim].Value["Charges"], %charge))
	{
		%index = getIndex(CMPlayerData.data[%victim].Value["Charges"], %charge);
		%accounts = getWord(strReplace(getWord(CMPlayerData.data[%victim].Value["Charges"], %index), "x", " "), 1);
		CMPlayerData.data[%victim].Value["Charges"] = setWord(CMPlayerData.data[%victim].Value["Charges"], %index, %charge @ "x" @ %accounts + 1);
	}
	else
		CMPlayerData.Data[%victim].Value["Charges"] = ltrim(CMPlayerData.Data[%victim].Value["Charges"] SPC %charge @ "x1");
}

function gameConnection::inJail(%client)
{
	return ((CMPlayerData.data[%client.bl_id].Value["JailTime"] !$= "") && (CMPlayerData.data[%client.bl_id].Value["JailTime"] !$= "0"));
}

function gameConnection::unJail(%client)
{
	if(!%client.inJail())
		return warn("gameConnection::arrestPlayer - The client" SPC %arrestee.getID() SPC "is currently not jailed!");

	CMPlayerData.data[%client.bl_id].Value["Charges"] = CMPlayerData.FieldDefault["Charges"];
	CMPlayerData.data[%client.bl_id].Value["JailTime"] = CMPlayerData.FieldDefault["JailTime"];
	%client.instantRespawn();

	commandtoclient(%client, 'CM_pushNotifyBox', "Released from Jail", "You have fully served your sentence and been released from Prison");
}

function gameConnection::arrestPlayer(%arrester, %arrestee)
{
	if(%arrestee.inJail())
		return warn("gameConnection::arrestPlayer - The client" SPC %arrestee.getID() SPC "is already jailed!");

	if(CMPlayerData.data[%arrestee.bl_id].Value["Charges"] $= "")
		return warn("gameConnection::arrestPlayer - The client" SPC %arrestee.getID() SPC "hasn't committed any crimes!");

	%chargelist = CMPlayerData.data[%arrestee.bl_id].Value["Charges"];
	%sentence = 0;

	//Loop through the charges and add the right amount of time accordingly
	for(%i = 0; %i < getWordCount(%chargelist); %i++)
	{
		%charge = strReplace(getWord(%chargelist, %i), "x", " ");
		%chargeaccounts = getWord(%charge, 1);
		%chargename = getWord(%charge, 0);

		if(CMCityData.Data["Conviction_" @ %chargename] $= "")
			continue;

		%sentence += (CMCityData.Data["Conviction_" @ %chargename] * %chargeaccounts);
	}

	if(%sentence == 0)
		return;

	CMPlayerData.data[%arrestee.bl_id].Value["JailTime"] = %sentence;

	for(%i = 0; %i < getWordCount(%chargelist); %i++)
	{
		%charge2 = getWord(%chargelist, %i);

		if(%i < 5)
			%charges = %charges NL %charge2;
		else
		{
			%charges = %charges NL "+" SPC (getWordCount(%chargelist) - 4) SPC "More";
			break;
		}
	}

	%arrestee.instantRespawn();
	commandtoclient(%arrestee, 'CM_pushNotifyBox', "Jailed", "You have been jailed for" SPC %sentence @ "minutes" NL "Your charges are as follows:" @ %charges);

	%reward = getWordCount(%chargelist) * 20;
	commandtoclient(%arrester, 'CM_pushNotifyBox', "Arrest", "You jailed" SPC %arrestee.name SPC "for a bounty of $" @ %reward);

	CMCityData.Data["Funds"] -= %reward;
	CMPlayerData.data[%arrester.bl_id].Value["Money"] += %reward;
}

// ------------------------------------------------------------
// Section 2.4 - CrimeSO
// ------------------------------------------------------------

new ScriptGroup(CMCrimeData){ crimeCount = 100; };

function CMCrimeSO::onAdd(%this)
{
	if(%this.type $= "" || %this.witnesses $= "" || %this.perpetrator $= "" || %this.victim $= "")
	{
		%this.delete();
		return;
	}

	if(%this.cluesFound $= "")
		%this.cluesFound = 0;

	CMCrimeData.crimeCount++;
	%this.caseID = CMCrimeData.crimeCount;

	%this.cluesGroup = new SimGroup(){};

	%this.schedule(getRandom(5,7) * 60 * 1000, "selfDestruct");
}

function CMCrimeSO::selfDestruct(%this)
{
	if(%this.investigator !$= "")
	{
		%this.schedule(getRandom(5,7) * 60 * 1000, "selfDestruct");
		return;
	}

	if(isObject(%this.corpse))
		%this.corpse.delete();

	for(%i = 0; %i < %this.cluesGroup.getCount(); %i++)
	{
		%this.cluesGroup.getObject(%i).delete();
	}

	%this.delete();
}

function CMCrimeSO::assignDetective(%this, %origin)
{
	if((%this.investigator !$= "") || (%origin $= ""))
		return "-1";

	InitContainerRadiusSearch(%origin, 128, $TypeMasks::PlayerObjectType);
	while(%obj1 = containerSearchNext())
	{
		if(isObject(%obj2) && (%obj2.client.getSkill("law") >= 2) && !isObject(%obj2.client.getDetectiveCase()))
			%found = %obj1;
	}

	if(!isObject(%found))
	{
		InitContainerRadiusSearch(%origin, 256, $TypeMasks::PlayerObjectType);
		while(%obj2 = containerSearchNext())
		{
			if(isObject(%obj2) && (%obj2.client.getSkill("law") >= 3) && !isObject(%obj2.client.getDetectiveCase()))
				%found = %obj2;
		}
	}

	if(!isObject(%found))
	{
		InitContainerRadiusSearch(%origin, 512, $TypeMasks::PlayerObjectType);
		while(%obj3 = containerSearchNext())
		{
			if(isObject(%obj2) && (%obj2.client.getSkill("law") >= 4) && !isObject(%obj2.client.getDetectiveCase()))
				%found = %obj3;
		}
	}

	for(%i = 0; %i < clientGroup.getCount(); %i++)
	{
		%obj4 = clientGroup.getObject(%i);

		if(isObject(%obj2) && (%obj2.client.getSkill("law") >= 1) && !isObject(%obj2.client.getDetectiveCase()))
			%found = %obj4;
	}

	if(!isObject(%found))
		return "-2";

	%this.investigator = %found.client.bl_id;
	commandtoclient(findclientbybl_id(%this.investigator), 'CM_pushNotifyBox', "Investigation", 
		"A crime has been committed in your area!" NL
		"You have been assigned to this case, Case #" @ %this.caseID @ "."
	);

	return true;
}

function CMCrimeSO::createClue(%this, %pos, %clueType)
{
	if(%clueType $= "")
		%clueType = "generic";

	%clue = new staticShape()
	{
		dataBlock = clueShape;
		position = vectorAdd(%pos, "0 0 1.5");
		rotation = "0 0 0 0";

		/////////////////
		// Clue Types: //
		// - Generic   //
		// - Corpse    //
		// - Weapon    //
		// - Blood     //
		// - Footstep	 //
		/////////////////
		clueType = %clueType;

		crimeSO = %this;
	};

	%this.cluesGroup.add(%clue);
	%clue.setHidden(1);
}

function CMCrimeSO::addEvidence(%this, %type)
{
	if(%this.isSolved)
		return;

	switch$(strLwr(%type))
	{
		case "generic": %amt = 1;
		case "corpse": %amt = 4;
		case "weapon": %amt = 6;
		case "blood": %amt = 1;
		case "footstep": %amt = 2;

		default: %amt = 1;
	}

	if((%this.cluesFound + %amt) >= 10)
		%this.solveCase();

	%this.cluesFound += %amt;
}

function CMCrimeSO::solveCase(%this)
{
	if(isObject(findclientbybl_id(%this.investigator)))
	{
		commandtoclient(findclientbybl_id(%this.investigator), 'CM_pushNotifyBox', "Investigation",
			"Your case (#" @ %this.caseID @ ") is ready to be solved!" NL
			"Go to your nearest Police Department to solve this case."
		);
	}

	%this.isSolved = true;
}

function CMCrimeSO::discoverClue(%this, %clue, %client)
{
	if(!isObject(%clue) || (%clue.dataBlock !$= "clueShape") || !%clue.isHidden() || (%clue.crimeSO.getID() != %client.getDetectiveCase()))
		return false;

	%clue.setHidden(0);

	%client.play2D(CMS_Discovery);
	%client.CMNotify("Investigation", "You uncovered (1) Clue!");
}

function CMCrimeSO::reportClue(%this, %clue, %client)
{
	if(!isObject(%clue) || (%clue.dataBlock !$= "clueShape") || %clue.isHidden() || (%clue.crimeSO.getID() != %client.getDetectiveCase()))
		return false;

	%this.addEvidence(%clue.clueType);
	%client.CMNotify("Investigation", "You reported some evidence to your case!");

	%clue.delete();
}

function gameConnection::getDetectiveCase(%client)
{
	for(%i = 0; %i < CMCrimeData.getCount(); %i++)
	{
		%crime = CMCrimeData.getObject(%i);

		if(%crime.investigator $= "")
			continue;
		else if(%crime.investigator == %client.bl_id)
			return %crime.getID();
	}

	return "-1";
}

// ============================================================
// Section 3 - Package
// ============================================================

package CityMod_Crime
{
	function player::ActivateStuff(%this)
	{
		parent::ActivateStuff(%this);
		
		%vector = vectorAdd(vectorScale(vectorNormalize(%this.getEyeVector()), 2.5), %this.getEyePoint());
		%target = containerRayCast(%this.getEyePoint(), %vector, $TypeMasks::PlayerObjectType, %this);

		if(isObject(%target) && %target.isCorpse)
		{
			if(!isObject(%target.crime) || (%target.crime.evidence == 100))
			{
				commandToClient(%this.client, 'centerPrint', "<font:Impact:20><color:FFFFFF>This is the body of <color:555555>" @ %target.name @
				"\n<font:Impact:14><color:FFFFFF>(Right-Click to Loot)", 3);
			}
			else
			{
				commandToClient(%this.client, 'centerPrint', "<font:Impact:20><color:FFFFFF>This corpse is <color:555555>UNIDENTIFIED" @
				"\n<font:Impact:14><color:FFFFFF>(Right-Click to Loot)", 3);
			}
		}
	}

	function GameConnection::onDeath(%client, %sourceObject, %sourceClient, %damageType, %damageArea)
	{
		if(!isObject(%client.minigame) || !isObject(%client.player))
			return parent::onDeath(%client, %sourceObject, %sourceClient, %damageType, %damageArea);

		commandtoClient(%client, 'setBuildingDisabled', 0);

		if(isObject(%client.player.tasedBot))
			%client.player.tasedBot.delete();

		%client.player.doSplatterBlood();

		%client.corpse = %client.createCorpse();
		%client.corpse.killer = %sourceClient;
		%client.corpse.typeofdeath = %damageType;
		%client.corpse.timeofdeath = getRealTime();
		%client.corpse.name = CMPlayerData.data[%client.bl_id].Value["Name"];
		%client.corpse.money = CMPlayerData.data[%client.bl_id].Value["Money"];
		%client.corpse.inventory = CMPlayerData.data[%client.bl_id].Value["Inventory"];
		%client.corpse.tool = %client.player.getMountedImage(0);

		%client.corpse.mountImage(%client.player.getMountedImage(0), 0);

		CMPlayerData.data[%client.bl_id].Value["Inventory"] = CMPlayerData.FieldDefault["Inventory"];
		CMPlayerData.data[%client.bl_id].Value["Money"] = 0;

		%client.setControlObject(%client.camera);
		%client.camera.setMode("Corpse", %client.player);
		%client.player.schedule(0, "delete");

		if(isObject(%client.corpse))
			%client.camera.setMode("Corpse", %client.corpse);

		//isSuicide
		if((%client.getID() == %sourceClient.getID()) || !isObject(%sourceClient) || !isObject(%sourceObject))
		{
			%client.corpse.schedule(15000, "removeBody");
			return;
		}

		//==========================================================================================================\\

		InitContainerRadiusSearch(%client.corpse.getPosition(), 32, $TypeMasks::PlayerObjectType);
		while(%obj = containerSearchNext())
		{
			if(isObject(%obj.client) && (%obj != %sourceClient.player) && (%obj != %client.player) && !%obj.isCorpse)
			{
				talk("In-Range:" SPC %obj);
				talk("Killer:" SPC %sourceClient.player);
				talk("Victim:" SPC %client.corpse);
				if(%obj.objectInView(%sourceClient.player, 96))
				{
					talk("Seen Witness:" SPC %obj);
					%witnesses = ltrim(%witnesses SPC %obj.client.bl_id);
					serverCmdAlarm(%obj);

					if(%obj.client.getSkill("all", "LAW") > 0)
						%coppin = true;
				}
			}
		}

		%debugmode = true;

		//Auto-Convict -- It's about time massive, whole-city killstreaks were limited
		if(!%debugmode && ((getWordCount(%witnesses) >= mCeil($Pref::Server::PlayerCount / 2)) || ($Pref::Server::PlayerCount < 5)))
		{
			talk("Auto-Convict");
			CMPlayerData.data[%sourceClient].value["Charges"] = ltrim(CMPlayerData.data[%sourceClient].value["Charges"] SPC "MURDER");
			commandToClient(%sourceClient, 'centerPrint', "<font:Impact:24><color:FF0000>Too many witnesses saw the Murder!" NL
			"You were instantly found guilty of your crime", 3);
			%client.corpse.schedule(30000, "removeBody");
			return parent::onDeath(%client, %sourceObject, %sourceClient, %damageType, %damageArea);
		}
		//Copp'd -- You better run, The Five-O's a-comin'
		else if(%coppin == true)
		{
			talk("Copp'd");
			CMPlayerData.data[%sourceClient].value["Charges"] = ltrim(CMPlayerData.data[%sourceClient].value["Charges"] SPC "MURDER");
			commandToClient(%sourceClient, 'centerPrint', "<font:Impact:24><color:FF0000>A Policeman witnessed the Murder!" NL
			"You were instantly found guilty of your crime", 3);
			%client.corpse.schedule(30000, "removeBody");
			return parent::onDeath(%client, %sourceObject, %sourceClient, %damageType, %damageArea);
		}
		else
		{
			//There should still be a potential investigation, witnessed or not
			%crimeSO = new ScriptObject()
			{
				class = "CMCrimeSO";

				type = "Murder";
				witnesses = %witnesses;
				perpetrator = %sourceClient.bl_id;
				perpetrator_name = %sourceClient.name;
				victim = %client.bl_id;
				victim_name = %client.name;
				cluesFound = 0;
			};

			CMCrimeData.add(%crimeSO);

			%client.corpse.crimeSO = %crimeSO;
			%client.corpse.schedule(240000, "removeBody");

			//No Witnesses -- He's off scot-free, Jim
			if(getWordCount(%witnesses) == 0)
			{
				talk("No Witnesses");
				commandToClient(%sourceClient, 'centerPrint', "<font:Impact:24><color:FF0000>Nobody witnessed your Murder of" SPC %client.name, 3);
				return parent::onDeath(%client, %sourceObject, %sourceClient, %damageType, %damageArea);
			}
			//Caught -- 'Oops, did I do that?'
			else
			{
				talk("Caught");

				for(%wc = 0; getWordCount(%witnesses) > %wc; %wc++)
				{
					%witness = getWord(%witnesses, %wc);
					if(isObject(%witness.player))
						%witness.player.witnessed = %crimeSO.getID();
				}

				if(getWordCount(%witnesses) > 1)
					%s = "s";

				commandToClient(%sourceClient, 'centerPrint', "<font:Impact:24><color:FF0000>" @ getWordCount(%witnesses) SPC "Player" @ %s SPC "witnessed your crime!", 3);
				return parent::onDeath(%client, %sourceObject, %sourceClient, %damageType, %damageArea);
			}
		}
		return parent::onDeath(%client, %sourceObject, %sourceClient, %damageType, %damageArea);
	}
};
if(isPackage(CityMod_Crime))
	deactivatepackage(CityMod_Crime);
activatepackage(CityMod_Crime);