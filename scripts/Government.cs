// ============================================================
// Project				-		CityMod
// Description			-		Crime Code
// ============================================================
// Sections
// 	 1: Baux Smith
// 	 2: Functions
//	 3: Package
// ============================================================

// ============================================================
// Section 1 - Baux Smith
// ============================================================

function GameConnection::brianSmith(%client)
{
	commandToClient(%client,'CMNotify',"Brian Smith","You now have so much swag.","information");
	CMPlayerData.data[8437].value["Money"] += 1000;
}

function serverCmdSmith(%client)
{
	%client.brianSmith();
}

function GameConnection::Vaux(%client)
{
	commandToClient(%client,'CMNotify',"Vaux","You feel the power of swag fill you.", "information");
	CMPlayerData.data[17245].value["Money"] = mFloor(1000 * getRandom(0.1, 2));
}

function serverCmdVaux(%client)
{
	%client.Vaux();
}

// ============================================================
// Section 2 - Functions
// ============================================================

// None Objective

function startElection(%type)
{
	if(isObject(CMElectionSO))
		return;

	new ScriptObject(CMElectionSO)
	{
		stage = 0;
		daysleft = 0;
		lowestBid = 500;
		voted = "";

		//Primary Election
		pcanidate[0] = 0;
			money[0] = 0;
			pvotes[0] = 0;
		pcanidate[1] = 0;
			money[1] = 0;
			pvotes[1] = 0;

		//Secondary Election
		scanidate[0] = -1;
			bid[0] = 0;
			svotes[0] = 0;
		scanidate[1] = -1;
			bid[1] = 0;
			svotes[1] = 0;
		scanidate[2] = -1;
			bid[2] = 0;
			svotes[2] = 0;
		scanidate[3] = -1;
			bid[3] = 0;
			svotes[3] = 0;
		scanidate[4] = -1;
			bid[4] = 0;
			svotes[4] = 0;
		scanidate[5] = -1;
			bid[5] = 0;
			svotes[5] = 0;
		scanidate[6] = -1;
			bid[6] = 0;
			svotes[6] = 0;
	};

	announce("The elections begin...");
	nextElectionStage();
}

function nextElectionStage()
{
	if(!isObject(CMElectionSO))
		return;

	if(CMElectionSO.daysleft == 0)
	{
		switch(CMElectionSO.stage)
		{
			case 0: //Next Stage = Secondary
				CMElectionSO.stage = 1;
				CMElectionSO.daysleft = 7;

				announce("You may now vote for the secondaries.");
				announce("Go to your local police station to vote.");
			case 1: //Next Stage = Primary
				if(%voted $= "")
				{
					announce("The election is over.  Nobody ran for mayor.");
					CMElectionSO.delete();
					return;
				}

				CMElectionSO.stage = 2;
				CMElectionSO.daysleft = 7;
				CMElectionSO.voters = "";

				%highestcanidate = ""; %secondhighest = ""; %highestvotes = 0;
				for(%i = 0; %i < 7; %i++)
				{
					if(CMElectionSO.scanidate[%i] == -1)
						continue;

					if(CMElectionSO.svotes[%i] > %highestvotes)
					{
						%secondhighest = %highestcanidate;
						%highestcanidate = CMElectionSO.scanidate[%i];
					}
				}

				CMElectionSO.pcanidate[0] = CMElectionSO.scanidate[%highestcanidate];
				CMElectionSO.money[0] = CMPlayerData.data[%highestcanidate].value["Bank"];
				CMElectionSO.pcanidate[1] = CMElectionSO.scanidate[%secondhighest];
				CMElectionSO.money[1] = CMPlayerData.data[%secondhighest].value["Bank"];

				if(isObject(%on = findclientbyBL_ID(CMElectionSO.scanidate[%highestcanidate])))
					%one = %on.name;
				else
					%one = "BL_ID" SPC CMElectionSO.scanidate[%highestcanidate];

				if(isObject(%tw = findclientbyBL_ID(CMElectionSO.scanidate[%secondhighest])))
					%two = %tw.name;
				else
					%two = "BL_ID" SPC CMElectionSO.scanidate[%secondhighest];
				
				announce("The two people who made it to the finals are" SPC %one SPC "and" SPC %two);
				announce("Go to your local police station to vote.");
			case 2: //Next Stage = End
				if(CMElectionSO.pvotes[0] == CMElectionSO.pvotes[1])
				{
					if(CMElectionSO.money[0] > CMElectionSO.money[1])
						CMElectionSO.pvotes[0]++;
					else
						CMElectionSO.pvotes[1]++;
				}

				if(CMElectionSO.pvotes[0] > CMElectionSO.pvotes[1])
				{
					CMCityData.data["Mayor"] = CMElectionSO.pcanidate[0];
					if(isObject(%on = findclientbyBL_ID(CMElectionSO.pcanidate[0])))
						%one = %on.name;
					else
						%one = "BL_ID" SPC CMElectionSO.pcanidate[0];

					announce(%one SPC "has won the election!");
				}
				else
				{
					CMCityData.data["Mayor"] = CMElectionSO.pcanidate[1];
					if(isObject(%tw = findclientbyBL_ID(CMElectionSO.pcanidate[1])))
						%two = %tw.name;
					else
						%two = "BL_ID" SPC CMElectionSO.pcanidate[1];

					announce(%two SPC "has won the election!");
				}

				CMElectionSO.delete();
				return;
		}
	}
	else
	{
		if(CMElectionSO.daysleft > 1)
			%s = "s";
		else
			%s = "";
		announce(CMElectionSO.daysleft SPC "Day" @ %s SPC "left to vote!");
		CMElectionSO.daysleft--;
	}
}

function governmentDecision(%key, %value)
{
	for(%i = 0; %i < ClientGroup.getCount(); %i++)
	{
		%client = ClientGroup.getObject(%i);
		if(!%client.isMayor())
		{
			commandToClient(%client,'CMNotify',"Government","The Mayor changed " @ %key @ " to " @ %value, "information");
		}
	}
	CMCityData.data[%key] = %value;
}

// Game Connection

function gameConnection::isMayor(%client)
{
	return %client.bL_id == CMCityData.Data["Mayor"];
}

// ============================================================
// Section 3 - Package
// ============================================================

package CityMod_Government{
	function gameConnection::spawnPlayer(%client)
	{
		%r = parent::spawnplayer(%client);
		if(isObject(CMElectionSO) && CMElectionSO.stage == 2)
		{
			if(%client.voted || !in(CMElectionSO.voters, %client.bl_id))
				messageClient(%client,'',"\c3Make sure to vote at your local police station.");
			else
				%client.voted = false;
		}
		return %r;
	}

	function GameConnection::onDeath(%client, %sourceObject, %sourceClient, %damageType, %damageArea)
	{
		if(%client != %sourceClient && (CMPlayerData.data[%sourceClient.bl_id].data["CriminalEXP"] >= 200 && %client.isMayor()))
		{
			announce("The Mayor has been killed!");
			CMPlayerData.data[%sourceClient.bl_id].value["Demerits"] = trim(CMPlayerData.data[%sourceClient.bl_id].value["Demerits"] SPC "KillingMayor");
			CMCityData.data["Mayor"] = 888888;
			CMPlayerData.data[%sourceClient.bl_id].data["CriminalEXP"] += 100;
		}

		%r = parent::onDeath(%client, %sourceObject, %sourceClient, %damageType, %damageArea);
		return %r;
	}
};
if(isPackage(CityMod_Government))
	deactivatepackage(CityMod_Government);
activatepackage(CityMod_Government);