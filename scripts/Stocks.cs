// ============================================================
// Project				-		CityMod
// Description			-		Stock Market Code
// ============================================================
// Sections
// 	 1: Functions
// ============================================================

// ============================================================
// Section 1 - Functions
// ============================================================

function CityMod_getGroupStats(%groupID)
{
	%data = CMGroupData.data[%groupID];

	%employees = %data.value["Employed"];
	%ranks = %data.value["Ranks"];
	%employeeCount = getFieldCount(%employees);

	%salaryPayout = 0;
	%funds = %data.value["Funds"];
	%broke = %funds <= 0 ? true : false;

	for(%i = 0; %i < %employeeCount; %i++)
	{
		%employee = getField(%employees, %i);
		%rank = getWord(%employee, 1);
		%salary = getWord(getField(%ranks, %rank), 1);
		
		%salaryPayout += %salary;
	}

	%lots = CMLots.getOwnedLots("GROUP", %groupID);
	%lotCount = getWordCount(%lots);
	%lotValue = 0;

	for(%i = 0; %i < %lotCount; %i++)
		%lotValue += CMLots.getLotByID(getWord(%lots, %i)).calculateLotCost();

	%productCount = 0;
	%productValue = 0;

	%networth = %lotValue + %productValue + %funds;

	return %employeeCount SPC %salaryPayout SPC %funds SPC %broke SPC %lotCount SPC %lotValue SPC %productCount SPC %productValue SPC %networth;
}

function CMCalendarSO::onAdd(%this)
{
	%this.marketgroups = "";

	%file = new fileObject();
	%file.openForRead("config/server/CityMod/Groups/Autoload.txt");

	while(!%file.isEOF())
	{
		%id = %file.readLine();

		if(CMGroupData.data[%id].value["StockData"] $= "")
			continue;

		%this.marketgroups = ltrim(%this.marketgroups SPC %id);
		%this.dividend[%id] = 0;
		%this.lastdividend[%id] = 0;
	}
	
	%file.close();
	%file.delete();
}

function CMStockMarketSO::onYear(%this)
{
	%groups = %this.marketgroups;

	for(%i = 0; %i < getWordCount(%groups); %i++)
	{
		%group = getWord(%groups, %i);

		%dividend = %this.dividend[%group];
		%previous = %this.lastdividend[%group];
		%change = %previous - %dividend;

		%stockdata = CMGroupData.data[%group].value["StockData"];
		%stockprice = getField(%stockdata, 0);
		%stockquantity = getField(%stockdata, 1);
		%stockowners = getField(%stockdata, 2);

		%stockprice += %change / 100;

		CMGroupData.data[%group].value["StockData"] = %stockprice TAB %stockquantity TAB %stockowners;
	}
}

function CMStockMarketSO::getAvailableStocks(%this, %groupID)
{
	%stockdata = CMGroupData.data[%groupID].value["StockData"];

	if(%stockdata $= "")
		return -1;

	%amount = getField(%stockdata, 1);
	%stockowners = getField(%stockdata, 2);

	for(%i = 0; %i < getWordCount(%stockowners); %i++)
	{
		%amount -= getWord(strReplace(getWord(%stockowners, %i), ":", " "), 1);
	}

	return %amount;
}

function CMStockMarketSO::getUserStockAmount(%this, %groupID, %blid)
{
	%stockowners = %this.getStockOwners(%groupID);

	for(%i = 0; %i < getWordCount(%stockowners); %i++)
	{
		%owner = strReplace(getWord(%stockowners, %i), ":", " ");

		if(getWord(%owner, 0) $= %blid)
			return getWord(%owner, 1);
	}

	return 0;
}

function CMStockMarketSO::isGroupInStockMarket(%this, %groupID)
{
	return ((CMGroupData.data[%groupID].value["StockData"] !$= "") && in(%this.marketgroups, %groupID));
}

function CMStockMarketSO::getStockPrice(%this, %groupID)
{
	return getField(CMGroupData.data[%groupID].value["StockData"], 0);
}

function CMStockMarketSO::getStockOwners(%this, %groupID)
{
	return getField(CMGroupData.data[%groupID].value["StockData"], 2);
}

function CMStockMarketSO::groupEnter(%this, %groupID, %stockAmount)
{
	//%stockprice = getField(CityMod_getGroupStats(%groupID), 8) / %stockAmount;
	%stockprice = 5;

	if(%stockprice >= 1)
	{
		CMGroupData.data[%groupID].value["StockData"] = %stockPrice TAB %stockAmount TAB "";
		%this.marketgroups = ltrim(%this.marketgroups SPC %groupID);
	
		return true;
	}

	return false;
}

function CMStockMarketSO::groupExit(%this, %groupID)
{
	%data = CMGroupData.data[%groupID].value["StockData"];

	if(%data $= "")
		return;

	%stockprice = getField(%stockdata, 0);
	%stockquantity = getField(%stockdata, 1);
	%stockowners = getField(%stockdata, 2);

	for(%i = 0; %i < getWordCount(%stockowners); %i++)
	{
		%owner = strReplace(getWord(%stockowners, %i), ":", " ");
		%blid = getWord(%owner, 0);
		%owned = getWord(%owned, 1);
		%give = %owned * %stockprice;

		CMGroupData.data[%groupID].value["Funds"] -= %give;
		CMPlayerData.data[%blid].value["Bank"] += %give;
	}

	CMGroupData.data[%groupID].value["StockData"] = "";

	return true;
}

function CMStockMarketSO::addStock(%this, %groupID, %blid, %amount)
{
	%stockdata = CMGroupData.data[%groupID].value["StockData"];

	if(%stockdata $= "")
		return false;

	if((%this.getAvailableStocks(%groupID) - %amount) < 0)
		return false;

	%stockowners = getField(%stockdata, 2);
	%bought = false;

	for(%i = 0; %i < getWordCount(%stockowners); %i++)
	{
		%owner = strReplace(getWord(%stockowners, %i), ":", " ");
		%ownerid = getWord(%owner, 0);
		%owneramt = getWord(%owner, 1);

		if(%ownerid != %blid)
			continue;

		CMGroupData.data[%groupID].value["StockData"] = setField(CMGroupData.data[%groupID].value["StockData"], 2, setWord(%stockowners, %i, %ownerid @ ":" @ (%owneramt + %amount)));
		%bought = true;
	}

	if(!%bought)
	{
		CMGroupData.data[%groupID].value["StockData"] = setField(CMGroupData.data[%groupID].value["StockData"], 2, ltrim(%stockowners SPC (%blid @ ":" @ %amount)));
		%bought = true;
	}

	return %bought;
}

function CMStockMarketSO::removeStock(%this, %groupID, %blid, %amount)
{
	%stockdata = CMGroupData.data[%groupID].value["StockData"];

	if(%stockdata $= "")
		return false;

	%stockowners = getField(%stockdata, 2);
	%sold = false;

	for(%i = 0; %i < getWordCount(%stockowners); %i++)
	{
		%owner = strReplace(getWord(%stockowners, %i), ":", " ");
		%ownerid = getWord(%owner, 0);
		%owneramt = getWord(%owner, 1);

		if(%ownerid != %blid)
			continue;

		if((%owneramt - %amount) <= 0)
			CMGroupData.data[%groupID].value["StockData"] = setField(CMGroupData.data[%groupID].value["StockData"], 2, removeWord(%stockowners, %i));
		else
			CMGroupData.data[%groupID].value["StockData"] = setField(CMGroupData.data[%groupID].value["StockData"], 2, setWord(%stockowners, %i, %ownerid @ ":" @ (%owneramt - %amount)));

		%sold = true;
	}

	return %sold;
}