// ============================================================
// Project				-		CityMod
// Description			-		Skills Code
// ============================================================
// Sections
// 	 1: Functions
// ============================================================

// ============================================================
// Section 1 - Functions
// ============================================================

//Format: "NAME_OF_SKILL" TAB "AMT:REQ(SKILL || PVALUE)" [SPC "AMT:REQ_SKILL"] [SPC "AMT:REQ_SKILL"] TAB "AMT:REWARD_SKILL" TAB "TIME_LENGTH"
//Example: $CM::Skills["Testing", 0] = "TestSkill\t0:NOTHING\t1:TESTSKILL\t6";

//Skills:
// - Business
// - Merchant
// - Law
// - Medical
// - Government
// - Labor

function listSkillRequirements(%skill)
{
	if(%skill $= "")
		return;

	%string1 = getField(%skill, 0);

	if(strLen(getWord(getField(%skill, 1), 0)))
	{
		%Req1 = getWord(getField(%skill, 1), 0);
		%Req1 = nextToken(%Req1, "ReqAmt0", ":");
		%Req1 = nextToken(%Req1, "ReqType0", ":");
		%reqamt++;
	}

	if(strLen(getWord(getField(%skill, 1), 1)))
	{
		%Req2 = getWord(getField(%skill, 1), 1);
		%Req2 = nextToken(%Req2, "ReqAmt1", ":");
		%Req2 = nextToken(%Req2, "ReqType1", ":");
		%reqamt++;
	}

	if(strLen(getWord(getField(%skill, 1), 2)))
	{
		%Req3 = getWord(getField(%skill, 1), 2);
		%Req3 = nextToken(%Req3, "ReqAmt2", ":");
		%Req3 = nextToken(%Req3, "ReqType2", ":");
		%reqamt++;
	}

	%first = true;
	for(%i = 0; %i < %reqamt; %i++)
	{
		if(%first == true)
		{
			%string2 = "Lvl" SPC %ReqAmt[%i] SPC %ReqType[%i];
			%first = false;
		}
		else
			%string2 = %string2 @ ", Lvl" SPC %ReqAmt[%i] SPC %ReqType[%i];
	}

	%string = setField(%string, 0, %string1);
	%string = setField(%string, 1, %string2);
	return %string;
}

function loadSkillsFolder()
{
	%rootFolder = "Add-Ons/Gamemode_CityMod/CityMod/assets/skills/";

	//Business
	for(%i = findFirstFile(%rootFolder @ "Business" @ "*.txt"); %i !$= ""; %i = findNextFile(%rootFolder @ "Business" @ "*.txt"))
	{
		loadSkillFile("Business", %i);
	}

	//Government
	for(%i = findFirstFile(%rootFolder @ "Government" @ "*.txt"); %i !$= ""; %i = findNextFile(%rootFolder @ "Government" @ "*.txt"))
	{
		loadSkillFile("Government", %i);
	}

	//Labor
	for(%i = findFirstFile(%rootFolder @ "Labor" @ "*.txt"); %i !$= ""; %i = findNextFile(%rootFolder @ "Labor" @ "*.txt"))
	{
		loadSkillFile("Labor", %i);
	}

	//Law
	for(%i = findFirstFile(%rootFolder @ "Law" @ "*.txt"); %i !$= ""; %i = findNextFile(%rootFolder @ "Law" @ "*.txt"))
	{
		loadSkillFile("Law", %i);
	}

	//Medical
	for(%i = findFirstFile(%rootFolder @ "Medical" @ "*.txt"); %i !$= ""; %i = findNextFile(%rootFolder @ "Medical" @ "*.txt"))
	{
		loadSkillFile("Medical", %i);
	}

	//Merchant
	for(%i = findFirstFile(%rootFolder @ "Merchant" @ "*.txt"); %i !$= ""; %i = findNextFile(%rootFolder @ "Merchant" @ "*.txt"))
	{
		loadSkillFile("Merchant", %i);
	}
}

function loadSkillFile(%type, %file)
{
	//TestSkill\t0:NOTHING\t1:TESTSKILL\t6
	%skill = "";

	%file = new fileObject();
	%file.openForRead(%file);

	while(!%file.isEOF())
	{
		%line = strUpr(%file.readLine());

		switch$(getSubStr(%line, 0, strStr(%line, ":")))
		{
			%value = ltrim(getSubStr(%line, strStr(%line, ":") + 1, strLen(%line)));

			case "NAME":
				setField(%skill, 0, %value);
			case "LEARN_TIME":
				setField(%skill, 3, %value);
			case "SKILL_REQUIREMENTS":
				%value = strReplace(%value, "=", " ");
				%value = getWord(%value, 0) @ ":" @ getWord(%value, 1);
				setField(%skill, 1, %value);
			case "REWARD_SKILLTREE_NAME":
				setField(%skill, 2, getField(%skill, 2) @ %value);
			case "REWARD_SKILLTREE_LEVEL":
				setField(%skill, 2, %value @ ":" @ getField(%skill, 2));
		}
	}

	%file.close();
	%file.delete();

	if(getFieldCount(%skill) != 4)
		return;

	$CM::SkillCount[%type] += 1;
	$CM::Skills[%type, $CM::SkillCount[%type]] = %skill;
}

function gameConnection::isEnrolled(%client){
	return CMPlayerData.data[%client.bl_id].Value["School"] !$= "0 0 0";
}

function gameConnection::meetsSkillReqs(%client, %skill)
{
	if(%skill $= "")
		return;

	%Req1 = getWord(getField(%skill, 1), 0);
	%Req1 = nextToken(%Req1, "ReqAmt0", ":");
	%Req1 = nextToken(%Req1, "ReqType0", ":");
	%reqamt = 1;

	if(%ReqType[0] $= "NOTHING")
		return true;

	if(getWord(getField(%skill, 1), 1) !$= "")
	{
		%Req2 = getWord(getField(%skill, 1), 1);
		%Req2 = nextToken(%Req2, "ReqAmt1", ":");
		%Req2 = nextToken(%Req2, "ReqType1", ":");
		%reqamt++;
	}

	if(getWord(getField(%skill, 1), 2) !$= "")
	{
		%Req3 = getWord(getField(%skill, 1), 2);
		%Req3 = nextToken(%Req3, "ReqAmt2", ":");
		%Req3 = nextToken(%Req3, "ReqType2", ":");
		%reqamt++;
	}

	for(%i = 0; %i < %reqamt; %i++)
	{
		if(!%client.getSkill("all", %ReqType[%i]))
		{
			if(CMPlayerData.data[%client.bl_id].Value[%ReqType[%i]] $= "")
				return false;
			else if(CMPlayerData.data[%client.bl_id].Value[%ReqType[%i]] >= %ReqAmt[%i])
				continue;
		}
		else if(%client.getSkill("all", %ReqType[%i]) >= %ReqAmt[%i])
			continue;
		else
			return false;
	}

	return true;
}

function gameConnection::getForemostSkill(%client)
{
	%Skill1 = getWord(CMPlayerData.data[%client.bl_id].Value["Skill"], 0);
	%Skill1 = nextToken(%Skill1, "Skill1Type", ":");
	%Skill1 = nextToken(%Skill1, "Skill1Value", ":");

	%Skill2 = getWord(CMPlayerData.data[%client.bl_id].Value["Skill"], 1);
	%Skill2 = nextToken(%Skill2, "Skill2Type", ":");
	%Skill2 = nextToken(%skill2, "Skill2Value", ":");

	%Skill3 = getWord(CMPlayerData.data[%client.bl_id].Value["Skill"], 2);
	%Skill3 = nextToken(%Skill3, "Skill3Type", ":");
	%Skill3 = nextToken(%Skill3, "Skill3Value", ":");

	if(%Skill1Value == 0 && %Skill2Value == 0 && %Skill3Value == 0)
		return "Nothing";

	%foremost = 0;

	if(%Skill2Value > %Skill1Value)
		%foremost = 1;
	if(%Skill3Value > %Skill2Value)
		%foremost = 2;

	%ForemostSkill = getWord(CMPlayerData.data[%client.bl_id].Value["Skill"], %foremost);
	%ForemostSkill = nextToken(%ForemostSkill, "ForemostSkillType", ":");
	%ForemostSkill = nextToken(%ForemostSkill, "ForemostSkillValue", ":");

	return %ForemostSkillType;
}

//Sets a Player's Skill for a Skillset
function gameConnection::setSkill(%client, %skill, %level)
{
	if(%skill $= "")
		return;

	if(%level $= "")
		%level = "++";
	
	%Skill1 = getWord(CMPlayerData.data[%client.bl_id].Value["Skill"], 0);
	%Skill1 = nextToken(%Skill1, "Skill1Type", ":");
	%Skill1 = nextToken(%Skill1, "Skill1Value", ":");

	%Skill2 = getWord(CMPlayerData.data[%client.bl_id].Value["Skill"], 1);
	%Skill2 = nextToken(%Skill2, "Skill2Type", ":");
	%Skill2 = nextToken(%skill2, "Skill2Value", ":");

	%Skill3 = getWord(CMPlayerData.data[%client.bl_id].Value["Skill"], 2);
	%Skill3 = nextToken(%Skill3, "Skill3Type", ":");
	%Skill3 = nextToken(%Skill3, "Skill3Value", ":");

	talk(%Skill1Type);

	if(((%Skill1Type $= strUpr(%skill)) || (%Skill1Type $= "NONE")))
		%match = "skill1";
	else if(((%Skill2Type $= strUpr(%skill)) || (%Skill2Type $= "NONE")))
		%match = "skill2";
	else if(((%Skill3Type $= strUpr(%skill)) || (%Skill3Type $= "NONE")))
		%match = "skill3";
	else
		%match = "full";

	switch$(%match)
	{
		case "skill1":
			CMPlayerData.data[%client.bl_id].Value["Skill"] = setWord(CMPlayerData.data[%client.bl_id].Value["Skill"], 
			0, (strUpr(%skill) @ ":" @ (%level $= "++" ? (%Skill1Value++) : (%Skill1Value + %level))));
		case "skill2":
			CMPlayerData.data[%client.bl_id].Value["Skill"] = setWord(CMPlayerData.data[%client.bl_id].Value["Skill"],
			1, (strUpr(%skill) @ ":" @ (%level $= "++" ? (%Skill2Value++) : (%Skill2Value + %level))));
		case "skill3":
			CMPlayerData.data[%client.bl_id].Value["Skill"] = setWord(CMPlayerData.data[%client.bl_id].Value["Skill"],
			2, (strUpr(%skill) @ ":" @ (%level $= "++" ? (%Skill3Value++) : (%Skill3Value + %level))));
		case "full":
			return commandtoclient(%client, 'CM_pushNotifyBox', "Skills", "You are at the maximum number of skillsets allowed!");
	}

	commandtoclient(%client, 'CM_pushNotifyBox', "Skill Gained", "You are now Level" SPC %level SPC "in the skillset," SPC %skill);
}

//Returns the Value & Type for a Skill of a slot in a client's Skillsets
function gameConnection::getSkill(%client, %slot, %skill)
{
	if(%slot $= "")
		return;

	// Tricky tokenizing system to store
	// a type and value in the same field
	// while having multiple fields in the
	// same string. (Fields = Words)

	%Skill1 = getWord(CMPlayerData.data[%client.bl_id].Value["Skill"], 0);
	%Skill1 = nextToken(%Skill1, "Skill1Type", ":");
	%Skill1 = nextToken(%Skill1, "Skill1Value", ":");

	%Skill2 = getWord(CMPlayerData.data[%client.bl_id].Value["Skill"], 1);
	%Skill2 = nextToken(%Skill2, "Skill2Type", ":");
	%Skill2 = nextToken(%Skill2, "Skill2Value", ":");

	%Skill3 = getWord(CMPlayerData.data[%client.bl_id].Value["Skill"], 2);
	%Skill3 = nextToken(%Skill3, "Skill3Type", ":");
	%Skill3 = nextToken(%Skill3, "Skill3Value", ":");

	// If the 1st Parameter, %slot, equals
	// "all" then search through the 3 slots
	// and return the slot's skill's value 
	// that corresponds to the skill name
	// supplied in the 2nd Parameter, %skill.
	// If no slot containing the skill name
	// given is found then return 0.

	if(%slot $= "all")
	{
		if(strUpr(getField($CM::Skills[%Skill1Type, %Skill1Value], 0)) $= strUpr(%skill))
		{
			return %Skill1Value;
		}
		else if(strUpr(getField($CM::Skills[%Skill2Type, %Skill2Value], 0)) $= strUpr(%skill))
		{
			return %Skill2Value;
		}
		else if(strUpr(getField($CM::Skills[%Skill3Type, %Skill3Value], 0)) $= strUpr(%skill))
		{
			 return %Skill3Value;
		}
		else
			return 0; 
	}
	else
	{
		switch$(%slot)
		{
			// Otherwise, if %slot does not equal
			// to "all" then return the type of 
			// skill and value of that skill contained 
			// in the slot matching the variable %slot.

			case "skill1": return %Skill1Type SPC %Skill1Value;
			case "skill2": return %Skill2Type SPC %Skill2Value;
			case "skill3": return %Skill3Type SPC %Skill3Value;
		}
	}
}