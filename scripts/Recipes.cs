// ============================================================
// Project				-		CityMod
// Description			-		Recipe Code
// ============================================================
// Sections
// 	 1: Functions
// ============================================================

// ============================================================
// Section 1 - Functions
// ============================================================

function populateRecipeList()
{
	%file = new fileObject();
	%file.openForRead("Add-Ons/Gamemode_CityMod/CityMod/assets/RecipeList.txt");

	while(!%file.isEOF())
	{
		%line = strReplace(%file.readLine(), "~", "\t");
		if(getFieldCount(%line) == 5)
			$CM::Recipes[getField(%line, 0), getField(%line, 1)] = (getField(%line, 2) TAB getField(%line, 3) TAB getField(%line, 4));
	}
	
	%file.close();
	%file.delete();
}

function gameConnection::hasRecipeIngredients(%client, %recipe)
{
	if(%recipe $= "")
		return;

	for(%i = 0; %i < getWordCount(getField(%recipe, 1)); %i++)
	{
		%ingredientName = getWord(parseTokenString(getField(%recipe, 1)), 0);
		%ingredientValue = getWord(parseTokenString(getField(%recipe, 1)), 1);
		
		if(%client.getInventory("all", %ingredientName) < %ingredientValue)
			return false;
	}

	return true;
}

function gameConnection::removeRecipeIngredients(%client, %recipe)
{
	if(%recipe $= "")
		return;

	for(%i = 0; %i < getWordCount(getField(%recipe, 1)); %i++)
	{
		%ingredientName = getWord(parseTokenString(getField(%recipe, 1)), 0);
		%ingredientValue = getWord(parseTokenString(getField(%recipe, 1)), 1);

		%client.removeInventory("all", %ingredientName);
	}
}