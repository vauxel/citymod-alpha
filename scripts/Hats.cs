// ============================================================
// Project				-		CityMod
// Description			-		Hats Code
// ============================================================
// Sections
// 	 1: Functions
// ============================================================

// ============================================================
// Section 1 - Functions
// ============================================================

datablock ShapeBaseImageData(CMHat_Fedora)
{
	CMHat = true;
	CMName = "Fedora";

	shapeFile = "Add-Ons/Gamemode_CityMod/CityMod/assets/hats/fedora/fedora.dts";
	emap = true;
	mountPoint = 5;
	offset = "0 0 0";
	eyeOffset = "0 0 10";
	scale = "1 1 1";
	correctMuzzleVector = true;
	doColorShift = false;
	colorShiftColor = "1 1 1 1";

	stateName[0] = "Activate";
	stateTimeoutValue[0] = 0.1;
	stateTransitionOnTimeout[0] = "Idle";
	stateSound[0] = weaponSwitchSound;

	stateName[1] = "Idle";
	stateAllowImageChange[1] = true;
};

function addCMHat(%name)
{
	return loadHat("Add-Ons/GameMode_CityMod/CityMod/assets/hats/" @ %name @ "/" @ %name  @".hat");
}

function loadHat(%path)
{
	if(!isFile(%path))
		return 0;
	%file = new FileObject();
	%file.openForRead(%path);
	%lines = %file.readLine();
	while(!%file.isEOF())
		%lines = %lines NL %file.readLine();
	%file.close();
	%file.delete();
	%cnt = getLineCount(%lines);
	for(%i=0;%i<%cnt;%i++)
	{
		%line = getLine(%lines,%i);
		%stuff = strReplace(%line,":","\t");
		%cmd = getField(%stuff,0);
		%args = getField(%stuff,1);
		%dict[trim(%cmd)] = trim(%args);
	}
	%required = "name\tshapeFile\toffset\teyeOffset\teyeRotation\tscale";

	for(%i=0;%i<getFieldCount(%required);%i++)
	{
		if(%dict[getField(%required,%i)] $= "")
			return -1;
	}
	if(isObject("CMHat_" @ %dict["name"]))
		return -2;
	datablock ShapeBaseImageData(CMHat_tobenamed)
	{
		hatFile = %path;
		CMHat = true;
		CMName = strReplace(%dict["name"],"_"," ");

		shapeFile = %dict["shapeFile"];
		emap = true;
		mountPoint = $HeadSlot;
		offset = %dict["offset"];
		eyeOffset = %dict["eyeOffset"];
		scale = %dict["scale"];
		correctMuzzleVector = true;
		doColorShift = false;
		colorShiftColor = "1 1 1 1";

		stateName[0] = "Activate";
		stateTimeoutValue[0] = 0.1;
		stateTransitionOnTimeout[0] = "Idle";
		stateSound[0] = weaponSwitchSound;

		stateName[1] = "Idle";
		stateAllowImageChange[1] = true;
	};
	CMHat_tobenamed.setName("CMHat_" @ %dict["name"]);
	if(isObject(%hat = "CMHat_" @ %dict["name"]))
		return %hat;
	else
		return -3;
}

function Player::hat(%player,%hat,%force)
{
	%client = %player.client;
	if(!isObject(%hat))
	{
		if(!isObject(%hat = "CMHat_" @ %hat))
			return -1;
	}
	%currImg = isObject(%player.getMountedImage(2));
	if((%force && %currImg) || %currImg)
	{
		%player.unmountImage(2);
		if(!%force)
			return -1;
	}
	%player.mountImage(%hat, 2);
	commandToClient(%client, 'centerPrint', "<font:Impact:24><color:FFFFFF>You mounted the <color:C0C0C0>" @ %hat.CMName @ " <color:FFFFFF>hat!",3);
}