// ============================================================
// Project				-		CityMod
// Description			-		Groups Code
// ============================================================
// Sections
// 	 1: Functions
//	 2: Package
// ============================================================

// ============================================================
// Section 1 - Functions
// ============================================================

function resolveGroupID(%name)
{
	if(%name $= "")
		return -1;

	%name = trim(%name);

	%file = new fileObject();
	%file.openForRead("config/server/CityMod/Groups/Autoload.txt");

	while(!%file.isEOF())
	{
		%id = %file.readLine();
		if(CMGroupData.data[%id].value["Title"] $= %name)
			return %id;
	}
	
	%file.close();
	%file.delete();

	return -1;
}

function gameConnection::getGroupMembership(%client)
{
	%file = new fileObject();
	%file.openForRead("config/server/CityMod/Groups/Autoload.txt");

	while(!%file.isEOF())
	{
		%id = %file.readLine();
		if(in(CMGroupData.data[%id].value["Employed"], %client.bl_id))
			return %id;
	}
	
	%file.close();
	%file.delete();

	return -1;
}

function gameConnection::inGroup(%client, %group)
{
	if(!isInteger(%group))
		%group = resolveGroupID(%group);

	if((%group == -1) || (CMGroupData.data[%group].value["Title"] $= ""))
		return false;

	if(in(CMGroupData.data[%group].value["Employed"], %client.bl_id))
		return true;

	return false;
}

function gameConnection::getGroupRank(%client, %group)
{
	if(!%client.inGroup(%group))
		return -1;

	if(!isInteger(%group))
		%group = resolveGroupID(%group);

	for(%i = 0; %i < getFieldCount(CMGroupData.data[%group].value["Employed"]); %i++)
	{
		%found = getField(CMGroupData.data[%group].value["Employed"], %i);
		if(getWord(%found, 0) == %client.bl_id)
		{
			%rank = getWord(%found, 1);
			break;
		}
	}

	if(%rank $= "")
		return -1;

	return %rank;
}

function gameConnection::getGroupSalary(%client, %group)
{
	if(!%client.inGroup(%group))
		return -1;

	if(!isInteger(%group))
		%group = resolveGroupID(%group);

	if(%client.getGroupRank(%group) != -1)
		%rank = %client.getGroupRank(%group);
	else
		return -1;

	%salary = getWord(getField(CMGroupData.data[%group].value["Ranks"], %rank), 1);

	return %salary;
}

// ============================================================
// Section 2 - Package
// ============================================================

package CityMod_Groups
{
	function CityMod_Day()
	{
		parent::CityMod_Day();

		%file = new fileObject();
		%file.openForRead("config/server/CityMod/Groups/Autoload.txt");

		while(!%file.isEOF())
		{
			%group = %file.readLine();
			
			if(CMGroupData.data[%group].value["Title"] $= "")
				continue;

			%numgroups++;

			if(getFieldCount(CMGroupData.data[%group].value["Employed"]) == 0)
				continue;

			if(CMGroupData.data[%group].value["Funds"] <= 0)
			{
				if(isObject(%owner = findclientbyBL_ID(CMGroupData.data[%group].value["Owner"])))
					%owner.CMNotify(CMGroupData.data[%group].value["Title"], "No paychecks were paid because your Organization is bankrupt");
				continue;
			}

			%payout = 0;
			%broke = false;

			for(%i = 0; %i < getFieldCount(CMGroupData.data[%group].value["Employed"]); %i++)
			{
				%subclient = getWord(getField(CMGroupData.data[%group].value["Employed"], %i), 0);

				if(isObject(%subclient = findclientbyBL_ID(%subclient)))
				{
					%paycheck = %subclient.getGroupSalary(%group);

					if(%paycheck == 0)
						continue;

					if((CMGroupData.data[%group].value["Funds"] - %paycheck) >= 0)
					{
						%payout += %paycheck;

						CMGroupData.data[%group].value["Funds"] -= %paycheck;
						CMPlayerData.data[%subclient.bl_id].value["Bank"] += %paycheck;

						%subclient.CMNotify(CMGroupData.data[%group].value["Title"], "Paycheck:" SPC "$" @ %paycheck);
					}
					else
					{
						%broke = true;
						break;
					}
				}
			}

			if(isObject(%owner = findclientbyBL_ID(CMGroupData.data[%group].value["Owner"])))
			{
				if(%broke)
					commandtoclient(%owner, 'CM_pushNotifyBox', "Organizations", "Your group," @ CMGroupData.data[%group].value["Title"] @ ", is now Broke!");
				else
					%owner.CMNotify(CMGroupData.data[%group].value["Title"], "Payroll Outcome:" SPC "$" @ %payout);
			}
		}
		
		%file.close();
		%file.delete();
	}
};
if(isPackage(CityMod_Groups))
	deactivatepackage(CityMod_Groups);
activatepackage(CityMod_Groups);