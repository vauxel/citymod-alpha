// ============================================================
// Project				-		CityMod
// Description			-		Lots Code
// ============================================================
// Sections
// 	 1: Functions
//   2: Trigger Package
// ============================================================

// ============================================================
// Section 1 - Functions
// ============================================================

function CMLotSO::onAdd(%this)
{
	%this.lotList = "";

	//%this.schedule(30000, "gatherAllLots");
}

function CMLotSO::addLot(%this, %lot)
{
	if(in(%this.lotList, %lot.getID()))
		return;

	%this.lotList = setWord(%this.lotList, getWordCount(%this.lotList), %lot.getID());
}

function CMLotSO::removeLot(%this, %lot)
{
	if(!in(%this.lotList, %lot.getID()))
		return;

	%this.lotList = removeWord(%this.lotList, getIndex(%this.lotList, %lot.getID()));
}

function CMLotSO::gatherAllLots(%this)
{
	for(%a = 0; %a < mainBrickGroup.getCount(); %a++)
	{
		%group = mainBrickGroup.getObject(%a);
		
		for(%b = 0; %b < %group.getCount(); %b++)
		{
			%brick = %group.getObject(%b);
			
			if(%brick.getDatablock().CMBrick && (%brick.getDatablock().CMBrickType $= "LOT"))
				%this.addLot(%brick);
		}
	}
}

function CMLotSO::getLotByID(%this, %id)
{
	if(%id $= "")
		return -1;

	for(%i = 0; %i < getWordCount(%this.lotList); %i++)
	{
		%lot = getWord(%this.lotList, %i);

		if(%lot.lotID == %id)
			return %lot;
	}

	return -1;
}

function CMLotSO::getOwnedLots(%this, %type, %id)
{
	if((%type $= "") || (%id $= ""))
		return;

	%ownedlist = "";

	for(%i = 0; %i < getWordCount(%this.lotList); %i++)
	{
		%lot = getWord(%this.lotList, %i);

		if((strUpr(%type) $= "PLAYER") && (%lot.getGroup().bl_id == %id))
			%ownedlist = ltrim(%ownedlist SPC %lot.lotID);
		else if((strUpr(%type) $= "GROUP") && (%lot.groupOwnership == %id))
			%ownedlist = ltrim(%ownedlist SPC %lot.lotID);
	}

	return trim(%ownedlist);
}

function CMLotSO::getNextID(%this)
{
	%highestid = 0;

	for(%i = 0; %i < getWordCount(%this.lotList); %i++)
	{
		%lotid = getWord(%this.lotList, %i).lotID;

		if(%lotid > %highestid)
			%highestid = %lotid;
	}

	return %highestid++;
}

function CMLotSO::isGroupOwnedLot(%this, %lotid)
{
	if(%lotid $= "")
		return -1;

	%file = new fileObject();
	%file.openForRead("config/server/CityMod/Groups/Autoload.txt");

	while(!%file.isEOF())
	{
		%id = %file.readLine();
		if(in(CMGroupData.data[%id].value["Lots"], %lotid))
			return %id;
	}
	
	%file.close();
	%file.delete();

	return false;
}

function SimGroup::getRandomSpawn(%this)
{
	return %this.spawn[getRandom(0,%this.spawnCount-1)];
}

//Called when a player enters a LotTrigger
function GameConnection::onEnterLot(%client, %lot)
{
	%client.player.inLot = %lot;
	commandtoclient(%client, 'CM_enterLot', %lot.getGroup().client.name @ "'s Lot", "Lot #" @ %lot.lotID);
}

//Called when a player leaves a LotTrigger
function GameConnection::onLeaveLot(%client, %lot)
{
	%client.player.inLot = false;
	commandtoclient(%client, 'CM_leaveLot');

	if(%client.player.foodCart > 0 || %client.player.gunCart > 0)
	{
		talk(%client.name SPC "shoplifted!");
		InitContainerRadiusSearch(%client.player.getPosition(), 50, $TypeMasks::PlayerObjectType);
		while(%obj = containerSearchNext())
		{
			if(isObject(%obj.client) && %obj.objectInView(%client.player))
			{
				talk(%client.name SPC "was seen by" SPC %obj.client.name);
				CMPlayerData.data[%found].value["Charges"] = trim(CMPlayerData.data[%found].value["Charges"] SPC "ROBBERY");
				commandToClient(%client, 'centerPrint', "<font:Impact:24><color:FF0000>You were caught shoplifting!", 3);
				break;
			}
		}
	}
}

//Called when a player enters a SchoolTrigger
function GameConnection::onEnterSchool(%client, %lot)
{
	%client.player.inSchoolLot = true;
}

//Called when a player leaves a SchoolTrigger
function GameConnection::onLeaveSchool(%client, %lot)
{
	%client.player.inSchoolLot = false;
	if(%client.isEnrolled())
	{
		commandtoclient(%client, 'CM_pushNotifyBox', "School", "You left the school campus. Your enrollment in any classes has ceased.");
		CMPlayerData.data[%client.bl_id].value["School"] = CMPlayerData.FieldDefault["School"];
	}
}

//Called when a player enters a JailTrigger
function GameConnection::onEnterJail(%client, %lot)
{
	if((%client.player.getMountedImage(0) == CMCuffsImage.getID()) && strLen(CMPlayerData.data[%client.bl_id].value["Charges"]) && isObject(%client.player.cufferPlayer))
	{
		%client.player.unHandCuff();
		%client.player.cufferPlayer.arrestPlayer(%client);
		return;
	}

	if(!%client.inJail())
		%client.player.visitingJail = true;
}

//Called when a player leaves a JailTrigger
function GameConnection::onLeaveJail(%client, %lot)
{
	if(%client.inJail())
	{
		addCriminalCharge(%client.bl_id, "JailEscape");
		announce(%client.name SPC "\c6has escaped jail!");
		CMPlayerData.data[%client.bl_id].value["JailTime"] = CMPlayerData.FieldDefault["JailTime"];
	}
	else
		%client.player.visitingJail = false;
}

function isValidtoPlant(%brick)
{
	if(!isObject(%brick))
		return "NO_LOT";

	if(!isObject(%brick.getDownBrick(0)))
		return "NO_LOT";

	%lot = %brick;
	while(isObject(%lot) && (%lot.getDataBlock().CMBrickType !$= "LOT"))
	{
		%lot = %lot.getDownBrick(0);
	}

	if(!isObject(%lot))
		return "NO_LOT";
	
	// ======================================================================= \\

	if(mFloor(getWord(%brick.rotation, 3)) == 90)
		%boxSize = (%brick.getDataBlock().bricksizeY / 2) SPC (%brick.getDataBlock().bricksizeX / 2) SPC ($CM::Prefs::Building::MaxLotHeight);
	else
		%boxSize = (%brick.getDataBlock().bricksizeX / 2) SPC (%brick.getDataBlock().bricksizeY / 2) SPC ($CM::Prefs::Building::MaxLotHeight);
				
	initContainerBoxSearch(%brick.getWorldBoxCenter(), %boxSize, $TypeMasks::TriggerObjectType);
				
	while(isObject(%trigger = containerSearchNext()))
	{
		if(%trigger.getDatablock() == CMLotTrigger.getID())
		{
			%lotTrigger = %trigger;
			break;
		}
	}

	%withinBounds = false;
	
	if(isObject(%lotTrigger))
	{
		%lotTriggerMinX = getWord(%lotTrigger.getWorldBox(), 0);
		%lotTriggerMinY = getWord(%lotTrigger.getWorldBox(), 1);
		%lotTriggerMinZ = getWord(%lotTrigger.getWorldBox(), 2);
						
		%lotTriggerMaxX = getWord(%lotTrigger.getWorldBox(), 3);
		%lotTriggerMaxY = getWord(%lotTrigger.getWorldBox(), 4);
		%lotTriggerMaxZ = getWord(%lotTrigger.getWorldBox(), 5);
						
		%brickMinX = getWord(%brick.getWorldBox(), 0) + 0.0016;
		%brickMinY = getWord(%brick.getWorldBox(), 1) + 0.0013;
		%brickMinZ = getWord(%brick.getWorldBox(), 2) + 0.00126;
						
		%brickMaxX = getWord(%brick.getWorldBox(), 3) - 0.0016;
		%brickMaxY = getWord(%brick.getWorldBox(), 4) - 0.0013;
		%brickMaxZ = getWord(%brick.getWorldBox(), 5) - 0.00126;
						
						
		if(%brickMinX >= %lotTriggerMinX && %brickMinY >= %lotTriggerMinY && %brickMinZ >= %lotTriggerMinZ)
		{
			if(%brickMaxX <= %lotTriggerMaxX && %brickMaxY <= %lotTriggerMaxY && %brickMaxZ <= %lotTriggerMaxZ)
			{
				%withinBounds = true;
			}
			else
			{
				return "OUT_OF_LOT";
			}
		}
		else
		{
			return "OUT_OF_LOT";
		}
	}

	// ======================================================================= \\

	if(!%lotTrigger && isObject(%lot) && (%lot.getDataBlock().CMBrickType $= "LOT"))
	{
		return "OUT_OF_LOT";
	}
	else if(!%lotTrigger && !isObject(%lot) && (%lot.getDataBlock().CMBrickType !$= "LOT"))
	{
		return "NO_LOT";
	}
	else if(%lotTrigger && isObject(%lot) && %withinBounds)
	{
		return %lotTrigger;
	}
	else
	{
		return "NO_LOT";
	}
}

function fxDTSBrick::calculateLotCost(%brick)
{
	if(%brick.getDataBlock().CMBrickType !$= "LOT")
		return echo("[CityMod] calculateLotCost() - Brick is not a Lot");

	%lotcost = $CM::Prefs::Building::DefaultLotPrice;

	if(mFloor(getWord(%brick.rotation, 3)) == 90)
		%boxSize = (%brick.getDataBlock().bricksizeY / 2) SPC (%brick.getDataBlock().bricksizeX / 2) SPC ($CM::Prefs::Building::MaxLotHeight);
	else
		%boxSize = (%brick.getDataBlock().bricksizeX / 2) SPC (%brick.getDataBlock().bricksizeY / 2) SPC ($CM::Prefs::Building::MaxLotHeight);
				
	initContainerBoxSearch(%brick.getWorldBoxCenter(), %boxSize, $TypeMasks::FxBrickObjectType);
				
	while(isObject(%found = containerSearchNext()))
	{
		if((%found.getDatablock() == CMLotTrigger.getID()) || (%found.getDatablock().CMBrick && strLen(%found.getDatablock().lotType)))
			continue;

		if(%found.getDatablock().CMBrick)
			%lotcost += 10;
		else
			%lotcost += mCeil((%brick.getDataBlock().bricksizeY + %brick.getDataBlock().bricksizeX + %brick.getDataBlock().bricksizeZ) / 16);
	}

	%brick.lotCost = %lotcost;
	return %lotcost;
}

// ============================================================
// Section 2 - Trigger Package
// ============================================================

package CMP_Triggers
{
	//Trigger creation function - [Thanks Ipqaurx]
	function fxDTSBrick::createTrigger(%this, %triggerDataBlock)
	{
		%t = new Trigger()
		{
			datablock = %triggerDataBlock;
			polyhedron = "0 0 0 1 0 0 0 -1 0 0 0 1";
		};
		
		missionCleanup.add(%t);
		
		%trigX = %this.getDatablock().brickSizeX;
		%trigY = %this.getDatablock().brickSizeY;
		%trigZ = $CM::Prefs::Building::MaxLotHeight * 2;
		if(mFloor(getWord(%this.rotation, 3)) == 90)
			%boxDiff = (%trigY / 2) SPC (%trigX / 2) SPC (%trigZ / 2);
		else
			%boxDiff = (%trigX / 2) SPC (%trigY / 2) SPC (%trigZ / 2);
		%t.setScale(%boxDiff);
		%posA = %this.getWorldBoxCenter();
		%posB = %t.getWorldBoxCenter();
		%posDiff = vectorSub(%posA, %posB);
		%posDiff = vectorAdd(%posDiff, "0 0 0.1");
		%t.setTransform(%posDiff);

		%this.trigger = %t;
		%t.brick = %this;

		return %t;
	}
	
	//Remove the trigger when its brick gets deleted
	function fxDTSBrick::onRemove(%this)
	{
		if(isObject(%this.trigger))
			%this.trigger.delete();

		parent::onRemove(%this);
	}
	
	//Called when a player enters the LotTrigger
	function CMLotTrigger::onEnterTrigger(%this, %trigger, %obj)
	{
		if(%obj.getClassName() $= "Player")
			%obj.client.schedule(750, "onEnterLot", %trigger.brick);
	}
	
	//Called when a player leaves the LotTrigger
	function CMLotTrigger::onLeaveTrigger(%this, %trigger, %obj)
	{
		if(%obj.getClassName() $= "Player")
			%obj.client.onLeaveLot(%trigger.brick);
	}
	
	//Called when a player enters the SchoolTrigger
	function CMSchoolTrigger::onEnterTrigger(%this, %trigger, %obj)
	{
		if(%obj.getClassName() $= "Player")
			%obj.client.onEnterSchool(%trigger.brick);
	}
	
	//Called when a player leaves the SchoolTrigger
	function CMSchoolTrigger::onLeaveTrigger(%this, %trigger, %obj)
	{
		if(%obj.getClassName() $= "Player")
			%obj.client.onLeaveSchool(%trigger.brick);
	}

	//Called when a player enters the JailTrigger
	function CMJailTrigger::onEnterTrigger(%this, %trigger, %obj)
	{
		if(%obj.getClassName() $= "Player")
			%obj.client.onEnterJail(%trigger.brick);
	}
	
	//Called when a player leaves the JailTrigger
	function CMJailTrigger::onLeaveTrigger(%this, %trigger, %obj)
	{
		if(%obj.getClassName() $= "Player")
			%obj.client.onLeaveJail(%trigger.brick);
	}
};

if(isPackage(CMP_Triggers))
	deactivatePackage(CMP_Triggers);
activatePackage(CMP_Triggers);