// ============================================================
// Project				-		CityMod
// Description			-		Calendar Code
// ============================================================
// Sections
//   1: Variables
// 	 2: Functions
// ============================================================

// ============================================================
// Section 1 - Variables
// ============================================================
//$CM::CalendarEvents["Date", "January 2"] = "testFunction";
$CM::CalendarEvents["Date", "January 1"] = "stockMarket_tick";
//$CM::CalendarEvents["Time", "00:30"] = "testFunction2";

// ============================================================
// Section 2 - Functions
// ============================================================

function stockMarket_tick()
{
	CMStockMarketData.onYear();

	%file = new fileObject();
	%file.openForRead("config/server/CityMod/Groups/Autoload.txt");

	while(!%file.isEOF())
	{
		%group = %file.readLine();
			
		if(CMGroupData.data[%group].value["Title"] $= "")
			continue;

		%dividend = CMGroupData.data[%group].value["Dividend"];
		CMGroupData.data[%group].value["Dividend"] = 0;
		CMGroupData.data[%group].value["LastDividend"] = %dividend;
	}	
	%file.close();
	%file.delete();
}

function testFunction()
{
	warn("THIS IS A TEST!!!!!");
}

function testFunction2()
{
	warn("THIS IS A SECOND TEST!!!!!");
}

function CMCalendarSO::onAdd(%this)
{
	// Month Names
	%this.monthName[0] = "January";
	%this.monthName[1] = "February";
	%this.monthName[2] = "March";
	%this.monthName[3] = "April";
	%this.monthName[4] = "May";
	%this.monthName[5] = "June";
	%this.monthName[6] = "July";
	%this.monthName[7] = "August";
	%this.monthName[8] = "September";
	%this.monthName[9] = "October";
	%this.monthName[10] = "November";
	%this.monthName[11] = "December";
	
	// Month Days
	%this.monthDays[0] = 31;
	%this.monthDays[1] = 28;
	%this.monthDays[2] = 31;
	%this.monthDays[3] = 30;
	%this.monthDays[4] = 31;
	%this.monthDays[5] = 30;
	%this.monthDays[6] = 31;
	%this.monthDays[7] = 31;
	%this.monthDays[8] = 30;
	%this.monthDays[9] = 31;
	%this.monthDays[10] = 30;
	%this.monthDays[11] = 31;

	%this.minute = 0;
	%this.hour = 0;
	%this.day = 0;
	%this.month = 0;
	%this.year = 2000;

	%this.passTime();
	%this.syncDayCycle();
}

function CMCalendarSO::passTime(%this)
{
	if(%this.minute >= 59)
	{
		%this.minute = 0;
		%this.hour++;

		CityMod_Hour();
	}
	else
	{
		%this.minute++;
	}
	
	if(%this.hour > 23)
	{
		%this.hour = 0;
		%this.day++;

		CityMod_Day();

		if($CM::CalendarEvents["Date", %this.getFormattedDate()] !$= "")
			call($CM::CalendarEvents["Date", %this.getFormattedDate()]);
	}

	if((%this.day + 1) > %this.monthDays[%this.month])
	{
		%this.day = 0;
		%this.month++;
	}

	if(%this.month > 11)
	{
		%this.month = 0;
		%this.year++;
	}

	if($CM::CalendarEvents["Time", %this.getFormattedTime(true)] !$= "")
		call($CM::CalendarEvents["Time", %this.getFormattedTime(true)]);

	%this.syncDayCycle();

	echo(CMCalendar.getFormattedDate() SPC CMCalendar.getFormattedTime() SPC pad(%this.hour) @ ":" @ pad(%this.minute));
}

function CMCalendarSO::syncDayCycle(%this)
{
	if(!isObject(Sun))
		return;

	Sun.elevation = (%this.getTimeInSeconds() / 432) - 10;
	Sun.sendUpdate();
}

function CMCalendarSO::getTimeInSeconds(%this)
{
	%seconds = 0;
	%seconds += (%this.hour * 60) * 60;
	%seconds += %this.minute * 60;

	return %seconds;
}

function CMCalendarSO::getFormattedTime(%this, %twentyfourhour)
{
	if(%twentyfourhour $= "")
		%twentyfourhour = false;

	%minute = pad(%this.minute);

	if(%twentyfourhour == false)
	{
		if(%this.hour >= 13)
		{
			return pad(%this.hour - 12) @ ":" @ %minute SPC "PM";
		}
		else
		{
			if(%this.hour == 12)
				return pad(%this.hour) @ ":" @ %minute SPC "PM";

			if(%this.hour == 0)
				return "12:" @ %minute SPC "AM";

			return pad(%this.hour) @ ":" @ %minute SPC "AM";
		}
	}
	else
		return pad(%this.hour) @ ":" @ pad(%this.minute);
}

function CMCalendarSO::getFormattedDate(%this)
{
	return %this.monthName[%this.month] SPC (%this.day + 1);
}