function gameConnection::isBlack(%client,%range)
{
	if(%range $= "")
		%range = 10;

	%face = getWords(%client.headColor,0,2);
	%left = getWords(%client.lHandColor,0,2);
	%right = getWords(%client.rHandColor,0,2);

	%red = mFloor(((getWord(%face,0) + getWord(%left,0) + getWord(%right,0)) / 3) + 0.5);
	%blue = mFloor(((getWord(%face,1) + getWord(%left,1) + getWord(%right,1)) / 3) + 0.5);
	%green = mFloor(((getWord(%face,2) + getWord(%left,2) + getWord(%right,2)) / 3) + 0.5);

	%vect = %red SPC %blue SPC %green;

	%avg = "127 63 0";

	return vectorDist(%avg,%vect) < %range;
}