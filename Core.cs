// ============================================================
// Project				-		CityMod
// Description			-		Handles all the essential Functions
// ============================================================
// Sections
// 	 1: CityMod Essentials
// 	 2: Client Functions
//	 3: General Functions
// ============================================================

// ============================================================
// Section 1 - CityMod Essentials
// ============================================================
// Usage CityMod_FunctionName(%parameter1, %parameter2);

//The function called for the game to start up
function CityMod_Initiate()
{
	if($CM::HasStarted)
		return;
	
	//Loading Variables for Players
	warn("[CityMod] :: Starting Up CityMod RP");
	echo("===================================");
	warn("[CityMod] :: Loading CityMod Player Data");
		
	new scriptObject(CMPlayerData)
	{
		class = "CityModPDB";
		AutomaticLoading = 1;
		filePath = "config/server/CityMod/Players/";
	};
		
	CMPlayerData.addField("Name", "Blockhead");
	CMPlayerData.addField("IP", "NONE");
	
	CMPlayerData.addField("Money", mFloor($CM::Prefs::Player::StartingCash / 2));
	CMPlayerData.addField("Bank", mFloor($CM::Prefs::Player::StartingCash / 2));
	CMPlayerData.addField("Debt", 100);
	CMPlayerData.addField("Taxes", 0);

	CMPlayerData.addField("Charges", "");
	CMPlayerData.addField("JailTime", "");

	CMPlayerData.addField("Reputation", 0);
	CMPlayerData.addField("Skill", "NONE:0 NONE:0 NONE:0"); //(Skill:Level)
	CMPlayerData.addField("Hunger", 100);
	CMPlayerData.addField("Inventory", "NONE:0 NONE:0 NONE:0 NONE:0 NONE:0 NONE:0");
	
	CMPlayerData.addField("School", "0 0 0");
	
	//Loading Variables for Groups
	warn("[CityMod] :: Loading CityMod Group Data");
	
	new scriptObject(CMGroupData)
	{
		class = "CityModGDB";
		AutomaticLoading = 1;
		filePath = "config/server/CityMod/Groups/";
	};
	
	CMGroupData.addField("Title", "Default");
	CMGroupData.addField("Owner", 999999);
	CMGroupData.addField("Funds", 1000);
	CMGroupData.addField("Employed", ""); //BL_ID //Rank
	CMGroupData.addField("Ranks", "DEFAULT 0"); //Name //Salary

	CMGroupData.addField("StockData", "");
	CMGroupData.addField("Lots", ""); //Lots owned by group

	//Creating Default Groups
	if(CMGroupData.data[1].value["Title"] $= "")
	{
		CMGroupData.addData(1);
		CMGroupData.data[%group].value["Title"] = "Police Department";
		CMGroupData.data[%group].value["Owner"] = "888888";
		CMGroupData.data[%group].value["Funds"] = mFloor($CM::Prefs::Core::CityStarterFunds / 2);
		CMGroupData.data[%group].value["Ranks"] = setField(CMGroupData.data[%group].value["Ranks"], 0, %starterrank_name SPC %starterrank_salary);
	}
	
	//Loading Variables for the City
	warn("[CityMod] :: Loading CityMod City Data");
	
	new scriptObject(CMCityData) { class = "CityModCDB"; };
	CMCityData.loadData();

	warn("[CityMod] :: Creating Script Objects");

	//Creating Spawn Collection
	new simGroup(CMCitySpawns) { spawnCount = 0; };
	new simGroup(CMJailSpawns) { spawnCount = 0; };
	
	//Creating ScriptObjects
	new scriptObject(CMCalendar) { class = "CMCalendarSO"; };
	new scriptObject(CMLots) { class = "CMLotSO"; };
	new scriptObject(CMStockMarket) { class = "CMStockMarketSO"; };

	//Creating the Minigame
	warn("[CityMod] :: Creating Gamemode");

	new ScriptObject(CityModMini) 
	{
		class = miniGameSO;

		brickDamage = 1;
		brickRespawnTime = 60000;
		colorIdx = -1;
			
		enableBuilding = 1;
		enablePainting = 1;
		enableWand = 1;
		fallingDamage = 1;
		inviteOnly = 0;
			
		points_plantBrick = 0;
		points_breakBrick = 0;
		points_die = 0;
		points_killPlayer = 0;
		points_killSelf = 0;
		
		playerDatablock = playerCM;
		respawnTime = 1000;
		selfDamage = 1;
			
		playersUseOwnBricks = 0;
		useAllPlayersBricks = 1;
		useSpawnBricks = 0;
		VehicleDamage = 1;
		vehicleRespawnTime = 10000;
		weaponDamage = 1;
		
		numMembers = 1;

		vehicleRunOverDamage = 1;
	};
		
	//Banning Events
	warn("[CityMod] :: Removing Abusive Events");

	UnRegisterOutputEvent("GameConnection", "incScore");
	UnRegisterOutputEvent("Minigame", "Reset");
	UnRegisterOutputEvent("Minigame", "RespawnAll");
	
	//Running additional start-up functions
	populateRecipeList();

	//Done
	warn("[CityMod] :: CityMod has been successfully started");
	$CM::HasStarted = 1;
	
	//Starting up the first Tick
	CityMod_Tick();
}

//The custom tick function for CityMod
function CityMod_Tick()
{
	cancel($CM::Tick);
	
	warn("[CityMod] :: Tick Initiated [" @ ClientGroup.getCount() SPC "Clients active]");
	for(%a = 0; %a < ClientGroup.getCount(); %a++){ %sub = ClientGroup.getObject(%a); %sub.tickStuff(); }

	CMCalendar.passTime();
	$CM::Tick = schedule(($CM::Prefs::Core::TickSpeed * 1000), 0, CityMod_Tick);
}

//Called each in-game hour
function CityMod_Hour()
{
	CityMod_SaveData(false);
}

//Called each in-game day
function CityMod_Day()
{
	if(isObject(CMElectionSO))
		nextElectionStage();
}

//Quick way to group together all the save functions and call them at once
function CityMod_SaveData(%broadcast)
{
	if(%broadcast)
		CityMod_NotifyAll("Data Saved", "Your data has been saved!", "add");
	
	CMCityData.saveData();
	CMGroupData.saveAll();
	
	for(%a = 0; %a < ClientGroup.getCount(); %a++)
	{
		%client = ClientGroup.getObject(%a);
		
		CMPlayerData.saveData(%client.bl_id);
		%client.setScore(CMPlayerData.data[%client.bl_id].Value["Reputation"]);
		%client.UpdateHUD();
		
		warn("[CityMod] :: Data for ID" SPC %client.bl_id SPC "has been saved");
	}
}

//Function to send a notification to every player on the server
function CityMod_NotifyAll(%header, %body, %icon)
{
	for(%a = 0; %a < ClientGroup.getCount(); %a++)
	{
		%client = ClientGroup.getObject(%a);
		commandtoclient(%client, 'CM_Notify', %header, %body, %icon);
	}
}

// ============================================================
// Section 2 - Client Functions
// ============================================================
// Usage: %client.FunctionName(%parameter1, %parameter2);

function gameConnection::getIncome(%client)
{
	%income = 0;

	if(%client.getGroupMembership() !$= "-1")
		%income += %client.getGroupSalary(%client.getGroupMembership());

	return %income;
}

function gameConnection::tickStuff(%client)
{
	//School
	if(%client.isEnrolled())
	{
		if(getWord(CMPlayerData.data[%client.bl_id].Value["School"], 2) > 0)
		{
			CMPlayerData.data[%client.bl_id].Value["School"] = setWord(CMPlayerData.data[%client.bl_id].Value["School"], 2, getWord(CMPlayerData.data[%client.bl_id].Value["School"], 2) - 1);
			%client.CMNotify("School", "You have" SPC getWord(CMPlayerData.data[%client.bl_id].Value["School"], 2) SPC "Days left in your class");
		}
	}

	//Jail
	if(%client.inJail())
	{
		if(CMPlayerData.data[%client.bl_id].Value["JailTime"] > 0)
			CMPlayerData.data[%client.bl_id].Value["JailTime"]--;
		else
			%client.unJail();
	}
}

function gameConnection::CMNotify(%client, %title, %text, %icon){
	commandtoclient(%client, 'CM_Notify', %title, %text, %icon);
}

//Sends a command to the client to update the client-sided CityMod HUD
function gameConnection::UpdateHUD(%client)
{
	%lastname = getSubStr(getWord(CMPlayerData.data[%client.bl_id].Value["Name"], 1), 0, 1);
	%fullname = getWord(CMPlayerData.data[%client.bl_id].Value["Name"], 0) SPC %lastname @ "."; //15 Chars is the max until the HUD starts overlapping

	%skills = getSubStr(firstWord(%client.getSkill("skill1")), 0, 1) SPC getSubStr(firstWord(%client.getSkill("skill2")), 0, 1) SPC getSubStr(firstWord(%client.getSkill("skill3")), 0, 1);
	
	if(isObject(%client.player))
		%health = mFloor(%client.player.dataBlock.maxDamage - %client.player.getDamageLevel());
	else
		%health = 0;
		
	if(%health <= 10)
		%health = "CRITICAL";
		
	%client.setScore(CMPlayerData.data[%client.bl_id].Value["Reputation"]);
		
	commandtoclient(%client, 'CM_updateHUD', 
	CMPlayerData.data[%client.bl_id].Value["Money"],
	CMPlayerData.data[%client.bl_id].Value["Reputation"],
	getWordCount(CMPlayerData.data[%client.bl_id].Value["Charges"]),
	CMPlayerData.data[%client.bl_id].Value["Taxes"],
	%client.getIncome(),
	%fullname,
	%skills,
	%health,
	CMPlayerData.data[%client.bl_id].Value["Hunger"]);
}

//Creates a Slow-Mo loop for the player
function GameConnection::slowMo(%client)
{
	for(%i = 0; %i <= 4; %i++)
	{
		%delay = %i * 250;
		%timescale = %delay * 0.001;
			
		if(%i == 0)
		{
			%delay = 16;
			%timescale = 0.2;
		}

		schedule(%delay, 0, commandToClient, %client, 'TimeScale', %timescale);
	}
}

// ============================================================
// Section 3 - Brick Functions
// ============================================================
// Usage: %brick.FunctionName(%parameter1, %parameter2);

function fxDTSBrick::isInPerimeter(%this, %brick)
{
	if(!isObject(%brick))
		return error("fxDTSBrick::getNextCrateType - Invalid Brick given!");

	if(mFloor(getWord(%this.rotation, 3)) == 90)
		%boxSize = (%this.getDataBlock().bricksizeY / 2) SPC (%this.getDataBlock().bricksizeX / 2) SPC (128);
	else
		%boxSize = (%this.getDataBlock().bricksizeX / 2) SPC (%this.getDataBlock().bricksizeY / 2) SPC (128);
				
	initContainerBoxSearch(%this.getWorldBoxCenter(), %boxSize, $TypeMasks::FxBrickObjectType);
				
	while(isObject(%found = containerSearchNext()))
	{
		if(%found.getID() == %brick.getID())
			return true;
	}

	return false;
}

function fxDTSBrick::getInsideBricks(%this)
{
	if(mFloor(getWord(%this.rotation, 3)) == 90)
		%boxSize = (%this.getDataBlock().bricksizeY / 2) SPC (%this.getDataBlock().bricksizeX / 2) SPC (%this.getDataBlock().bricksizeZ / 2);
	else
		%boxSize = (%this.getDataBlock().bricksizeX / 2) SPC (%this.getDataBlock().bricksizeY / 2) SPC (%this.getDataBlock().bricksizeZ / 2);
				
	initContainerBoxSearch(%this.getWorldBoxCenter(), %boxSize, $TypeMasks::FxBrickObjectType);
				
	while(isObject(%found = containerSearchNext()))
	{
		if(%found.getID() == %this.getID())
			continue;

		if(vectorDist(%this.getPosition(), %found.getPosition()) > 1)
			continue;

		%list = %list SPC %found;
	}

	return ltrim(%list);
}

// ============================================================
// Section 4 - General Functions
// ============================================================
// Usage: FunctionName(%parameter1, %parameter2);

function hasDataFile(%id)
{
	if(%id $= "" || !isInteger(%id))
		return false;

	return isFile("config/server/CityMod/Players/" @ %id @ ".dat");
}

//Black-Box function to get a random name from the LastNames text file
function getLastName()
{
	%file = new fileObject();
	%file.openForRead("Add-Ons/Gamemode_CityMod/CityMod/assets/LastNames.txt");
	
	%linenum = -1;
	%list = "NAMES LIST";
	
	while(!%file.isEOF())
	{
		%linenum++;
		%list[%linenum] = getWord(%file.readLine(),0);
	}
	
	%file.close();
	%file.delete();

	return %list[getRandom(0,%linenum)];
}

function resolvePlayerID(%name)
{
	if(%name $= "")
		return -1;

	%file = new fileObject();
	%file.openForRead("config/server/CityMod/Players/Autoload.txt");

	while(!%file.isEOF())
	{
		%bl_id = %file.readLine();
		if(strLwr(getWord(CMPlayerData.data[%bl_id].value["Name"], 0)) $= strLwr(%name))
			return %bl_id;
	}
	
	%file.close();
	%file.delete();

	return -1;
}

function parseTokenString(%string, %token)
{
	if(%token $= "")
		%token = ":";

	%tokenizedstring = %string;

	for(%i = 0; %i <= getInstances(%string, ":"); %i++)
	{
		%tokenizedstring = nextToken(%tokenizedstring, "tokenPart" @ %i, %token);
		%tokenParsed = %tokenParsed SPC %tokenPart[%i];
	}

	return ltrim(%tokenParsed);
}

function containsBlacklistedWords(%string)
{
	%file = new fileObject();
	%file.openForRead("Add-Ons/Gamemode_CityMod/CityMod/assets/BannedWords.txt");
	
	while(!%file.isEOF())
	{
		%blacklist = ltrim(%blacklist SPC %file.readLine());
	}
	
	%file.close();
	%file.delete();

	for(%a = 0; %a < getWordCount(%string); %a++)
	{
		%found = getWord(%string, %a);
		if(in(%blacklist, strLwr(%found)))
			return true;
	}

	return false;
}

function isInteger(%string)
{
	return %string == mFloor(%string + 0.5);
}

function properText(%text)
{
	if(strLen(%text) == 1)
		return %text;

	return strUpr(getSubStr(%text, 0, 1)) @ strLwr(getSubStr(%text, 1, strLen(%text)));
}

function vowelCheck(%string)
{
	%char = strLwr(getSubStr(%string, 0, 1));

	if((%char $= "a") || (%char $= "e") || (%char $= "i") || (%char $= "o") || (%char $= "u"))
		return "an";
	else
		return "a";
}

function createDroppable(%type, %pos, %value)
{
	if(%pos $= "")
		return echo("[CityMod] createDroppable() - Position not specified (Can be Player or Transform)");
	if(%type $= "")
		return echo("[CityMod] createDroppable() - Type not specified");
	if(%value $= "")
		return echo("[CityMod] createDroppable() - Value not specified");
	
	switch$(strLwr(%type))
	{
		case "wood": %droppable = new Item("Droppable"){ datablock = woodItem; canPickup = true; value = %value; position = (isObject(%pos) ? %pos.getPosition() : %pos); };
		case "metal": %droppable = new Item("Droppable"){ datablock = metalItem; canPickup = true; value = %value; position = (isObject(%pos) ? %pos.getPosition() : %pos); };
		case "coal": %droppable = new Item("Droppable"){ datablock = coalItem; canPickup = true; value = %value; position = (isObject(%pos) ? %pos.getPosition() : %pos); };
		case "plastic": %droppable = new Item("Droppable"){ datablock = plasticItem; canPickup = true; value = %value; position = (isObject(%pos) ? %pos.getPosition() : %pos); };
		case "money": %droppable = new Item("Droppable"){ datablock = moneyItem; canPickup = true; value = %value; position = (isObject(%pos) ? %pos.getPosition() : %pos); };
	}

	if(isObject(%droppable) && isObject(%pos))
	{
		%droppable.setTransform(vectorAdd(%pos.getEyePoint(), vectorScale(%pos.getEyeVector(), 1)));
		%droppable.setVelocity(vectorScale(%pos.getEyeVector(), 3));
	}
}

function getIndex(%haystack, %needle, %stringtype)
{
	if(%stringtype $= "")
		%stringtype = "word";

	%stringtype = strLwr(%stringtype);

	if(%stringtype $= "word")
	{
		for(%index = 0; %index < getWordCount(%haystack); %index++)
		{
			if(getWord(%haystack, %index) $= %needle)
				return %index;
		}
	}
	else if(%stringtype $= "field")
	{
		for(%index = 0; %index < getFieldCount(%haystack); %index++)
		{
			if(getField(%haystack, %index) $= %needle)
				return %index;
		}
	}

	return -1;
}

function removeWords(%string, %word)
{
	while(!in(%string, %word))
	{
		for(%a = 0; getWordCount(%string) >= %a; %a++)
		{
			%found = getWord(%string, %a);
			if(strLwr(%found) $= strLwr(%word))
				%string = removeWord(%string, %a);
		}
	}

	return %string;
}

function removeFields(%string, %field)
{
	while(!inFields(%string, %word))
	{
		for(%a = 0; getFieldCount(%string) >= %a; %a++)
		{
			%found = getField(%string, %a);
			if(strLwr(%found) $= strLwr(%word))
				%string = removeField(%string, %a);
		}
	}

	return %string;
}

function in(%a, %b)
{
	for(%i = 0; %i < getWordCount(%a); %i++)
	{
		if(getWord(%a, %i) $= %b)
			return true;
	}

	return false;
}

function inFields(%a, %b)
{
	for(%i = 0; %i < getFieldCount(%a); %i++)
	{
		if(getField(%a, %i) $= %b)
			return true;
	}

	return false;
}

function getInstances(%string, %character)
{
	%count = 0;

	for(%i = 0; %i < strLen(%string); %i++)
	{
		if(getSubStr(%string, %i, 1) == %character)
			%count++;
	}

	return %count;
}

function pad(%number, %length)
{
	if(%length $= "")
		%length = 2;

	while(strLen(%number) < %length)
	{
		%number = "0" @ %number;
	}

	return %number;
}