// ======================================================================
// Project				-		CityMod
// Description			-		Adds new functionality to default functions
// ======================================================================
// Sections
//   1: gameConnection Functions
//      1.1: Core
//      1.2: Bodies
//   2: Player Functions
//   3: fxDTSBrick Functions
// ======================================================================
package CityMod_Core
{
	// ============================================================
	// Section 1 - gameConnection Functions
	// ============================================================

	//function GameConnection::onConnectRequest(%this, %netAddress, %lanName, %netName, %prefix, %suffix, %int, %g, %h, %i)
	//{
	//	%r = Parent::onConnectRequest(%this, %netAddress, %lanName, %netName, %prefix, %suffix, %int, %g, %h, %i);
	//
	//	%DateTime = getWord(getDateTime(), 0);
	//	%DateTime = nextToken(%DateTime, "month", "/");
	//	%DateTime = nextToken(%DateTime, "day", "/");
	//	%DateTime = nextToken(%DateTime, "year", "/");
	//
	//	%weekday = ((%day + ((13 * (%month + 1)) / 5) + %year + (%year / 4) + 105) % 7) - 1;
	//
	//	//if(%weekday == 1 || %weekday == 2 || %weekday == 3 || %weekday == 4) //Monday, Tuesday, Wednesday, Thursday
	//	//{
	//	//	if($CM::WhiteList && !in($CM::WhiteList::BL_IDs, %this.bl_id) && (%this.bl_id != getNumKeyID()))
	//	//	{
	//	//		return "You're not on the Whitelist for CityMod!";
	//	//		%NoC = 0;
	//	//	}
	//	//}
	//
	//	talk(%this SPC %this.bl_id);
	//
	//	return %r;
	//}

	// ------------------------------------------------------------
	// Section 1.1 - Core
	// ------------------------------------------------------------

	//The closest stable function to when a player joins a server
	function gameConnection::AutoAdminCheck(%client)
	{
		CMPlayerData.addData(%client.bl_id);
		
		if(!isFile("Config/Server/CityMod/Players/" @ %client.bl_id @ ".dat") || getWord(CMPlayerData.data[%client.bl_id].Value["Name"], 0) !$= %client.name)
			CMPlayerData.data[%client.bl_id].Value["Name"] = %client.name SPC GetLastName();
		
		parent::AutoAdminCheck(%client);
	}
	
	//Called when a player first spawns
	function gameConnection::onClientEnterGame(%client)
	{
		parent::onClientEnterGame(%client);
		CityModMini.addMember(%client);
		
		if(CMPlayerData.data[%client.bl_id].Value["Reputation"] < 100)
			CMPlayerData.data[%client.bl_id].Value["Reputation"]++;
		else
			CMPlayerData.data[%client.bl_id].Value["Reputation"] = 100;
		
		%client.setScore(CMPlayerData.data[%client.bl_id].Value["Reputation"]);
		CMPlayerData.data[%client.bl_id].Value["IP"] = %client.getRawIP();

		commandtoclient(%client, 'CM_openHUD');
		%client.schedule(500,"UpdateHUD");
	}
	
	function gameConnection::onClientLeaveGame(%client)
	{
		CMPlayerData.saveData(%client.bl_id);

		parent::onClientLeaveGame(%client);
	}
	
	//Called every time a player spawns
	function gameConnection::spawnPlayer(%client)
	{
		parent::spawnPlayer(%client);
			
		%client.player.giveTool("hammerItem");
		%client.player.giveTool("wrenchItem");
		%client.player.giveTool("printGun");

		%client.player.isPlayer = true;
		%client.UpdateHUD();
		%client.updateInventory();
	}

	//Thanks Port for the function to give players tools
	function Player::giveTool(%this, %tool) 
	{
		if(!isObject(%tool) || %tool.getClassName() !$= "ItemData")
			error("Player::giveTool - Invalid tool given");

		%tool = %tool.getID();
		%slots = %this.getDataBlock().maxTools;

		for(%i = 0; %i < %slots; %i++)
		{
			if(!isObject(%this.tool[%i]))
			{
				%this.tool[%i] = %tool;

				if(isObject(%this.client))
					messageClient(%this.client, 'MsgItemPickup', '', %i, %tool, true);

				return true;
			}
		}

		return false;
	}

	function gameConnection::createPlayer(%client, %spawnPos)
	{
		if(%client.inJail())
			%spawn = CMJailSpawns.getRandomSpawn();
		else
		{
			%brickGroup = %client.brickGroup;
				
			if(%brickGroup.spawnCount > 0)
			{
				%spawn = %brickGroup.getRandomSpawn();
			}
			else
			{
				%spawn = CMCitySpawns.getRandomSpawn();
			}
		}
		if(isObject(%spawn))
			%spawnPos = %spawn.getPosition();

		return parent::createPlayer(%client, %spawnPos);
	}

	// ------------------------------------------------------------
	// Section 1.2 - Bodies
	// ------------------------------------------------------------

	function gameConnection::createCorpse(%this)
	{
		if(!isObject(%this.player))
			return 0;

		%corpse = new AIPlayer()
		{
			dataBlock = %this.player.getDataBlock();
			client = %this;

			isCorpse = true;
		};

		if(!isObject(BodyGroup))
			MissionCleanup.add(new SimGroup(BodyGroup));

		BodyGroup.add(%corpse);

		%corpse.vehicle = %this.createcorpseVehicle();
		%corpse.vehicle.mountObject(%corpse, 0);

		%player = %this.player;
		%this.player = %corpse;

		%this.applyBodyParts();
		%this.applyBodyColors();

		%this.player = %player;

		//%this.applyAppearanceTo(%corpse);
		return %corpse;
	}

	function gameConnection::createCorpseVehicle(%this)
	{
		if(!isObject(%this.player))
			return 0;

		%vehicle = new WheeledVehicle()
		{
			dataBlock = corpseVehicle;
			client = %this;
			isCorpseVehicle = true;
		};

		MissionCleanup.add(%vehicle);

		%position = vectorAdd(%this.player.getPosition(), "0 0 0.15");
		%rotation = getWords(%this.player.getTransform(), 3, 6);

		if(%this.player.isCrouched())
			%crouched = true;

		%vehicle.setTransform(%position SPC %rotation);

		%ang = vectorSub(%this.player.getHackPosition(), %this.player.lastDamagePos);
		%ang = vectorCross("0 0 1", vectorScale(vectorNormalize(setWord(%ang, 2, 0)), 4));

		%vehicle.setVelocity(vectorScale(%this.player.getVelocity(), 1.5));

		if(!%crouched)
			%vehicle.setAngularVelocity(%ang);

		%vehicle.monitorCorpseVelocity(0);

		return %vehicle;
	}

	function WheeledVehicle::monitorCorpseVelocity(%this, %lastSpeed)
	{
		cancel(%this.monitorCorpseVelocity);
		%speed = vectorLen(%this.getVelocity());

		if(isObject(%this.getMountedObject(0)) && (%lastSpeed >= 4) && %lastSpeed - %speed >= 1.5)
		{
			%pos = %this.getWorldBoxCenter();
			%this.getMountedObject(0).doDripBlood(true);

			if(%lastSpeed >= 12)
			{
				serverPlay3D(CMS_ImpactHard, %pos);
				%damage = %lastSpeed * 4;
				%this.getMountedObject(0).damage(%this.getMountedObject(0), %pos, %damage);
			}
			else
				serverPlay3D(CMS_ImpactSoft, %pos);
		}

		%this.monitorCorpseVelocity = %this.schedule(50, "monitorCorpseVelocity", %speed);
	}

	function Armor::onMount(%this, %obj, %mount, %slot)
	{
		Parent::onMount(%this, %obj, %mount, %slot);

		if(!%obj.isCorpse || %slot != 0)
			return;

		if(%mount.isCorpseVehicle)
		{
			%obj.playThread(3, "root");
		}
		else
		{
			%mount.playThread(3, "ArmReadyBoth");

			%obj.playThread(3, "death1");
			%obj.setTransform("0 0 0 0 0 -1 -1.5709");
		}
	}

	function Armor::onRemove(%this, %obj)
	{
		if(%obj.isCorpse && isObject(%obj.vehicle))
			%obj.vehicle.delete();

		Parent::onRemove(%this, %obj);
	}

	function Armor::damage(%this, %obj, %src, %pos, %damage, %type)
	{
		if(!%obj.isCorpse)
			Parent::damage(%this, %obj, %src, %pos, %damage, %type);
	}

	function AIPlayer::setVelocity(%this, %velocity)
	{
		Parent::setVelocity(%this, %velocity);

		%mount = %this.getObjectMount();

		if(%this.isCorpse && isObject(%mount) && (%mount.getDataBlock() == nameToID("CorpseVehicle")))
			%mount.setVelocity(%velocity);
	}

	function AIPlayer::addVelocity(%this, %velocity)
	{
		Parent::addVelocity(%this, %velocity);

		%mount = %this.getObjectMount();

		if(%this.isCorpse && isObject(%mount) && (%mount.getDataBlock() == nameToID("CorpseVehicle")))
			%mount.setVelocity(vectorAdd(%mount.getVelocity(), %velocity));
	}

	function CorpseVehicle::onTrigger(%this, %obj, %slot, %state)
	{
		if(isObject(%obj.client.camera))
		{
			%obj.client.setControlObject(%obj.client.camera);
			Observer.onTrigger(%obj.client.camera, %slot, %state);

			if(%obj.client.getControlObject() == %obj.client.camera)
				%obj.client.setControlObject(%obj);
		}
	}

	// ============================================================
	// Section 2 - Player Functions
	// ============================================================

	function Armor::onTrigger(%this,%player,%slot,%val)
	{
		%return = parent::onTrigger(%this, %player, %slot, %val);

		if(%slot != 4 || %val != 1)
			return %return;

		%vector = vectorAdd(vectorScale(vectorNormalize(%player.getEyeVector()), 2.5), %player.getEyePoint());
		%target = containerRayCast(%player.getEyePoint(), %vector, $TypeMasks::PlayerObjectType, %player);
		%client = %player.client;

		if(!isObject(%target))
			return %return;

		if(%target.getClassName() $= "AIPlayer")
		{
			if(%target.isCorpse)
			{
				if((%player.CM_LootingTimeout + 2) > $Sim::Time)
					return %return;
					
				serverPlay3D(CMS_Looting, %player.getPosition());
					
				%stashamt = "-1";

				if(%target.money != 0)
				{
					%stashamt++;
					%stash[%stashamt] = "Money";
				}
				if(%target.metal != 0)
				{
					%stashamt++;
					%stash[%stashamt] = "Metal";
				}
				if(%target.wood != 0)
				{
					%stashamt++;
					%stash[%stashamt] = "Wood";
				}
				if(%target.coal != 0)
				{
					%stashamt++;
					%stash[%stashamt] = "Coal";
				}
				if(%target.plastic != 0)
				{
					%stashamt++;
					%stash[%stashamt] = "Plastic";
				}
				if(%target.tool != 0 && isObject(%target.tool) && %target.tool.getID() != CMCuffsImage.getID())
				{
					%stashamt++;
					%stash[%stashamt] = "Tool";
				}
				
				if(%stashamt $= "-1")
				{
					%player.client.schedule(1000, "CMNotify", "Looting", "This body doesn't have any valuables", "information");
					%player.CM_LootingTimeout = $Sim::Time;
					return %return;
				}
					
				%selected = %stash[getRandom(0, %stashamt)];
					
				switch$(%selected)
				{
					case "Money": 
						%value = getRandom(1, %target.money); %target.money -= %value; CMPlayerData.data[%player.client.bl_id].Value["Money"] += %value;
						%player.client.schedule(1000, "CMNotify", "Looting", "You looted $" @ %value, "money");
					case "Metal": 
						%value = getRandom(1, %target.metal); %target.metal -= %value; %player.client.schedule(1000, "addInventory", "Metal", %value, "looted");
					case "Wood": 
						%value = getRandom(1, %target.wood); %target.wood -= %value; %player.client.schedule(1000, "addInventory", "Wood", %value, "looted");
					case "Coal": 
						%value = getRandom(1, %target.coal); %target.coal -= %value; %player.client.schedule(1000, "addInventory", "Coal", %value, "looted");
					case "Plastic": 
						%value = getRandom(1, %target.plastic); %target.plastic -= %value; %player.client.schedule(1000, "addInventory", "Plastic", %value, "looted");
					case "Tool":
						%player.client.schedule(1000, "CMNotify", "Looting", "You looted a" SPC %target.getMountedImage(0).item.uiName, "information");
						%player.giveTool(%target.getMountedImage(0).item);
						%target.unMountImage(0);
						%target.tool = 0;
				}
					
				%player.client.schedule(1000, "UpdateHUD");
				%player.CM_LootingTimeout = $Sim::Time;
			}
		}
		return %return;
	}

	// ============================================================
	// Section 3 - fxDTSBrick Functions
	// ============================================================

	function fxDTSBrick::onActivate(%this, %player, %position, %rotation)
	{
		parent::onActivate(%this, %player, %position, %rotation);

		if(!isObject(%player))
			return;

		%datablock = %this.getDatablock();

		if(%datablock.CMBrick && %datablock.CMBrickType $= "INTERACTIVE")
		{
			if(%datablock.isKiosk && strLen(%this.getName()))
			{
				serverPlay3d(CMS_Activate, %this.getPosition());
				switch$(strReplace(strLwr(%this.getName()), "_", ""))
				{
					case "bank": commandtoClient(%player.client, 'CM_openGUI', "bank");
					case "realestate": commandtoClient(%player.client, 'CM_openGUI', "realestate");
					case "police": commandtoClient(%player.client, 'CM_openGUI', "police");
					case "group": commandtoClient(%player.client, 'CM_openGUI', "groups");
					case "school": commandtoClient(%player.client, 'CM_openGUI', "school");
				}
			}
			else if(%datablock.isMachine)
			{
				switch$(strLwr(%datablock.machineType))
				{
					case "processor":
						if($CM::Formulas[strReplace(strLwr(%this.getName()), "_", "")] !$= "")
						{
							%player.currentMachinery = %this.getID();
							commandtoClient(%player.client, 'CM_openGUI', "machinery");

							%this.updateClients = ltrim(%this.updateClients SPC %player.client.getID());
							return;
						}
						else if(%player.client.bl_id == %this.getGroup().bl_id)
						{
							commandToClient(%player.client, 'centerPrint', "<font:Impact:24><color:555555>You didn't choose a Formula for this Machine to use!" NL
							"<color:ffffff>To choose a Formula to use, name the Machine the name of your desired Formula.", 3);
							return;
						}
						else
						{
							commandToClient(%player.client, 'centerPrint', "<font:Impact:24><color:ffffff>This machine hasn't been configured properly yet.", 2);
							return;
						}
					case "smelter":
						talk("Clicked");
				}
			}
			else if(%datablock.isBallotBox)
			{
				if(!isObject(CMElectionSO))
					commandToClient(%player.client, 'centerPrint', "<font:Impact:24><color:ffffff>There are no elections currently running!", 1);
				else
					commandtoClient(%player.client, 'CM_openGUI', "voting");
			}
		}
	}

	function fxDTSBrick::onPlant(%brick)
	{
		%return = Parent::onPlant(%brick);
		if(!isObject(%brick))
			return %return;

		%client = %brick.getGroup().client;
		
		if(!isObject(isValidtoPlant(%brick).brick) && %brick.getDataBlock().CMBrickType !$= "LOT" && %brick.getDataBlock().spawnType !$= "Homeless" && !%client.devMode)
		{
			%val = isValidtoPlant(%brick);
			switch$(%val)
			{
				case "OUT_OF_LOT":
					commandToClient(%client, 'centerPrint', "<font:Impact:24><color:FF0000>Brick Out of Lot!", 1);
				case "NO_LOT":
					commandToClient(%client, 'centerPrint', "<font:Impact:24><color:FF0000>No Lot Found!", 1);
			}
				
			%brick.schedule(0, "delete");
			return %return;
		}
		
		%lot = isValidtoPlant(%brick).brick;
		%brick.lot = %lot;
		
		if(%brick.getDatablock().CMBrick)
		{
			if(%brick.getDatablock().CMAdminBrick && !%client.isAdmin)
			{
				commandToClient(%client, 'centerPrint', "<font:Impact:24><color:FF0000>You're not allowed to plant this brick!", 1);
				%brick.schedule(0, "delete");
				return %return;
			}
			
			switch$(%brick.getDataBlock().CMBrickType)
			{
				case "LOT":
					switch$(%brick.getDatablock().lotType)
					{
						case "Residential":
							%brick.createTrigger("CMLotTrigger");
							%brick.lotID = CMLots.getNextID();
							%brick.calculateLotCost();
							%brick.beingSold = true;
							%brick.govSeller = true;
						case "Commercial":
							%brick.createTrigger("CMLotTrigger");
							%brick.lotID = CMLots.getNextID();
							%brick.calculateLotCost();
							%brick.beingSold = true;
							%brick.govSeller = true;
						case "Industrial":
							%brick.createTrigger("CMLotTrigger");
							%brick.lotID = CMLots.getNextID();
							%brick.calculateLotCost();
							%brick.beingSold = true;
							%brick.govSeller = true;
					}
					CMLots.addLot(%brick);
					//if(%brick.groupOwnership !$= "")
					//	%brick.lotID = CMLotData.getLotID("G" @ %brick.groupOwnership);
					//else
					//	%brick.lotID = CMLotData.getLotID("B" @ %client.bl_id);
					//CMLotData.lot[%brick.lotID] = %brick;
				case "RESOURCE":
					if(%brick.getDatablock().isStone)
					{
						%brick.regenStone();
					}
					else if(%brick.getDatablock().isTree)
					{
						%brick.regenTree();
					}
					else if(%brick.getDatablock().isStorageZone)
					{
						if(isObject(%lot.storageZone))
						{
							commandToClient(%client, 'centerPrint', "<font:Impact:24><color:FF0000>Maximum of 1 Storage Zone /per Lot", 2);
							%brick.schedule(0, "delete");
							return %return;
						}

						%lot.storageZone = %brick;
						%brick.updateStorageZone();
					}
				case "CRATE":
					if(%client.getInventory("all", %brick.getDatablock().type) < $CM::Prefs::Default::CrateQuantity)
					{
						commandToClient(%client, 'centerPrint', "<font:Impact:24><color:FF0000>Insufficient" SPC %brick.getDatablock().type @ "!", 1);
						%brick.schedule(0, "delete");
						return %return;
					}

					%client.removeInventory(%brick.getDatablock().type, $CM::Prefs::Default::CrateQuantity, "was stored in this Crate");

					if(isObject(%brick.lot.storageZone) && %brick.lot.storageZone.isInPerimeter(%brick))
					{
						%client.CMNotify("Storage Zone", %brick.getDatablock().type SPC "Crate linked to Storage Zone");
						%brick.lot.storageZone.updateStorageZone();
					}
				case "INTERACTIVE":
					if(%brick.getDatablock().isMachine)
					{
						if(%lot.getDatablock().lotType !$= "Industrial")
						{
							commandToClient(%client, 'centerPrint', "<font:Impact:24><color:FF0000>This Lot isn't zoned for Industrial Bricks!", 1);
							%brick.schedule(0, "delete");
							return %return;
						}
						if(%client.getInventory("all", "Metal") < 5)
						{
							commandToClient(%client, 'centerPrint', "<font:Impact:24><color:FF0000>Insufficient Metal!", 1);
							%brick.schedule(0, "delete");
							return %return;
						}
						else
						{
							%client.removeInventory("Metal", 5, "was used in this Machine");
							%brick.isMachineRunning = false;
							%brick.updateClients = "";
							%brick.machineProgress = 0;
						}
					}
					if(%brick.getDatablock().isShelf)
					{
						if(%lot.getDatablock().lotType !$= "Commercial")
						{
							commandToClient(%client, 'centerPrint', "<font:Impact:24><color:FF0000>This Lot isn't zoned for Commercial Bricks!", 1);
							%brick.schedule(0, "delete");
							return %return;
						}
						if(%client.getInventory("all", "Plastic") < 3)
						{
							commandToClient(%client, 'centerPrint', "<font:Impact:24><color:FF0000>Insufficient Plastic!", 1);
							%brick.schedule(0, "delete");
							return %return;
						}
						else
							%client.removeInventory("plastic", 3, "was used in this Shelf");
					}
					if(%brick.getDatablock().isCashRegister)
					{
						if(%lot.getDatablock().lotType !$= "Commercial")
						{
							commandToClient(%client, 'centerPrint', "<font:Impact:24><color:FF0000>This Lot isn't zoned for Commercial Bricks!", 1);
							%brick.schedule(0, "delete");
							return %return;
						}
						%brick.foodPricing = $CM::Prefs::Default::FoodPrice["Tiny"] SPC $CM::Prefs::Default::FoodPrice["Small"] SPC
											 $CM::Prefs::Default::FoodPrice["Medium"] SPC $CM::Prefs::Default::FoodPrice["Large"] SPC
											 $CM::Prefs::Default::FoodPrice["Oversized"] SPC $CM::Prefs::Default::FoodPrice["XXL"];
					}
				case "SPAWN":
					switch$(%brick.getDatablock().spawnType)
					{
						case "Jail":
							CMJailSpawns.spawn[CMJailSpawns.spawnCount] = %brick;
							CMJailSpawns.spawnCount += 1;
						case "Homeless":
							CMCitySpawns.spawn[CMCitySpawns.spawnCount] = %brick;
							CMCitySpawns.spawnCount += 1;
						case "Home":
							if(%client.brickGroup.spawnCount $= "")
								%client.brickGroup.spawnCount = 0;

							if((%client.getInventory("all", "metal") - 5) < 0)
							{
								commandToClient(%client, 'centerPrint', "<font:Impact:24><color:FF0000>Insufficient Metal!", 1);
								%brick.schedule(0, "delete");
								return %return;
							}

							%client.brickGroup.spawn[%client.brickGroup.spawnCount] = %brick;
							%client.brickGroup.spawnCount += 1;
							%client.removeInventory("metal", 5, "was used in this spawn");
					}
			}

			if(%brick.getDatablock().staticColorID !$= "")
				%brick.setColor(%brick.getDatablock().staticColorID);
			
			return %return;
		}
		else
		{
			if(%client.devMode)
				return %return;

			if(strLwr(getWord(%brick.getDatablock().uiName, 1)) $= "clump")
			{
				%type = strLwr(firstWord(%brick.getDatablock().uiName));

				if((%client.getInventory("all", %type) - $CM::Prefs::Default::ClumpQuantity) < 0)
				{
					commandToClient(%client, 'centerPrint', "<font:Impact:24><color:FF0000>You Need 4 Raw" SPC firstWord(%brick.getDatablock().uiName) @ "!", 1);
					%brick.schedule(0, "delete");
					return %return;
				}

				%client.removeInventory(%type, $CM::Prefs::Default::ClumpQuantity, "was used in this" SPC %brick.getDatablock().uiName);
			}
			else
			{
				%cost = mCeil((%brick.getDatablock().brickSizeX + %brick.getDatablock().brickSizeY + %brick.getDatablock().brickSizeZ) / 8);
				
				%downbrick = %brick.getDownBrick(0);
				if(isObject(%downbrick) && %downbrick.getDatablock().CMBrick && (%downbrick.getDatablock().machineType $= "smelter") && !%downbrick.getDatablock().isOpen)
				{
					commandToClient(%client, 'centerPrint', "<font:Impact:24><color:FF0000>You have to open the Smelter before placing an Ore Clump inside!", 1);
					%brick.schedule(0, "delete");
					return %return;
				}

				if((%client.getInventory("all", "plastic") - %cost) < 0)
				{
					commandToClient(%client, 'centerPrint', "<font:Impact:24><color:FF0000>Insufficient Plastic!", 1);
					%brick.schedule(0, "delete");
					return %return;
				}
				
				%name = strReplace(%brick.dataBlock, "brick", "");
				%name = strReplace(%name, "Data", "");
				%client.removeInventory("plastic", %cost, "was used in this" SPC %name);
			}
			
			return %return;
		}
		return %return;
	}
	
	function fxDTSBrick::onLoadPlant(%brick)
	{
		%return = parent::onLoadPLant(%brick);
		if(%brick.getDataBlock().CMBrick)
		{
			switch$(%brick.getDataBlock().CMBrickType)
			{
				case "LOT":
					%brick.schedule(10, "createTrigger", "CMLotTrigger");
					if(%brick.lotID !$= "")
					{
						if(CMLots.isGroupOwnedLot(%brick.lotID) != false)
							%brick.groupOwnership = CMLots.isGroupOwnedLot(%brick.lotID);
					}
					else
					{
						%brick.lotID = CMLots.getNextID();
					}
					CMLots.addLot(%brick);
					//CMLotData.lot[%brick.lotID] = %brick;
				case "RESOURCE":
					if(%brick.getDatablock().isStone){ %brick.schedule(10, "regenStone"); }
					if(%brick.getDatablock().isTree){ %brick.schedule(10, "regenTree"); }
				case "INTERACTIVE":
					if(%brick.getDatablock().isMachine)
					{
						%brick.isMachineRunning = false;
						%brick.updateClients = "";
						%brick.machineProgress = 0;
					}
					if(%brick.getDatablock().isShelf)
					{
						if(%brick.dataBlock $= "brickCMShelfData")
							%brick.contents = 0;
						else if(%brick.dataBlock $= "brickCM1ThirdShelfData")
							%brick.contents = 2;
						else if(%brick.dataBlock $= "brickCM2ThirdShelfData")
							%brick.contents = 4;
						else if(%brick.dataBlock $= "brickCMFullShelfData")
							%brick.contents = 6;
					}
					if(%brick.getDatablock().isCashRegister)
					{
						%brick.foodPricing = $CM::Prefs::Default::FoodPrice["Tiny"] SPC $CM::Prefs::Default::FoodPrice["Small"] SPC
											 $CM::Prefs::Default::FoodPrice["Medium"] SPC $CM::Prefs::Default::FoodPrice["Large"] SPC
											 $CM::Prefs::Default::FoodPrice["Oversized"] SPC $CM::Prefs::Default::FoodPrice["XXL"];
					}
			}
		}
		return %return;
	}

	function fxDTSBrick::onRemove(%brick)
	{
		%return = parent::onRemove(%brick);

		if(!isObject(%brick) || !%brick.isPlanted)
			return %return;

		%client = %brick.getGroup().client;

		if(%brick.getDataBlock().CMBrick)
		{
			switch$(%brick.getDataBlock().CMBrickType)
			{
				case "CRATE":
					%client.addInventory(%brick.getDataBlock().type, $CM::Prefs::Default::CrateQuantity, "smashed the Crate and received");
				case "LOT":
					CMLots.removeLot(%brick);
			}
		}
		return %return;
	}

	function fxDTSbrick::setColor(%brick, %color)
	{
		if(%brick.getDatablock().staticColorID !$= "")
			parent::setColor(%brick, %brick.getDatablock().staticColorID);
		else
			parent::setColor(%brick, %color);
	}
};
if(isPackage(CityMod_Core))
	deactivatepackage(CityMod_Core);
activatepackage(CityMod_Core);