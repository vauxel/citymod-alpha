// ============================================================
// Project				-		CityMod
// Description			-		Boot-Up Code
// ============================================================

function execFolder(%folder)
{
	for(%i = findFirstFile(%folder @ "*.cs"); %i !$= ""; %i = findNextFile(%folder @ "*.cs"))
	{
		exec(%i);
	}
}

// Core

exec("./Datablocks.cs");
exec("./Variables.cs");
exec("./Core.cs");
exec("./Commands.cs");
exec("./Package.cs");
exec("./Database.cs");

execFolder("./scripts/");
execFolder("./support/");

// Items

execFolder("./items/tools/");

//This is so much yes.
//exec("./items/weapons/pistol.cs");
//exec("./items/weapons/shotgun.cs");
//exec("./items/weapons/sniper.cs");
//exec("./items/weapons/assaultrifle.cs");
//exec("./items/weapons/dualpistol.cs");
//exec("./items/weapons/submachinegun.cs");
//exec("./items/weapons/knife.cs");

//exec("./items/tools/baton.cs");
//exec("./items/tools/rope.cs");
//exec("./items/tools/handcuffs.cs");
//exec("./items/tools/camera.cs");

CityMod_Initiate();