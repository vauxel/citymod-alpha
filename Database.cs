// ============================================================
// Project				-		CityMod
// Description			-		CityMod Saving Database
// ============================================================
// Sections
// 	 1: CityModPlayerDB Saving
// 	 2: CityModGroupDB Saving
// 	 3: CityModCityDB Saving
// ============================================================

// ============================================================
// Section 1 - CityModPlayerDB Saving
// ============================================================
function CityModPDB::onAdd(%this)
{
	%this.Name = %this.getName();
	if(%this.filePath $= "")
	{
		%this.schedule(0, "delete");
		echo(%this.name @" needs a filepath.");
		return 0;
	}
	%this.fieldCount = 0;
	%this.AutoLoadCount = 0;
	if(%this.AutomaticLoading && isfile(%this.filepath @"AutoLoad.txt"))
	{
		%file = new fileObject();
		%file.openForRead(%this.filepath @"AutoLoad.txt");
		while(!%file.isEOF())
		{
			%loadcount++;
			%line = %file.readLine();
			%this.LoadData(%line);
			%this.AutoLoad[%this.AutoLoadCount] = %line;
			%this.AutoLoadCount++;
		}
		%file.close();
		%file.delete();
		warn("[CMDB] :: CityMod has loaded " @ %loadcount @ " keys.");
	}
}

function CityModPDB::addField(%this, %name, %default)
{
	if(%name $= "" || %this.FieldExist[%name])
		warn("[CMDB] :: Field "@ %name @" already exists.");
	%this.FieldName[%this.FieldCount] = %name;
	%this.FieldValue[%this.FieldCount] = %default;
	%this.FieldDefault[%name] = %default;
	%this.FieldExist[%name] = 1;
	%this.FieldCount++;
}

function CityModPDB::loadData(%this,%key)
{
	if(isObject(%this.Data[%key]))
	{
		warn("[CMDB] :: Key " @ %key @ " has already been loaded.");
		return 0;
	}
	if(!isFile(%this.filepath @ %key @".dat"))
		return %this.addData(%key);
	%this.Data[%key] = new ScriptObject(){class = "CityModPDBData";};
	%file = new fileObject();
	%file.openForRead(%this.filepath @ %key @".dat");
	while(!%file.isEOF())
	{
		%line = %file.readLine();
		%equalpos = striPos(%line,":");
		%this.Data[%key].Value[getSubStr(%line,0,%equalpos)] = getSubStr(%line,%equalpos+1,500);
	}
	%file.close();
	%file.delete();
}

function CityModPDB::AddData(%this,%key)
{
	if(isObject(%this.Data[%key]) || isFile(%this.filepath @ %key @".dat"))
	{
		warn("[CMDB] :: Key "@ %key @" already exists.");
		return 0;
	}
	%this.Data[%key] = new ScriptObject(){class = "CityModPDBData";};
	for(%a = 0; %a < %this.FieldCount;%a++)
		%this.Data[%key].Value[%this.fieldname[%a]] = %this.fieldValue[%a];
	if(%this.AutomaticLoading)
	{
		%this.AutoLoad[%this.AutoLoadCount] = %key;
		%this.AutoLoadCount++;
		%file = new fileObject();
		%file.openForWrite(%this.FilePath @"AutoLoad.txt");
		for(%a = 0;%a<%this.AutoLoadCount;%a++)
			%file.writeline(%this.AutoLoad[%a]);
		%file.close();
		%file.delete();
	}
	warn("[CMDB] :: Key "@ %key @" has been added to "@ %this.name @".");
}

function CityModPDB::ResetData(%this,%key)
{
	if(!isObject(%this.Data[%key]))
	{
		warn("[CMDB] :: Key " @ %key @ " does not exist.");
		return 0;
	}
	for(%a = 0; %a < %this.FieldCount; %a++)
		%this.Data[%key].Value[%this.fieldname[%a]] = %this.fieldValue[%a];
	warn("[CMDB] :: Key " @ %key @ " has been reset.");
}

function CityModPDB::SaveData(%this,%key)
{
	if(!isobject(%this.Data[%key]))
	{
		warn("[CMDB] :: Key " @ %key @ " does not exist in the Database.");
		return 0;
	}
	%file = new fileObject();
	%file.openForWrite(%this.FilePath @ %key @".dat");
	for(%a = 0; %a < %this.FieldCount; %a++)
		%file.writeLine(%this.FieldName[%a] @":"@ %this.Data[%key].Value[%this.FieldName[%a]]);
	%file.close();
	%file.delete();
}

// ============================================================
// Section 2 - CityModGroupDB Saving
// ============================================================
function CityModGDB::onAdd(%this)
{
	%this.Name = %this.getName();
	if(%this.filePath $= "")
	{
		%this.schedule(0, "delete");
		echo(%this.name @" needs a filepath.");
		return 0;
	}
	%this.fieldCount = 0;
	%this.AutoLoadCount = 0;
	if(%this.AutomaticLoading && isfile(%this.filepath @"AutoLoad.txt"))
	{
		%file = new fileObject();
		%file.openForRead(%this.filepath @"AutoLoad.txt");
		while(!%file.isEOF())
		{
			%loadcount++;
			%line = %file.readLine();
			%this.LoadData(%line);
			%this.AutoLoad[%this.AutoLoadCount] = %line;
			%this.AutoLoadCount++;
		}
		%file.close();
		%file.delete();
		warn("[CMDB] :: CityMod has loaded " @ %loadcount @ " Groups.");
	}
		
}

function CityModGDB::addField(%this, %name, %default)
{
	if(%name $= "" || %this.FieldExist[%name])
		warn("[CMDB] :: Field "@ %name @" already exists.");
	%this.FieldName[%this.FieldCount] = %name;
	%this.FieldValue[%this.FieldCount] = %default;
	%this.FieldExist[%name] = 1;
	%this.FieldCount++;
}

function CityModGDB::LoadData(%this,%id)
{
	if(isObject(%this.Data[%id]))
	{
		warn("[CMDB] :: Group #" @ %id @ " has already been loaded.");
		return 0;
	}
	if(!isFile(%this.filepath @ %id @ ".dat"))
		return %this.addData(%id);
	%this.Data[%id] = new ScriptObject(){class = "CityModGDBData";};
	%file = new fileObject();
	%file.openForRead(%this.filepath @ %id @ ".dat");
	while(!%file.isEOF())
	{
		%line = %file.readLine();
		%equalpos = striPos(%line,":");
		%this.Data[%id].Value[getSubStr(%line,0,%equalpos)] = getSubStr(%line,%equalpos+1,500);
	}
	%file.close();
	%file.delete();
}

function CityModGDB::AddData(%this,%id)
{
	if(isObject(%this.Data[%id]) || isFile(%this.filepath @ %id @".dat"))
	{
		warn("[CMDB] :: Group #"@ %id @" already exists.");
		return 0;
	}
	%this.Data[%id] = new ScriptObject(){class = "CityModGDBData";};
	for(%a = 0; %a < %this.FieldCount;%a++)
		%this.Data[%id].Value[%this.fieldname[%a]] = %this.fieldValue[%a];
	if(%this.AutomaticLoading)
	{
		%this.AutoLoad[%this.AutoLoadCount] = %id;
		%this.AutoLoadCount++;
		%file = new fileObject();
		%file.openForWrite(%this.FilePath @"AutoLoad.txt");
		for(%a = 0;%a<%this.AutoLoadCount;%a++)
			%file.writeline(%this.AutoLoad[%a]);
		%file.close();
		%file.delete();
	}
	warn("[CMGDB] :: Group #"@ %id @" has been added to "@ %this.name @".");
}

function CityModGDB::ResetData(%this,%id)
{
	if(!isObject(%this.Data[%id]))
	{
		warn("[CMGDB] :: Group #" @ %id @ " does not exist.");
		return 0;
	}
	for(%a = 0; %a < %this.FieldCount; %a++)
		%this.Data[%id].Value[%this.fieldname[%a]] = %this.fieldValue[%a];
	warn("[CMGDB] :: Group #" @ %id @ " has been reset.");
}

function CityModGDB::SaveData(%this,%id)
{
	if(!isobject(%this.Data[%id]))
	{
		warn("[CMGDB] :: Group #" @ %id @ " does not exist in the Database.");
		return 0;
	}
	%file = new fileObject();
	%file.openForWrite(%this.FilePath @ %id @".dat");
	for(%a = 0; %a < %this.FieldCount; %a++)
		%file.writeLine(%this.FieldName[%a] @":"@ %this.Data[%id].Value[%this.FieldName[%a]]);
	%file.close();
	%file.delete();
}

function CityModGDB::saveAll(%this,%id)
{
	%file = new fileObject();
	%file.openForRead(%this.FilePath @ "AutoLoad.txt");
	
	while(!%file.isEOF())
	{
		%line = %file.readLine();
		
		if(isObject(%this.Data[%line]))
			%this.saveData(%line);
	}
	
	%file.close();
	%file.delete();
}

// ============================================================
// Section 3 - CityModCityDB Saving
// ============================================================
function CityModCDB::onAdd(%this)
{
	%this.keys = "Funds Mayor TaxRate Conviction_MURDER Conviction_ASSAULT Conviction_ROBBERY";

	%this.def["Funds"] = mFloor($CM::Prefs::Core::CityStarterFunds / 2);
	%this.changable["Funds"] = false;

	%this.def["Mayor"] = 888888;
	%this.changable["Mayor"] = false;

	%this.def["TaxRate"] = 5;
	%this.changable["TaxRate"] = true;
	%this.min["TaxRate"] = 5;
	%this.max["TaxRate"] = 35;

	//Crimes
	%this.def["Conviction_MURDER"] = 10;
	%this.changable["Conviction_MURDER"] = true;
	%this.min["Conviction_MURDER"] = 10;
	%this.max["Conviction_MURDER"] = 60;

	%this.def["Conviction_ASSAULT"] = 5;
	%this.changable["Conviction_ASSAULT"] = true;
	%this.min["Conviction_ASSAULT"] = 5;
	%this.max["Conviction_ASSAULT"] = 20;

	%this.def["Conviction_ROBBERY"] = 10;
	%this.changable["Conviction_ROBBERY"] = true;
	%this.min["Conviction_ROBBERY"] = 10;
	%this.max["Conviction_ROBBERY"] = 30;
}


function CityModCDB::loadData(%this)
{
	if(!isFile("config/server/CityMod/City.dat"))
		return %this.saveData(true);
		
	%file = new fileObject();
	%file.openForRead("config/server/CityMod/City.dat");

	for(%i=0;%i<getWordCount(%this.keys);%i++)
	{
		if(%file.isEOF())
		{
			error("There are more keys than lines!");
			break;
		}
		%key = getWord(%this.keys,%i);
		%line = %file.readLine();
		%equalpos = striPos(%line,":");
		%this.Data[%key] = getSubStr(%line,%equalpos+1,500);
	}

	%file.close();
	%file.delete();
}

function CityModCDB::saveData(%this, %first)
{
	if(%first == false)
	{
		%file = new fileObject();
		%file.openForWrite("config/server/CityMod/City.dat");
		
		for(%i=0;%i<getWordCount(%this.keys);%i++)
		{
			%key = getWord(%this.keys,%i);
			%file.writeLine(%key @ ":" @ %this.Data[%key]);
		}

		%file.close();
		%file.delete();
	}
	else if(%first == true)
	{
		%file = new fileObject();
		%file.openForWrite("config/server/CityMod/City.dat");
		
		for(%i=0;%i<getWordCount(%this.keys);%i++)
		{
			%key = getWord(%this.keys,%i);
			%file.writeLine(%key @ ":" @ %this.def[%key]);
		}

		%file.close();
		%file.delete();
		
		%this.loadData();
	}
}

// ============================================================
// Section 4 - CityModLotDB Saving
// ============================================================
//function CityModLDB::onAdd(%this)
//{
//	%this.keys = "CurrentLotID LotData";
//
//	%this.def["CurrentLotID"] = 0;
//	%this.def["LotData"] = "";
//
//	%this.changable["LotData"] = false;
//	%this.changable["CurrentLotID"] = false;
//}
//
//function CityModLDB::getLotID(%this, %owner) {
//	%this.def["CurrentLotID"] += 1;
//	%this.def["LotData"] = %this.def["LotData"] @ "\t" @ %this.def["CurrentLotID"] @ " " @ %owner;
//	return %this.def["CurrentLotID"];
//}
//
//function CityModLDB::getLotOwner(%this, %id) {
//	%count = getFieldCount(%this.def["LotData"]);
//	for(%i=0;%i<%count;%i++) {
//		%field = getField(%this.def["LotData"], %i);
//		if(firstWord(%field) == %id)
//			return trim(getWord(%field,1));
//	}
//	return -1;
//}
//
//function CityModLDB::getOwnedLots(%this, %owner) {
//	%count = getFieldCount(%this.def["LotData"]);
//	for(%i=0;%i<%count;%i++) {
//		%field = getField(%this.def["LotData"], %i);
//		if(lastWord(%field) == %owner)
//			%data = %data SPC firstWord(%field);
//	}
//	return trim(%data);
//}
//
//function CityModLDB::loadData(%this)
//{
//	if(!isFile("config/server/CityMod/Lots.dat"))
//		return %this.saveData(true);
//		
//	%file = new fileObject();
//	%file.openForRead("config/server/CityMod/Lots.dat");
//
//	for(%i=0;%i<getWordCount(%this.keys);%i++)
//	{
//		if(%file.isEOF())
//		{
//			error("There are more keys than lines!");
//			break;
//		}
//		%key = getWord(%this.keys,%i);
//		%line = %file.readLine();
//		%equalpos = striPos(%line,":");
//		%this.Data[%key] = getSubStr(%line,%equalpos+1,500);
//	}
//
//	%file.close();
//	%file.delete();
//}

//function CityModLDB::saveData(%this, %first)
//{
//	if(%first == false)
//	{
//		%file = new fileObject();
//		%file.openForWrite("config/server/CityMod/Lots.dat");
//		
//		for(%i=0;%i<getWordCount(%this.keys);%i++)
//		{
//			%key = getWord(%this.keys,%i);
//			%file.writeLine(%key @ ":" @ %this.Data[%key]);
//		}
//
//		%file.close();
//		%file.delete();
//	}
//	else if(%first == true)
//	{
//		%file = new fileObject();
//		%file.openForWrite("config/server/CityMod/Lots.dat");
//		
//		for(%i=0;%i<getWordCount(%this.keys);%i++)
//		{
//			%key = getWord(%this.keys,%i);
//			%file.writeLine(%key @ ":" @ %this.def[%key]);
//		}
//
//		%file.close();
//		%file.delete();
//		
//		%this.loadData();
//	}
//}

// ============================================================
// Section 5 - CityModStockMarketDB Saving
// ============================================================
function CityModSMDB::onAdd(%this)
{
	%this.keys = "StockData";

	%this.def["StockData"] = "";
	%this.changable["StockData"] = false;
}


function CityModSMDB::loadData(%this)
{
	if(!isFile("config/server/CityMod/Stocks.dat"))
		return %this.saveData(true);
		
	%file = new fileObject();
	%file.openForRead("config/server/CityMod/Stocks.dat");

	for(%i=0;%i<getWordCount(%this.keys);%i++)
	{
		if(%file.isEOF())
		{
			error("There are more keys than lines!");
			break;
		}
		%key = getWord(%this.keys,%i);
		%line = %file.readLine();
		%equalpos = striPos(%line,":");
		%this.Data[%key] = getSubStr(%line,%equalpos+1,500);
	}

	%file.close();
	%file.delete();
}

function CityModSMDB::saveData(%this, %first)
{
	if(%first == false)
	{
		%file = new fileObject();
		%file.openForWrite("config/server/CityMod/Stocks.dat");
		
		for(%i=0;%i<getWordCount(%this.keys);%i++)
		{
			%key = getWord(%this.keys,%i);
			%file.writeLine(%key @ ":" @ %this.Data[%key]);
		}

		%file.close();
		%file.delete();
	}
	else if(%first == true)
	{
		%file = new fileObject();
		%file.openForWrite("config/server/CityMod/Stocks.dat");
		
		for(%i=0;%i<getWordCount(%this.keys);%i++)
		{
			%key = getWord(%this.keys,%i);
			%file.writeLine(%key @ ":" @ %this.def[%key]);
		}

		%file.close();
		%file.delete();
		
		%this.loadData();
	}
}