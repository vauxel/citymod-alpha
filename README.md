# Welcome to CityMod: Alpha #

### What is CityMod: Alpha? ###

CityMod: Alpha is a city role-playing/simulation mod for Blockland that aims to set itself apart from others of its genre and generally improve the Blockland CityRP experience.  It was built from the ground up to re-write the core problems that have been the demise of many CityRP(G)'s.

### How Can I Contribute? ###

1. Create a new [Branch](https://bitbucket.org/vauxel/citymod/branch)
2. Push any commits you make to your branch
3. Create a [Pull Request](https://bitbucket.org/vauxel/citymod/pull-request/new) to merge your branch with the master and your code will be reviewed and most likely approved

### Basic Contribution Guidelines ###

* Use common sense.  Don't try to commit malicious or exploitative code.
* Keep it mature.  Make nice and respectful comments on code and don't bash others' code.
* Follow a standard.  For a neat codebase, please use tabs instead of spaces, and if possible, [Kernel Style](http://en.wikipedia.org/wiki/Indent_style#Kernel_style)

### Licensing ###

CityMod: Alpha is licensed under the [Creative Commons Attribution-NonCommercial-ShareAlike 4.0 International](http://creativecommons.org/licenses/by-nc-sa/4.0/) (CC BY-NC-SA 4.0) License