// ============================================================
// Project				-		CityMod
// Description		-		Pickaxe Item
// ============================================================
// Sections
// 	 1: Datablocks
// 	 2: Functions
// ============================================================

// ============================================================
// Section 1 - Datablocks
// ============================================================
if(!isObject(CMPickaxeItem))
{
	datablock ExplosionData(CMPickaxeExplosion)
	{
		lifeTimeMS = 400;

		soundProfile = "";

		particleEmitter = swordExplosionEmitter;
		particleDensity = 10;
		particleRadius = 0.2;

		faceViewer     = true;
		explosionScale = "1 1 1";

		shakeCamera = true;
		camShakeFreq = "10.0 11.0 10.0";
		camShakeAmp = "0.5 0.5 0.5";
		camShakeDuration = 0.25;
		camShakeRadius = 5.0;

		lightStartRadius = 1.5;
		lightEndRadius = 0;
		lightStartColor = "00.0 0.2 0.6";
		lightEndColor = "0 0 0";
	};

	datablock ItemData(CMPickaxeItem : swordItem)
	{
		shapeFile = "Add-Ons/Gamemode_CityMod/CityMod/assets/shapes/Pickaxe.dts";
		uiName = "Pickaxe";
		doColorShift = true;
		colorShiftColor = "0.471 0.471 0.471 1.000";

		image = CMPickaxeImage;
		canDrop = true;
		iconName = "Add-Ons/Gamemode_CityMod/CityMod/assets/icons/Pickaxe";
	};

	datablock ProjectileData(CMPickaxeProjectile)
	{
		directDamage        = 0;
		directDamageType  = $DamageType::CMPickaxe;
		radiusDamageType  = $DamageType::CMPickaxe;
		explosion           = CMPickaxeExplosion;

		muzzleVelocity      = 50;
		velInheritFactor    = 1;

		armingDelay         = 0;
		lifetime            = 100;
		fadeDelay           = 70;
		bounceElasticity    = 0;
		bounceFriction      = 0;
		isBallistic         = false;
		gravityMod = 0.0;

		hasLight    = false;
		lightRadius = 3.0;
		lightColor  = "0 0 0.5";

		uiName = "";
	};

	datablock ShapeBaseImageData(CMPickaxeImage)
	{
		shapeFile = CMPickaxeItem.shapeFile;
		emap = true;

		mountPoint = 0;
		offset = "0 0 0";

		correctMuzzleVector = false;

		className = "WeaponImage";

		item = CMPickaxeItem;
		ammo = " ";
		projectile = CMPickaxeProjectile;
		projectileType = Projectile;

		melee = true;
		doRetraction = false;

		armReady = true;

		doColorShift = true;
		colorShiftColor = "0.471 0.471 0.471 1.000";

		stateName[0]                     = "Activate";
		stateTimeoutValue[0]             = 0.5;
		stateTransitionOnTimeout[0]      = "Ready";
		stateSound[0]                    = weaponSwitchSound;

		stateName[1]                     = "Ready";
		stateTransitionOnTriggerDown[1]  = "PreFire";
		stateAllowImageChange[1]         = true;

		stateName[2]			= "PreFire";
		stateScript[2]                  = "onPreFire";
		stateAllowImageChange[2]        = false;
		stateTimeoutValue[2]            = 0.2;
		stateTransitionOnTimeout[2]     = "Fire";

		stateName[3]                    = "Fire";
		stateTransitionOnTimeout[3]     = "CheckFire";
		stateTimeoutValue[3]            = 0.5;
		stateFire[3]                    = true;
		stateAllowImageChange[3]        = false;
		stateSequence[3]                = "Fire";
		stateScript[3]                  = "onFire";
		stateWaitForTimeout[3]		= true;

		stateName[4]			= "CheckFire";
		stateTransitionOnTriggerUp[4]	= "StopFire";
		stateTransitionOnTriggerDown[4]	= "PreFire";
		
		stateName[5]                    = "StopFire";
		stateTransitionOnTimeout[5]     = "Ready";
		stateTimeoutValue[5]            = 0.1;
		stateAllowImageChange[5]        = false;
		stateWaitForTimeout[5]		= true;
		stateSequence[5]                = "StopFire";
		stateScript[5]                  = "onStopFire";
	};
}

// ============================================================
// Section 2 - Functions
// ============================================================
function CMPickaxeImage::onPreFire(%this, %obj, %slot)
{
	%obj.playthread(2, shiftAway);
}

function CMPickaxeImage::onFire(%this, %obj, %slot)
{
	%obj.playthread(2, shiftTo);

	parent::onFire(%this, %obj, %slot);
}

function CMPickaxeImage::onStopFire(%this, %obj, %slot)
{	
	//%client = %obj.client;
	//%client.setControlObject(%client.player);
	hammerImage.onStopFire(%obj, %slot);
}
	
function CMPickaxeImage::onHitObject(%this, %obj, %slot, %col, %pos, %normal)
{	
	serverPlay3D(hammerHitSound, %pos);

	if(isObject(%obj.client) && isObject(%col) && %col.getDataBlock().isStone)
		%col.mineStone(%obj.client, 1);
}