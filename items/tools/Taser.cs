// ============================================================
// Project           -     CityMod
// Description       -     Taser Item
// ============================================================
// Sections
//     1: Datablocks
//     2: Functions
// ============================================================

$defaultShapeNameDistance = 600;

// ============================================================
// Section 1 - Datablocks
// ============================================================

if(!isObject(CMTaserItem))
{
	datablock AudioProfile(CMTaserFireSound)
	{
		filename	 = "Add-Ons/Gamemode_CityMod/CityMod/assets/sounds/taserFire.wav";
		description = AudioClosest3d;
		preload = true;
	};

	datablock AudioProfile(CMTaserShockSound)
	{
		filename	 = "Add-Ons/Gamemode_CityMod/CityMod/assets/sounds/taserShock.wav";
		description = AudioClosest3d;
		preload = true;
	};

	datablock ParticleData(CMTaserParticle)
	{
		dragCoefficient			= 8;
		gravityCoefficient		= 0;
		inheritedVelFactor		= 0.2;
		constantAcceleration	= 0.0;
		lifetimeMS			  	= 500;
		lifetimeVarianceMS		= 100;
		textureName			 	= "Add-ons/Projectile_Radio_wave/bolt";
		spinSpeed				= 3380.0;
		spinRandomMin			= -50.0;
		spinRandomMax			= 50.0;
		colors[0]	 	= "1 1 0.0 0.6";
		colors[1]	 	= "1 1 1 0";
		sizes[0]		= 0.51;
		sizes[1]		= 0.26;
	
		useInvAlpha = true;
	};

	datablock ParticleEmitterData(CMTaserEmitter)
	{
		ejectionPeriodMS	= 1;
		periodVarianceMS	= 0;
		ejectionVelocity	= 5;
		velocityVariance	= 0.6;
		ejectionOffset		= 0.8;
		thetaMin			= 0;
		thetaMax			= 0;
		phiReferenceVel		= 0;
		phiVariance			= 30;
		overrideAdvance		= false;
		particles			= "CMTaserParticle";
	};

	datablock ProjectileData(CMTaserProjectile)
	{
		directDamage		  = 0;
		directDamageType	 = $DamageType::Gun;
		radiusDamageType	 = $DamageType::Gun;
	
		brickExplosionRadius = 0;
		brickExplosionImpact = true;
		brickExplosionForce  = 10;
		brickExplosionMaxVolume = 1;
		brickExplosionMaxVolumeFloating = 2;
	
		impactImpulse		= 0;
		verticalImpulse		= 0;
		explosion			= radioWaveExplosion;
		particleEmitter		= CMTaserEmitter;
	
		muzzleVelocity		= 120;
		velInheritFactor	= 1;
	
		armingDelay			= 00;
		lifetime			= 100;
		fadeDelay			= 800;
		bounceElasticity	= 0.5;
		bounceFriction		= 0.20;
		isBallistic			= false;
		gravityMod 			= 0.0;
	
		hasLight	 = true;
		lightRadius = 1.0;
		lightColor  = "1.0 1.0 0.5";
	
		uiName = "Taser Bolt";
	};

	datablock ItemData(CMTaserItem)
	{
		category = "Weapon";
		className = "Weapon";
		
		shapeFile = "Add-Ons/Gamemode_CityMod/CityMod/assets/shapes/Taser.dts";
		rotate = false;
		mass = 1;
		density = 0.2;
		elasticity = 0.2;
		friction = 0.6;
		emap = true;
	
		uiName = "Taser";
		iconName = "Add-Ons/Gamemode_CityMod/CityMod/assets/icons/Taser";
		doColorShift = false;
		colorShiftColor = "0.25 0.25 0.25 1.000";
		
		image = CMTaserImage;
		canDrop = true;
	};

	datablock ShapeBaseImageData(CMTaserImage)
	{
		shapeFile = "Add-Ons/Gamemode_CityMod/CityMod/assets/shapes/Taser.dts";
		emap = true;
		
		mountPoint = 0;
		offset = "0 0 0";
		eyeOffset = 0;
		rotation = eulerToMatrix("0 0 0");
		
		correctMuzzleVector = true;
		
		className = "WeaponImage";
		
		item = BowItem;
		ammo = " ";
		projectile			= CMTaserProjectile;
		projectileType		= Projectile;
	
		shellExitDir		= "1.0 -1.3 1.0";
		shellExitOffset		= "0 0 0";
		shellExitVariance	= 0.0;	
		shellVelocity		= 15.0;
		
		melee = false;
		armReady = true;
	
		doColorShift		= false;
		colorShiftColor 	= CMTaserItem.colorShiftColor;
		
		stateName[0]					= "Activate";
		stateTimeoutValue[0]			= 0.10;
		stateTransitionOnTimeout[0]		= "Ready";
		stateSound[0]					= weaponSwitchSound;
	
		stateName[1]					= "Ready";
		stateTransitionOnTriggerDown[1] = "Fire";
		stateAllowImageChange[1]		= true;
		stateSequence[1]				= "Ready";
	
		stateName[2]					= "Fire";
		stateTransitionOnTimeout[2]	 	= "Recharge";
		stateTimeoutValue[2]			= 0.10;
		stateFire[2]					= true;
		stateAllowImageChange[2]		= false;
		stateSequence[2]				= "Fire";
		stateScript[2]					= "onFire";
		stateWaitForTimeout[2]			= true;
		stateEmitter[2]					= CMTaserEmitter;
		stateEmitterTime[2]				= 0.05;
		stateEmitterNode[2]				= "muzzleNode";
		stateSound[2]					= CMTaserFireSound;
		stateEjectShell[2]				= true;
	
		stateName[3]					= "Recharge";
		stateEmitterTime[3]				= 0.50;
		//stateEmitter[3]					= taserInduceEmitter;
		stateEmitterNode[3]				= "muzzleNode";
		stateTimeoutValue[3]			= 0.50;
		stateTransitionOnTimeout[3]	 	= "Reload";
	
		stateName[4]					= "Reload";
		stateSequence[4]				= "Reload";
		stateTransitionOnTriggerUp[4]	= "Ready";
		stateSequence[4]				= "Ready";
	};
}

if(!isObject(PlayerCMTased))
{
	datablock PlayerData(PlayerCMTased : PlayerCM)
	{
		uiName = "";
		firstPersonOnly = 1;

		maxItems = 0;
		maxTools = 0;
		maxWeapons = 0;

		maxForwardSpeed = "0";
		maxBackwardSpeed = "0";
		maxSideSpeed = "0";
		maxUnderwaterForwardSpeed = "0";
		maxUnderwaterBackwardSpeed = "0";
		maxUnderwaterSideSpeed = "0";
		maxForwardCrouchSpeed = "0";
		maxBackwardCrouchSpeed = "0";
		maxSideCrouchSpeed = "0";
	};
}

// ============================================================
// Section 2 - Functions
// ============================================================

function CMTaserProjectile::onCollision(%this, %obj, %col, %fade, %pos, %normal)
{
	if((%col.getType() & $TypeMasks::PlayerObjectType) && isObject(%col.client) && %col.isPlayer)
	{
		%col.taserTumble();
	}
}

function Player::taserTumble(%this)
{
	if(!isObject(%this) || isObject(%this.tasedBot))
		return false;

	if(isObject(%this.cuffedBot))
		%this.cuffedBot.delete();

	%bot = new AIPlayer()
	{
		dataBlock = %this.getDataBlock();
		player = %this;
		isTasedBot = true;
	};

	MissionCleanup.add(%bot);

	%vehicle = %bot.corpseVehicle = new WheeledVehicle()
	{
		dataBlock = CorpseVehicle;

		bot = %bot;
	};

	MissionCleanup.add(%vehicle);

	%pos = vectorAdd(%this.position, "0 0 0.15");
	%rot = getWords(%this.getTransform(), 3, 6);

	%vehicle.setTransform(%pos SPC %rot);
	%vehicle.mountObject(%bot, 0);

	%ang = vectorSub(%this.getHackPosition(), %this.lastDamagePos);
	%ang = vectorCross("0 0 1", vectorScale(vectorNormalize(setWord(%ang, 2, 0)), 4));

	%vehicle.setVelocity(vectorScale(%this.getVelocity(), 1.5));
	%vehicle.setAngularVelocity(%ang);

	%client = %this.client;
	%client.player = %bot;

	%client.applyBodyParts();
	%client.applyBodyColors();

	%client.player = %this;

	%this.tasedBot = %bot;
	%this.setDataBlock(PlayerCMTased);
	%this.hideNode("ALL");
	%this.setShapeNameDistance(0);
	%this.canDismount = false;
	%bot.mountObject(%this, 8);

	if(isObject(%this.getMountedImage(0)))
		%bot.mountImage(%this.getMountedImage(0).getName(), 0);

	if(%this.getMountedImage(0) == CMCuffsImage.getID())
		%bot.playThread(0, "armReadyBoth");

	%this.unMountImage(0);
	%this.taserTick = %this.schedule(3000, "unTaserTumble");

	serverPlay3D(CMTaserShockSound, %this.getHackPosition());
}

function Player::unTaserTumble(%this)
{
	if(!isObject(%this))
		return;

	talk(%this.tasedBot.getMountedImage(0));
	cancel(%this.taserTick);
	%this.taserTick = "";

	%this.unHideNode("ALL");
	%this.client.applyBodyParts();
	%this.client.applyBodyColors();
	%this.setShapeNameDistance($defaultShapeNameDistance);
	%this.canDismount = true;

	%this.setDataBlock(PlayerCM);
	%this.tasedBot.unMountObject(%this);
	%this.setTransform(%this.tasedBot.getTransform());
	%this.mountImage(%this.tasedBot.getMountedImage(0), 0);

	if(%this.tasedBot.getMountedImage(0) == CMCuffsImage.getID())
	{
		%this.setDataBlock(PlayerCMCuffed);
		%this.playThread(0, "armReadyBoth");
	}

	%this.tasedBot.corpseVehicle.delete();
	%this.tasedBot.delete();
}

package CityMod_Taser
{
	function Player::unMount(%this)
	{
		if(!isObject(%this) || %this.getDatablock().getID() != PlayerCMTased.getID())
			parent::unMount(%this);
	}
};
if(isPackage(CityMod_Taser))
	deactivatePackage(CityMod_Taser);
activatePackage(CityMod_Taser);