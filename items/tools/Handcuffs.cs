// ============================================================
// Project           -     CityMod
// Description       -     Handcuff Item
// ============================================================
// Sections
//     1: Datablocks
//     2: Functions
// ============================================================

// ============================================================
// Section 1 - Datablocks
// ============================================================

if(!isObject(CMHandCuffItem))
{
	datablock ProjectileData(CMHandCuffProjectile)
	{
		projectileShapeName = "base/data/shapes/empty.dts";
	};

	datablock ItemData(CMHandCuffItem)
	{
		category = "Weapon";
		className = "Weapon";

		shapeFile = "Add-Ons/Gamemode_CityMod/CityMod/assets/shapes/Handcuffs_item.dts";
		mass = 1;
		density = 0.2;
		elasticity = 0.2;
		friction = 0.6;
		emap = true;

		uiName = "Hand Cuffs";
		iconName = "Add-Ons/Gamemode_CityMod/CityMod/assets/icons/Handcuffs";
		doColorShift = true;
		colorShiftColor = "0.54 0.53 0.53 1";

		image = CMHandCuffImage;
		canDrop = true;
	};

	datablock ShapeBaseImageData(CMHandCuffImage : WrenchImage)
	{
		shapeFile = "Add-Ons/Gamemode_CityMod/CityMod/assets/shapes/Handcuffs_holding.dts";
	   	emap = true;

	   	mountPoint = 0;
	   	correctMuzzleVector = false;

		//Yndaaa's Cuffs
		//rotation = eulerToMatrix("10 -10 -25");
		//offset = "0.0 -0.025 0.20";
		//eyeOffset = "0.275 1 -1.15";

		//Jack's Cuffs
		offset = "0.04 0.0 0.05";
		eyeOffset = "0.0 0.0 0.0";
		rotation = eulerToMatrix("0 0 1");
		
	   	className = "WeaponImage";

	   	item = CMHandCuffItem;
	   	ammo = " ";
	   	projectile = CMHandCuffProjectile;
	   	projectileType = Projectile;

	   	melee = true;
	   	doRetraction = false;
	  	armReady = true;

	   	doColorShift = true;
	   	colorShiftColor = CMHandCuffItem.colorShiftColor;

		showBricks = 0;
	};

	datablock ShapeBaseImageData(CMCuffsImage)
	{
		shapeFile = "Add-Ons/Gamemode_CityMod/CityMod/assets/shapes/Handcuffs.dts";
		emap = true;

		mountPoint = 0;

		//Yndaaa's Cuffs
		//offset = "-0.525 -0.150 0.105";
		//eyeOffset = "-0.525 -0.150 0.105";
		
		//Jack's Cuffs
		offset = "-0.03 -0.03 0.03";
		eyeOffset = "0.0 0.0 0.0";
		rotation = eulerToMatrix("5 0 0");

		className = "WeaponImage";
		item = "";
		armReady = true;

		doColorShift = true;
		colorShiftColor = CMHandCuffItem.colorShiftColor;
	};
}

if(!isObject(PlayerCMCuffed))
{
	datablock PlayerData(PlayerCMCuffed : PlayerCM)
	{
		uiName = "";
		
		maxItems = 0;
		maxTools = 0;
		maxWeapons = 0;

		maxForwardSpeed = "7";
		maxBackwardSpeed = "3";
		maxSideSpeed = "3";
		maxUnderwaterForwardSpeed = "4.2";
		maxUnderwaterBackwardSpeed = "3.9";
		maxUnderwaterSideSpeed = "3.9";
		maxForwardCrouchSpeed = "1.5";
		maxBackwardCrouchSpeed = "1";
		maxSideCrouchSpeed = "1";
	};

	datablock PlayerData(PlayerCMCuffedBot : PlayerCMCuffed){ maxForwardSpeed = "15"; maxSideSpeed = "15"; maxBackwardSpeed = "15"; };
}

// ============================================================
// Section 2 - Functions
// ============================================================

function CMHandCuffImage::onFire(%this, %obj, %slot)
{
	Parent::onFire(%this,%obj,%slot);

	%start = %obj.getEyePoint();
	%targets = $TypeMasks::FxBrickObjectType | 
					$TypeMasks::FxBrickAlwaysObjectType | 
					$TypeMasks::PlayerObjectType | 
					$TypeMasks::StaticObjectType | 
					$TypeMasks::TerrainObjectType | 
					$TypeMasks::VehicleObjectType;
	%vec = %obj.getEyeVector();
	%end = vectorAdd(%start, vectorScale(%vec, 4));
	%ray = containerRaycast(%start, %end, %targets, %obj);
	%col = firstWord(%ray);

	if(!isObject(%col))
		return;

	if(isObject(%col.cuffedBot))
	{
		commandToClient(%obj.client, 'centerPrint', "<font:Impact:21><color:555555>" @ %col.client.name SPC "is currently following you." NL
		"Right-Click him to make him stop following you before you can un-cuff him.", 4);
		return;
	}

	if((%col.getType() & $TypeMasks::PlayerObjectType) && %col.isPlayer)
	{
		if((%col.getDatablock().getID() != PlayerCMCuffed.getID()) || (isObject(%col.tasedBot) && (%col.tasedBot.getMountedImage(0) != CMCuffsImage.getID())))
		{
			if(strLen(CMPlayerData.data[%col.client.bl_id].Value["Charges"]))
			{
				if((%col.getDamageLevel() > 50) || isObject(%col.tasedBot))
				{
					if(%col.taserTick !$= "")
						%col.unTaserTumble();

					%col.setDataBlock(PlayerCMCuffed);
					%col.setShapeNameDistance($defaultShapeNameDistance);
					%col.unMountImage(0);
					%col.clearTools();
					%col.dismount();
					%col.playThread(0, "armReadyBoth");
					%col.mountImage(CMCuffsImage, 0);

					%col.cufferPlayer = %obj;

					serverPlay3D(wrenchHitSound, %col.getPosition());
					servercmdSit(%col.client);
					commandtoClient(%col.client, 'setBuildingDisabled', 1);
					commandToClient(%col.client, 'centerPrint', "<font:Impact:20><color:555555>You have been handcuffed", 1);
				}
				else
					commandToClient(%obj.client, 'centerPrint', "<font:Impact:24><color:ffffff>" @ %col.client.name SPC "is not weak enough to be handcuffed!", 2);
			}
			else
				commandToClient(%obj.client, 'centerPrint', "<font:Impact:24><color:ffffff>" @ %col.client.name SPC "is not wanted!", 2);
		}
		else if((%col.getDatablock().getID() == PlayerCMCuffed.getID()) || (isObject(%col.tasedBot) && (%col.tasedBot.getMountedImage(0) == CMCuffsImage.getID())))
		{
			if(%col.taserTick !$= "")
				%col.unTaserTumble();

			%col.unHandCuff();
			serverPlay3D(wrenchMissSound, %col.getPosition());
			commandToClient(%col.client, 'centerPrint', "<font:Impact:20><color:555555>You have been unhandcuffed", 1);
		}
	}
}

function Player::unHandCuff(%this)
{
	%this.unMountImage(0);
	%this.setDatablock(PlayerCM);
	%this.unhideNode("lhand");
	%this.playThread(0, "root");
	%this.unhideNode("rhand");

	%this.cufferPlayer = "";
	commandtoClient(%this.client, 'setBuildingDisabled', 0);
}

function Player::unCuffedFollow(%this)
{
	%this.unHideNode("ALL");
	%this.client.applyBodyParts();
	%this.client.applyBodyColors();
	%this.cuffedBot.unMountObject(%target.player);
	%this.canDismount = true;

	%this.mountImage(CMCuffsImage, 0);
	%this.setTransform(%target.cuffedBot.getTransform());

	%this.cuffedBot.cuffedRope.delete();
	%this.cuffedBot.delete();

	%this.schedule(100, "playThread", 0, "armReadyBoth");
}

function AIPlayer::cuffedFollowLoop(%obj, %target)
{
	if(!%obj.isCuffedBot)
		return;

	cancel(%obj.followLoop);

	if(isObject(%target) && (%target.getState() !$= "Dead") && (vectorDist(%target.getPosition(), %obj.getPosition()) <= 8))
	{
		%obj.setMoveDestination(%target.getPosition());

		%botPos = vectorAdd(%obj.getSlotTransform(0), vectorCross(%obj.getForwardVector(), "0 0 -0.5"));
		%targetPos = %target.getSlotTransform(0);

		%vec1 = vectorNormalize(vectorSub(%botPos, %targetPos));
		%vec2 = vectorNormalize(vectorSub(%targetPos, %botPos));

		%xyz = vectorNormalize(vectorCross("1 0 0", %vec1));
		%pow = mACos(vectorDot("1 0 0", %vec1)) * -1;

		if(!isObject(%obj.cuffedRope))
		{
			%rope = new StaticShape()
			{
				datablock = ropeShape;
				scale = %ropeScale;

				position = %ropePosition;
				rotation = %ropeRotation;

				botPos = %botPos;
				targetPos = %targetPos;
			};

			if(!isObject(RopeGroup))
				MissionCleanup.add(new SimGroup(RopeGroup));

			RopeGroup.add(%rope);
			%obj.cuffedRope = %rope;

			%rope.setNodeColor("ALL", "0.3 0.3 0.3 1.0");
		}

		%obj.cuffedRope.setScale(vectorDist(%botPos, %targetPos) SPC 0.1 SPC 0.1);

		%ropePosition = vectorScale(vectorAdd(%botPos, %targetPos), 0.5);
		%ropeRotation = %xyz SPC %pow;

		%obj.cuffedRope.setTransform(%ropePosition SPC %ropeRotation);
		%obj.cuffedRope.botPos = %botPos;
		%obj.cuffedRope.targetPos = %targetPos;

		%obj.schedule(16, "cuffedFollowLoop", %target);
	}
	else
		%obj.player.unCuffedFollow();
}

package CityMod_Handcuffs
{
	function serverCmdSuicide(%client)
	{
		if(!isObject(%client.player) || (%client.player.getDatablock().getID() != PlayerCMCuffed.getID() && %client.player.getDatablock().getID() != PlayerCMTased.getID()))
			parent::serverCmdSuicide(%client);
	}

	function serverCmdUseInventory(%client, %slot)
	{
		if(!isObject(%client.player) || %client.player.getDatablock().getID() != PlayerCMCuffed.getID())
			parent::serverCmdUseInventory(%client, %slot);
	}

	function serverCmdWand(%client)
	{
		if(!isObject(%client.player) || %client.player.getDatablock().getID() != PlayerCMCuffed.getID())
			parent::serverCmdWand(%client);
	}

	function serverCmdUseFXCan(%client, %id)
	{
		if(!isObject(%client.player) || %client.player.getDatablock().getID() != PlayerCMCuffed.getID())
			parent::serverCmdUseFXCan(%client, %id);
	}

	function serverCmdUseSprayCan(%client, %id)
	{
		if(!isObject(%client.player) || %client.player.getDatablock().getID() != PlayerCMCuffed.getID())
			parent::serverCmdUseSprayCan(%client, %id);
	}

	function serverCmdUseTool(%client, %slot)
	{
		if(!isObject(%client.player) || %client.player.getDatablock().getID() != PlayerCMCuffed.getID())
			parent::serverCmdUseTool(%client, %slot);
	}

	function serverCmdUnUseTool(%client)
	{
		if(!isObject(%client.player) || %client.player.getDatablock().getID() != PlayerCMCuffed.getID())
			parent::serverCmdUnUseTool(%client);
	}

	function serverCmdShiftBrick(%client, %x, %y, %z)
	{
		if(!isObject(%client.player) || %client.player.getDatablock().getID() != PlayerCMCuffed.getID())
			parent::serverCmdShiftBrick(%client, %x, %y, %z);
	}

	function serverCmdPlantBrick(%client)
	{
		if(!isObject(%client.player) || %client.player.getDatablock().getID() != PlayerCMCuffed.getID())
			parent::serverCmdPlantBrick(%client);
	}

	function serverCmdSuperShiftBrick(%client, %x, %y, %z)
	{
		if(!isObject(%client.player) || %client.player.getDatablock().getID() != PlayerCMCuffed.getID())
			parent::serverCmdSuperShiftBrick(%client, %x, %y, %z);
	}

	function serverCmdRotateBrick(%client, %rotation)
	{
		if(!isObject(%client.player) || %client.player.getDatablock().getID() != PlayerCMCuffed.getID())
			parent::serverCmdRotateBrick(%client, %rotation);
	}

	function Player::ActivateStuff(%this)
	{
		if(!isObject(%this) || %this.getDatablock().getID() != PlayerCMCuffed.getID())
			parent::ActivateStuff(%this);
	}

	function Player::unMount(%this)
	{
		if(!isObject(%this) || %this.getDatablock().getID() != PlayerCMCuffed.getID())
			parent::unMount(%this);
	}

	function GameConnection::onDeath(%client, %sourceObject, %sourceClient, %damageType, %damageArea)
	{
		if(isObject(%client.player.cuffedBot))
			%client.player.cuffedBot.delete();

		parent::onDeath(%client, %sourceObject, %sourceClient, %damageType, %damageArea);
	}

	function Armor::onTrigger(%this, %player, %slot, %val)
	{
		%return = parent::onTrigger(%this, %player, %slot, %val);

		if(%slot != 4 || %val != 1 || %player.currTool $= "" || !isObject(%player.tool[%player.currTool]) || %player.tool[%player.currTool].getName() !$= "CMHandCuffItem")
			return %return;

		%vector = vectorAdd(vectorScale(vectorNormalize(%player.getEyeVector()), 2.5), %player.getEyePoint());
		%target = containerRayCast(%player.getEyePoint(), %vector, $TypeMasks::PlayerObjectType, %player);
		%target = firstWord(%target);

		if(!isObject(%target) || %target.isCuffedBot || %target.isTasedBot || %target.isCorpse || %target.getDatablock().getID() != PlayerCMCuffed.getID())
			return %return;

		if(isObject(%target.cuffedBot))
		{
			%target.unCuffedFollow();
		}
		else if(!isObject(%target.cuffedBot))
		{
			%bot = new AIPlayer()
			{
				datablock = PlayerCMCuffedBot;
				position = %target.getPosition();
				isCuffedBot = true;
				player = %target;
				master = %player.client;
			};

			%target.cuffedBot = %bot;
			MissionCleanup.add(%bot);

			%target2 = %target;
			%targetclient = %target.client;
			%target = %bot;
			%targetclient.player = %target;

			%targetclient.applyBodyParts();
			%targetclient.applyBodyColors();

			%target = %target2;
			%targetclient.player = %target;

			%bot.cuffedFollowLoop(%player);

			%bot.unMountImage(0);
			%bot.playThread(0, "armReadyBoth");
			%bot.mountImage(CMCuffsImage, 0);

			%target.hideNode("ALL");
			%target.unMountImage(0);
			%target.canDismount = false;
			%bot.mountObject(%target, 8);
			%target.playThread(0, "root");
		}

		return %return;
	}
};
if(isPackage(CityMod_Handcuffs))
	deactivatePackage(CityMod_Handcuffs);
activatePackage(CityMod_Handcuffs);