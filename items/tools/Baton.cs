// ============================================================
// Project           -     CityMod
// Description       -     Baton Item
// ============================================================
// Sections
//     1: Datablocks
//     2: Functions
// ============================================================

// ============================================================
// Section 1 - Datablocks
// ============================================================

if(!isObject(CMBatonItem))
{
	datablock AudioProfile(CMBatonHitSound)
	{
		filename    = "Add-Ons/Gamemode_CityMod/CityMod/assets/sounds/batonHit.wav";
		description = AudioClosest3d;
		preload = true;
	};

	datablock AudioProfile(CMBatonEquipSound)
	{
		filename    = "Add-Ons/Gamemode_CityMod/CityMod/assets/sounds/batonEquip.wav";
		description = AudioClosest3d;
		preload = true;
	};

	//projectile
	AddDamageType("CMBaton", 'Batond %1', '%2 Batond %1', 0.5, 1);
	 
	datablock ExplosionData(CMBatonExplosion : SwordExplosion)
	{
		soundProfile = CMBatonHitSound;
	};
	 
	datablock ProjectileData(CMBatonProjectile)
	{
		directDamage        = 0;
		directDamageType  	= $DamageType::CMBaton;
		radiusDamageType  	= $DamageType::CMBaton;
		explosion           = CMBatonExplosion;
	 
		muzzleVelocity      = 50;
		velInheritFactor    = 1;
	 
		armingDelay         = 0;
		lifetime            = 100;
		fadeDelay           = 70;
		bounceElasticity    = 0;
		bounceFriction      = 0;
		isBallistic         = false;
		gravityMod = 0.0;
	 
		hasLight    = false;
		lightRadius = 3.0;
		lightColor  = "0 0 0.5";
	 
		uiName = "Baton Hit";
	};

	datablock ItemData(CMBatonItem)
	{
		category = "Weapon";
		className = "Weapon";
		
	 	uiName = "Baton";

	 	iconName = "Add-Ons/Gamemode_CityMod/CityMod/assets/icons/Baton";
		shapeFile = "Add-Ons/Gamemode_CityMod/CityMod/assets/shapes/Baton.dts";

		mass = 1;
		density = 0.2;
		elasticity = 0.2;
		friction = 0.6;
		emap = true;
	 
		doColorShift = true;
		colorShiftColor = "0.471 0 0 1.000";

		image = CMBatonImage;
		canDrop = true;
	};
	 
	datablock ShapeBaseImageData(CMBatonImage)
	{
		className = "WeaponImage";
		shapeFile = "Add-Ons/Gamemode_CityMod/CityMod/assets/shapes/Baton.dts";

		emap = true;
		mountPoint = 0;
		offset = "0 0 0";
		correctMuzzleVector = false;
	 
		eyeOffset = 0; //"0.7 1.2 -0.25";
	 
		item = CMBatonItem;
		ammo = " ";
		projectile = CMBatonProjectile;
		projectileType = Projectile;
	 
		raycastWeaponRange = 5;
		raycastWeaponTargets =
							 $TypeMasks::PlayerObjectType |    //AI/Players
							 $TypeMasks::StaticObjectType |    //Static Shapes
							 $TypeMasks::TerrainObjectType |    //Terrain
							 $TypeMasks::VehicleObjectType |    //Terrain
							 $TypeMasks::FXBrickObjectType;    //Bricks
		raycastExplosionProjectile = CMBatonProjectile;
		raycastDirectDamage = 10;
		raycastDirectDamageType = $DamageType::CMBaton;
	 
		melee = true;
		doRetraction = false;
		armReady = true;
	 
		doColorShift = true;
		colorShiftColor = CMBatonItem.colorShiftColor;

		stateName[0]                     = "Activate";
		stateTimeoutValue[0]             = 0.1;
		stateTransitionOnTimeout[0]      = "Ready";
		stateSound[0]                    = CMBatonEquipSound;
	 
		stateName[1]                     = "Ready";
		stateTransitionOnTriggerDown[1]  = "AmmoCheck";
		stateAllowImageChange[1]         = true;
	 
		stateName[2]                     = "AmmoCheck";
		stateTransitionOnTimeout[2]      = "PreFire";
		stateAllowImageChange[2]         = true;
	 
		stateName[3]                     = "PreFire";
		stateScript[3]                   = "onPreFire";
		stateAllowImageChange[3]         = false;
		stateTimeoutValue[3]             = 0.2;
		stateTransitionOnTimeout[3]      = "Fire";
	 
		stateName[4]                     = "Fire";
		stateTransitionOnTimeout[4]      = "CheckFire";
		stateTimeoutValue[4]             = 0.3;
		stateFire[4]                     = true;
		stateAllowImageChange[4]         = false;
		stateSequence[4]                 = "Fire";
		//stateSound[4]                    = CMBatonSwingSound;
		stateScript[4]                   = "onFire";
		stateWaitForTimeout[4]		       = true;
	 
		stateName[5]                     = "CheckFire";
		stateTransitionOnTriggerUp[5]    = "StopFire";
		stateTransitionOnTriggerDown[5]  = "Ready";
	 
		stateName[6]                     = "StopFire";
		stateTransitionOnTimeout[6]      = "Ready";
		stateTimeoutValue[6]             = 0.1;
		stateAllowImageChange[6]         = false;
		stateWaitForTimeout[6]		       = true;
		stateSequence[6]                 = "StopFire";
		stateScript[6]                   = "onStopFire";
	};
}

// ============================================================
// Section 2 - Functions
// ============================================================

function CMBatonImage::onPreFire(%this, %obj, %slot)
{
	%obj.playThread(2, shiftRight);
	%obj.playThread(3, shiftLeft);
}
	 
function CMBatonImage::onFire(%this, %obj, %slot)
{
	parent::onFire(%this, %obj, %slot);
	//serverPlay3d(CMBatonSwingSound, %obj.getHackPosition());
	%obj.playthread(2, shiftTo);
	%obj.playthread(3, shiftleft); 
}
	 
function CMBatonImage::onStopFire(%this, %obj, %slot)
{
	%obj.setImageAmmo(0, 1);
}
	 
function CMBatonImage::onHitObject( %this, %obj, %slot, %col, %pos, %normal, %shotVec, %crit )
{
	cancel(%obj.crowbarSchedule);
	%obj.setImageAmmo(0, 0);
	%obj.crowbarSchedule = %obj.schedule(600, setImageAmmo, 0, 1);
		
	if((%col.getClassName() $= "Player") && (%col.getType() & $TypeMasks::PlayerObjectType) && isObject(%col.client))
	{
		if((%col.getDamageLevel() > 50) && (%col.getDatablock().getID() != PlayerCMCuffed.getID()) && !%col.isCorpse)
		{
			commandToClient(%obj.client, 'centerPrint', "<font:Impact:24><color:ffffff>" @ %col.client.name SPC "is weak enough to be handcuffed!", 2);
			%noDamage = true;
		}
	}

	if(%noDamage){ %this.raycastDirectDamage = 0; }
	parent::onHitObject(%this, %obj, %slot, %col, %pos, %normal);
	if(%noDamage){ %this.raycastDirectDamage = 10; }
}