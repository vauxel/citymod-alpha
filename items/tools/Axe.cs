// ============================================================
// Project				-		CityMod
// Description		-		Axe Item
// ============================================================
// Sections
// 	 1: Datablocks
// 	 2: Functions
// ============================================================

// ============================================================
// Section 1 - Datablocks
// ============================================================
if(!isObject(CMAxeItem))
{
	datablock ItemData(CMAxeItem)
	{
		category		= "Weapon";
		className		= "Weapon";
		
		shapeFile		= "Add-Ons/Gamemode_CityMod/CityMod/assets/shapes/Axe.dts";
		mass			= 1;
		density 		= 0.2;
		elasticity		= 0.2;
		friction		= 0.6;
		emap			= true;
		
		uiName			= "Axe";
		iconName		= "Add-Ons/Gamemode_CityMod/CityMod/assets/icons/Axe";
		doColorShift	= false;
		
		image			= CMAxeImage;
		canDrop			= true;
	};
	
	datablock ShapeBaseImageData(CMAxeImage)
	{
	   raycastWeaponRange = 3;
	   raycastWeaponTargets = $TypeMasks::All;
	   raycastDirectDamage = 1;
	   raycastDirectDamageType = $DamageType::Hammer;
	   raycastExplosionProjectile = hammerProjectile;
	   raycastExplosionSound = CMS_AxeHit;
		
	   shapeFile = "Add-Ons/Gamemode_CityMod/CityMod/assets/shapes/Axe.dts";
	   emap = true;
	   mountPoint = 0;
	   offset = "0 0 0";
	   eyeOffset = "0 0 0";

	   correctMuzzleVector = true;

	   className = "WeaponImage";

	   item = CMAxeItem;
	   ammo = " ";
	   projectile = hammerProjectile;
	   projectileType = Projectile;

	   melee = true;
	   armReady = true;
	   
	   doColorShift = false;
	   colorShiftColor = "0.400 0.196 0 1.000";

		stateName[0]			= "Activate";
		stateTimeoutValue[0]		= 0.1;
		stateTransitionOnTimeout[0]	= "Ready";
		stateSequence[0]		= "ready";
		stateSound[0]					= weaponSwitchSound;

		stateName[1]			= "Ready";
		stateTransitionOnTriggerDown[1]	= "Charge";
		stateAllowImageChange[1]	= true;
		
		stateName[2]                    = "Charge";
		stateTransitionOnTimeout[2]	= "Armed";
		stateTimeoutValue[2]            = 0.7;
		stateWaitForTimeout[2]		= false;
		stateTransitionOnTriggerUp[2]	= "AbortCharge";
		stateScript[2]                  = "onCharge";
		stateAllowImageChange[2]        = false;
		
		stateName[3]			= "AbortCharge";
		stateTransitionOnTimeout[3]	= "Ready";
		stateTimeoutValue[3]		= 0.3;
		stateWaitForTimeout[3]		= true;
		stateScript[3]			= "onAbortCharge";
		stateAllowImageChange[3]	= false;

		stateName[4]			= "Armed";
		stateTransitionOnTriggerUp[4]	= "Fire";
		stateAllowImageChange[4]	= false;

		stateName[5]			= "Fire";
		stateTransitionOnTimeout[5]	= "Ready";
		stateTimeoutValue[5]		= 0.5;
		stateFire[5]			= true;
		stateSequence[5]		= "fire";
		stateScript[5]			= "onFire";
		stateWaitForTimeout[5]		= true;
		stateAllowImageChange[5]	= false;
		stateSound[5]				= CMS_AxeHit;
	};
}
//if(!isObject(CMAxeItem))
//{
//	datablock itemData(CMAxeItem : hammerItem)
//	{
//		category = "Weapon";
//		uiName = "Axe";
//		image = CMAxeImage;
//	};
//
//	datablock shapeBaseImageData(CMAxeImage : hammerImage)
//	{
//		raycastWeaponRange = 3;
//		raycastWeaponTargets = $TypeMasks::All;
//		raycastDirectDamage = 2;
//		raycastExplosionProjectile = hammerProjectile;
//		
//		item = CMAxeItem;
//		projectile = hammerProjectile;
//		showBricks = 0;
//	};
//}

// ============================================================
// Section 2 - Functions
// ============================================================
function CMAxeImage::onPreFire(%this, %obj, %slot)
{
	hammerImage.onPreFire(%obj, %slot);
}

function CMAxeImage::onStopFire(%this, %obj, %slot)
{	
	hammerImage.onStopFire(%obj, %slot);
}
	
function CMAxeImage::onHitObject(%this, %obj, %slot, %col, %pos, %normal)
{
	if(isObject(%obj.client) && %col.getDataBlock().isTree)
		%col.chopTree(%obj.client, 1);
	
	parent::onHitObject(%this, %obj, %slot, %col, %pos, %normal);
}