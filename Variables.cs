// ============================================================
// Project				-		CityMod
// Description			-		Preferences and Variables
// ============================================================
// Sections
// 	 1: Preferences
// ============================================================

// ============================================================
// Section 1 - Preferences
// ============================================================

//Core
$CM::WhiteList = true;
$CM::ClientVersion = "1.2";
$CM::Password = "c64WDkvQ";

$CM::WhiteList::BL_IDs = "8437 17245 10833 19273 7639 11239 8061 32843 29019 5012 27066 15312 2143 22749 3810 9789 31734";

$CM::Prefs::DebugMode = true;

$CM::Prefs::Core::CityStarterFunds = 100000;
$CM::Prefs::Core::TickSpeed = 2.5; //1 In-Game Day = 1 IRL Hours

//Misc
$CM::Prefs::Misc::MiningSpeed = 4;

//Building
$CM::Prefs::Building::MaxLotHeight = 256;
$CM::Prefs::Building::DefaultLotPrice = 200;

//Player
$CM::Prefs::Player::StartingCash = 20;

//Groups
$CM::Prefs::Group::MaxGroups = 2;
$CM::Prefs::Group::MaxEmployees = 25;
$CM::Prefs::Group::MaxSalary = 20000;
$CM::Prefs::Group::MaxRanks = 20;
$CM::Prefs::Group::MaxStockAmount = 50000;

//Defaults
$CM::Prefs::Default::FoodQuantities[1] = "Tiny";
$CM::Prefs::Default::FoodQuantities[2] = "Small";
$CM::Prefs::Default::FoodQuantities[3] = "Medium";
$CM::Prefs::Default::FoodQuantities[4] = "Large";
$CM::Prefs::Default::FoodQuantities[5] = "Oversized";
$CM::Prefs::Default::FoodQuantities[6] = "XXL";
$CM::Prefs::Default::MaxFoodQuantities = 6;

$CM::Prefs::Default::FoodPrice[$CM::Prefs::Default::FoodQuantities[1]] = 5;
$CM::Prefs::Default::FoodPrice[$CM::Prefs::Default::FoodQuantities[2]] = 10;
$CM::Prefs::Default::FoodPrice[$CM::Prefs::Default::FoodQuantities[3]] = 15;
$CM::Prefs::Default::FoodPrice[$CM::Prefs::Default::FoodQuantities[4]] = 20;
$CM::Prefs::Default::FoodPrice[$CM::Prefs::Default::FoodQuantities[5]] = 25;
$CM::Prefs::Default::FoodPrice[$CM::Prefs::Default::FoodQuantities[6]] = 30;

$CM::Prefs::Default::CrateQuantity = 20;
$CM::Prefs::Default::ClumpQuantity = 4;

$CM::Prefs::Default::MaxInventorySpace = 20;  //This Var * 6 Slots