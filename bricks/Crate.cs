// ============================================================
// Project				-		CityMod
// Description			-		Crate Datablock
// ============================================================
// Sections
// 	 1: fxDTSBrick
// 	 2: Functions
// ============================================================

// ============================================================
// Section 1 - fxDTSBrick
// ============================================================
datablock fxDTSBrickData(brickCMFoodCrateData)
{
	brickFile = "./BLBs/Crate.blb";
	
	category = "CityMod";
	subCategory = "Storage";
	
	CMBrick = true;
	CMBrickType = "CRATE";
	CMAdminBrick = false;

	staticColorID = 57;
	
	iconName = "Add-Ons/Gamemode_CityMod/CityMod/assets/icons/Crate";
	uiName = "Raw Food Crate";
	
	type = "Food";
};

datablock fxDTSBrickData(brickCMMetalCrateData)
{
	brickFile = "./BLBs/Crate.blb";
	
	category = "CityMod";
	subCategory = "Storage";
	
	CMBrick = true;
	CMBrickType = "CRATE";
	CMAdminBrick = false;
	
	staticColorID = 59;

	iconName = "Add-Ons/Gamemode_CityMod/CityMod/assets/icons/Crate";
	uiName = "Metals Crate";
	
	type = "Metal";
};

datablock fxDTSBrickData(brickCMCoalCrateData)
{
	brickFile = "./BLBs/Crate.blb";
	
	category = "CityMod";
	subCategory = "Storage";
	
	CMBrick = true;
	CMBrickType = "CRATE";
	CMAdminBrick = false;

	staticColorID = 60;
	
	iconName = "Add-Ons/Gamemode_CityMod/CityMod/assets/icons/Crate";
	uiName = "Coal Crate";
	
	type = "Coal";
};

datablock fxDTSBrickData(brickCMWoodCrateData)
{
	brickFile = "./BLBs/Crate.blb";
	
	category = "CityMod";
	subCategory = "Storage";
	
	CMBrick = true;
	CMBrickType = "CRATE";
	CMAdminBrick = false;

	staticColorID = 55;
	
	iconName = "Add-Ons/Gamemode_CityMod/CityMod/assets/icons/Crate";
	uiName = "Lumber Crate";
	
	type = "Wood";
};

datablock fxDTSBrickData(brickCMPlasticCrateData)
{
	brickFile = "./BLBs/Crate.blb";
	
	category = "CityMod";
	subCategory = "Storage";
	
	CMBrick = true;
	CMBrickType = "CRATE";
	CMAdminBrick = false;
	
	staticColorID = 58;

	iconName = "Add-Ons/Gamemode_CityMod/CityMod/assets/icons/Crate";
	uiName = "Plastic Crate";
	
	type = "Plastic";
};

// ============================================================
// Section 2 - Functions
// ============================================================
//package CMP_Crates
//{
//	//To-Do
//};
//if(isPackage(CMP_Crates))
//	deactivatePackage(CMP_Crates);
//activatePackage(CMP_Crates);