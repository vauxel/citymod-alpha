// ============================================================
// Project				-		CityMod
// Description		-		Stone Datablock
// ============================================================
// Sections
// 	 1: fxDTSBrick
// 	 2: Functions
// ============================================================

// ============================================================
// Section 1 - fxDTSBrick
// ============================================================
datablock fxDTSBrickData(brickCMStoneData)
{
	brickFile = "./BLBs/Stone.blb";
	
	category = "CityMod";
	subCategory = "Admin";
	
	CMBrick = true;
	CMBrickType = "RESOURCE";
	CMAdminBrick = true;
	
	iconName = "Add-Ons/Gamemode_CityMod/CityMod/assets/icons/Stone";
	uiName = "Stone";
	
	isStone = true;
	type = "Stone";
	health = 100;
};

// ============================================================
// Section 2 - Functions
// ============================================================
package CityMod_Stone
{
	function fxDTSBrick::mineStone(%this, %client, %level)
	{
		if(isObject(%this) && isObject(%client) && %this.getDataBlock().isStone)
		{	
			if(%this.type $= "")
			{
				if(getRandom(1,2) == 2)
					%this.type = "Metal";
				else
					%this.type = "Coal";
			}
				
			if(mFloor(%this.health) > 0)
			{
				%this.health -= $CM::Prefs::Misc::MiningSpeed;
				
				if(mFloor(%this.health) <= 0)
				{
					%this.health = 0;
					%amt = getRandom(1,3);
						
					if(%this.type !$= "Stone")
						createDroppable(%this.type, %this.getPosition(), getRandom(1,3), true);
					else
						%client.CMNotify("Mining", "The rock didn't contain any resources", "information");

					%this.setColliding(0);
					%this.setRendering(0);
					%this.setRayCasting(0);
					%this.schedule(getRandom(10000, 30000), "regenStone");
				}
				else
					commandToClient(%client, 'centerPrint', "<font:Impact:24><color:555555>Rock Health:<color:777777>" SPC %this.health @ "%", 1);
			}
			else
				commandToClient(%client, 'centerPrint', "<font:Impact:24><color:FF0000>This Rock is Empty!", 1);
		}
	}

	function fxDTSBrick::regenStone(%this)
	{
		if(isObject(%this))
		{
			%this.setColliding(1);
			%this.setRendering(1);
			%this.setRayCasting(1);
			
			switch(getRandom(1, 3))
			{
				case 1:
					%this.type = "Metal";
				case 2:
					%this.type = "Coal";
				case 3:
					%this.type = "Stone";
			}
			%this.health = 100;
		}
	}
};
if(isPackage(CityMod_Stone))
	deactivatePackage(CityMod_Stone);
activatePackage(CityMod_Stone);