// ============================================================
// Project				-		CityMod
// Description			-		Resources Code
// ============================================================
// Sections
//   1: fxDTSBrick
// 	 2: Functions
//	 3: Package
// ============================================================

//Format: "Description" TAB "AMT:REQ_MATERIAL" [SPC "AMT:REQ_MATERIAL"] [SPC "AMT:REQ_MATERIAL"] [SPC "AMT:REQ_MATERIAL"] TAB "AMT:PRODUCT_MATERIAL" TAB "SPEED"
//Example: $CM::Formulas["RefinedMetal"] = "Refine Metal\t1:METAL\t1:REFINEDMETAL\t6";
//NOTE: "SPEED" may ONLY be an even number.  Odd numbers won't work.

$CM::Formulas["Plastic"] = "Make Plastic\t1:METAL 1:WOOD\t1:PLASTIC\t5";

// ============================================================
// Section 1 - fxDTSBrick
// ============================================================

datablock fxDTSBrickData(brickCMRawMetal2x2 : brick2x2Data){ category = "Special"; subCategory = "Ore Clumps"; uiName = "Metal Clump"; };

datablock fxDTSBrickData(brickCMFactoryMachineData)
{
	brickFile = "./BLBs/Machine.blb";
	
	category = "CityMod";
	subCategory = "Machinery";
	
	CMBrick = true;
	CMBrickType = "INTERACTIVE";
	CMAdminBrick = false;
	
	iconName = "Add-Ons/Gamemode_CityMod/CityMod/assets/icons/Machine";
	uiName = "Machine";
	
	isMachine = true;
	machineType = "processor";

	orientationFix = 3;
	isMachineRunning = false;
	updateClients = "";
	machineProgress = 0;
};

datablock fxDTSBrickData(brickCMSmelterData)
{
	brickFile = "./BLBs/Smelter.blb";

	category = "Special";
	subCategory = "CityMod";

	CMBrick = true;
	CMBrickType = "INTERACTIVE";
	CMAdminBrick = false;

	iconName = "Add-Ons/Gamemode_CityMod/CityMod/assets/icons/Machine";
	uiName = "Smelter";

	isDoor = 1;
	isOpen = 0;

	closedCW = "brickCMSmelterData";
	openCW = "brickCMSmelterOpenData";
	
	closedCCW = "brickCMSmelterData";
	openCCW = "brickCMSmelterOpenData";

	isMachine = true;
	machineType = "smelter";
};

datablock fxDTSBrickData(brickCMSmelterOpenData : brickCMSmelterData)
{
	brickFile = "./BLBs/Smelter_Open.blb";

	category = "";
	subCategory = "";
	uiName = "Smelter Open";
	
	isOpen = 1;
};

// ============================================================
// Section 2 - Functions
// ============================================================

function listFormulaRequirements(%formula)
{
	if(%formula $= "")
		return;

	%string1 = getField(%formula, 0);

	if(strLen(getWord(getField(%formula, 1), 0)))
	{
		%Req1 = getWord(getField(%formula, 1), 0);
		%Req1 = nextToken(%Req1, "ReqAmt0", ":");
		%Req1 = nextToken(%Req1, "ReqType0", ":");
		%reqamt++;
	}

	if(strLen(getWord(getField(%formula, 1), 1)))
	{
		%Req2 = getWord(getField(%formula, 1), 1);
		%Req2 = nextToken(%Req2, "ReqAmt1", ":");
		%Req2 = nextToken(%Req2, "ReqType1", ":");
		%reqamt++;
	}

	if(strLen(getWord(getField(%formula, 1), 2)))
	{
		%Req3 = getWord(getField(%formula, 1), 2);
		%Req3 = nextToken(%Req3, "ReqAmt2", ":");
		%Req3 = nextToken(%Req3, "ReqType2", ":");
		%reqamt++;
	}

	%first = true;
	for(%i = 0; %i < %reqamt; %i++)
	{
		if(%first == true)
		{
			%string2 = %ReqAmt[%i] SPC %ReqType[%i];
			%first = false;
		}
		else
			%string2 = %string2 @ "," SPC %ReqAmt[%i] SPC %ReqType[%i];
	}

	%string = setField(%string, 0, %string1);
	%string = setField(%string, 1, %string2);
	return %string;
}

function gameConnection::hasFormulaMaterials(%client, %formula)
{
	if(%formula $= "")
		return false;

	%Req1 = getWord(getField(%formula, 1), 0);
	%Req1 = nextToken(%Req1, "ReqAmt0", ":");
	%Req1 = nextToken(%Req1, "ReqType0", ":");
	%reqamt = 1;

	if(getWord(getField(%formula, 1), 1) !$= "")
	{
		%Req2 = getWord(getField(%formula, 1), 1);
		%Req2 = nextToken(%Req2, "ReqAmt1", ":");
		%Req2 = nextToken(%Req2, "ReqType1", ":");
		%reqamt++;
	}

	if(getWord(getField(%formula, 1), 2) !$= "")
	{
		%Req3 = getWord(getField(%formula, 1), 2);
		%Req3 = nextToken(%Req3, "ReqAmt2", ":");
		%Req3 = nextToken(%Req3, "ReqType2", ":");
		%reqamt++;
	}

	if(getWord(getField(%formula, 1), 3) !$= "")
	{
		%Req4 = getWord(getField(%formula, 1), 3);
		%Req4 = nextToken(%Req4, "ReqAmt3", ":");
		%Req4 = nextToken(%Req4, "ReqType3", ":");
		%reqamt++;
	}

	for(%i = 0; %i < %reqamt; %i++)
	{
		if(%client.getInventory("all", %ReqType[%i]) < %ReqAmt[%i])
			return false;
	}

	return true;
}

function gameConnection::removeFormulaMaterials(%client, %formula)
{
	if(%formula $= "")
		return;

	%Req1 = getWord(getField(%formula, 1), 0);
	%Req1 = nextToken(%Req1, "ReqAmt0", ":");
	%Req1 = nextToken(%Req1, "ReqType0", ":");
	%reqamt = 1;

	if(getWord(getField(%formula, 1), 1) !$= "")
	{
		%Req2 = getWord(getField(%formula, 1), 1);
		%Req2 = nextToken(%Req2, "ReqAmt1", ":");
		%Req2 = nextToken(%Req2, "ReqType1", ":");
		%reqamt++;
	}

	if(getWord(getField(%formula, 1), 2) !$= "")
	{
		%Req3 = getWord(getField(%formula, 1), 2);
		%Req3 = nextToken(%Req3, "ReqAmt2", ":");
		%Req3 = nextToken(%Req3, "ReqType2", ":");
		%reqamt++;
	}

	if(getWord(getField(%formula, 1), 3) !$= "")
	{
		%Req4 = getWord(getField(%formula, 1), 3);
		%Req4 = nextToken(%Req4, "ReqAmt3", ":");
		%Req4 = nextToken(%Req4, "ReqType3", ":");
		%reqamt++;
	}

	for(%i = 0; %i < %reqamt; %i++)
	{
		%client.removeInventory(%ReqType[%i], %ReqAmt[%i], "put into the Processor", false);
	}
}

function fxDTSBrick::machineLoop(%brick)
{
	cancel(%brick.machineSchedule);

	if(%brick.machineProgress >= 100)
	{
		for(%i = 0; getWordCount(%brick.updateClients) > %i; %i++)
		{
			%client = getWord(%brick.updateClients, %i).getID();
			if(!isObject(%client))
				continue;

			commandtoclient(%client, 'CM_machineryDone');
		}

		%name = strReplace(strLwr(%brick.getName()), "_", "");
		if((%name !$= "") && ($CM::Formulas[%name] !$= ""))
			createDroppable("plastic", %brick.getPosition(), 1);

		%brick.machineProgress = 0;
		%brick.machineRate = 0;
		%brick.isMachineRunning = false;
	}
	else
	{
		%brick.machineProgress += %brick.machineRate;

		for(%i = 0; getWordCount(%brick.updateClients) > %i; %i++)
		{
			%subclient = getWord(%brick.updateClients, %i).getID();
			if(!isObject(%subclient))
				continue;

			commandtoclient(%subclient, 'CM_machineryUpdate', %brick.machineProgress / 100);
		}

		%brick.machineSchedule = %brick.schedule(1000, "machineLoop");
	}
}

// ============================================================
// Section 3 - Packages
// ============================================================

package CityMod_ResourceMachine
{
	function GameConnection::onDeath(%client, %sourceObject, %sourceClient, %damageType, %damageArea)
	{
		%return = Parent::onDeath(%client, %sourceObject, %sourceClient, %damageType, %damageArea);
		if(isObject(%client.player.currentMachinery))
		{
			%brick = %client.player.currentMachinery.getID();
			%brick.updateClients = removeWords(%brick.updateClients, %client.getID());
		}
		return %return;
	}
};
if(isPackage(CityMod_ResourceMachine))
	deactivatepackage(CityMod_ResourceMachine);
activatepackage(CityMod_ResourceMachine);