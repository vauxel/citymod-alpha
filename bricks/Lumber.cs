// ============================================================
// Project				-		CityMod
// Description		-		Tree Datablock
// ============================================================
// Sections
// 	 1: fxDTSBrick
// 	 2: Functions
// ============================================================

// ============================================================
// Section 1 - fxDTSBrick
// ============================================================
datablock fxDTSBrickData(brickCMTreeData : brickPineTreeData)
{
	category = "CityMod";
	subCategory = "Admin";
	
	CMBrick = true;
	CMBrickType = "RESOURCE";
	CMAdminBrick = true;
	
	uiName = "Tree";
	
	isTree = true;
	health = 100;
};

// ============================================================
// Section 2 - Functions
// ============================================================
package CityMod_Lumber
{
	function fxDTSBrick::chopTree(%this, %client, %level)
	{
		if(isObject(%this) && isObject(%client) && %this.getDatablock().isTree)
		{
			if(getWord(CMPlayerData.data[%client.bl_id].Value["Inventory"], 0) !$= $CM::Prefs::Player::InventoryLimit)
			{		
				if(mFloor(%this.health) > 0)
				{
					%this.health -= $CM::Prefs::Misc::MiningSpeed;
					
					if(mFloor(%this.health) <= 0)
					{
						%this.health = 0;
						%time = getRandom(10000, 30000);
						
						%this.fakekillbrick(1, mFloor(%time / 1000));
						
						createDroppable("wood", %this.getPosition(), getRandom(1,3));
						%this.schedule(%time, "regenTree");
					}
					else
						commandToClient(%client, 'centerPrint', "<font:Impact:24><color:526F35>Tree Health:<color:5A6351>" SPC %this.health @ "%", 1);
				}
			}
			else
				%client.CMNotify("Inventory", "Your inventory is full", "information");
		}
	}

	function fxDTSBrick::regenTree(%this)
	{
		if(isObject(%this))
		{
			%this.health = 100;
			%this.fakekillbrick(1, 1);
			%this.setEmitter(VehicleTireEmitter);
			%this.schedule(250, "setEmitter", "NONE");
		}
	}
};
if(isPackage(CityMod_Lumber))
	deactivatePackage(CityMod_Lumber);
activatePackage(CityMod_Lumber);