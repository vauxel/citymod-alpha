// ============================================================
// Project				-		CityMod
// Description			-		Farming Code
// ============================================================
// Sections
//   1: Datablocks
// 	 2: Functions
//	 3: Package
// ============================================================

// ============================================================
// Section 1 - Datablocks
// ============================================================

function CM_createCrop(%name, %totalStages, %spoilTime, %growthPace)
{
	%name = properText(%name);

	datablock fxDTSBrickData(CMDummyBrick1)
	{
		brickFile = "./BLBs/Crops/" @ %name @ "/" @ %name @ "1.blb";

		category = "CityMod";
		subCategory = "Farming";

		CMBrick = true;
		CMBrickType = "PLANT";
		CMAdminBrick = false;

		iconName = "";
		uiName = %name;

		isCrop = true;
		stage = 0;
		cropType = strLwr(%name);
		totalStages = %totalStages;
		spoilTime = %spoilTime;
		growPace = %growthPace;

		nextStage = "brickCM" @ %name @ "2CropData";
	};
	
	for(%i = 2; %i <= %totalStages; %i++)
	{
		datablock fxDTSBrickData(CMDummyBrick : CMDummyBrick1)
		{
			brickFile = "./BLBs/Crops/" @ %name @ "/" @ %name @ %i @ ".blb";
			uiName = "";
			stage = %i;
		};

		if((%i - 1) == %totalStages)
			CMDummyBrick.nextStage = "brickCM" @ %name @ %i @ "CropData";
		else
			CMDummyBrick.nextStage = "brickCM" @ %name @ (%i + 1) @ "CropData";

		CMDummyBrick.setName("brickCM" @ %name @ %i @ "CropData");
	}

	CMDummyBrick1.setName("brickCM" @ %name @ "1CropData");
}

datablock fxDTSBrickData(brickCM4xSoilData : brick4x4fData)
{
	category = "CityMod";
	subCategory = "Farming";

	CMBrick = true;
	CMBrickType = "SOIL";
	CMAdminBrick = false;

	staticColorID = 60;

	uiName = "Soil (Small)";
};

datablock fxDTSBrickData(brickCM8xSoilData : brick8x8fData)
{
	category = "CityMod";
	subCategory = "Farming";

	CMBrick = true;
	CMBrickType = "SOIL";
	CMAdminBrick = false;

	staticColorID = 60;
	
	uiName = "Soil (Medium)";
};

datablock fxDTSBrickData(brickCM16xSoilData : brick16x16fData)
{
	category = "CityMod";
	subCategory = "Farming";

	CMBrick = true;
	CMBrickType = "SOIL";
	CMAdminBrick = false;

	staticColorID = 60;
	
	uiName = "Soil (Large)";
};

if(!isObject(CMWaterCanItem))
{
	datablock ProjectileData(CMWaterCanProjectile)
	{
		projectileShapeName = "base/data/shapes/empty.dts";
	};

	datablock ItemData(CMWaterCanItem)
	{
		category = "Weapon";
		className = "Weapon";

		shapeFile = "Add-Ons/Gamemode_CityMod/CityMod/assets/shapes/WateringCan/WateringCan.dts";
		mass = 1;
		density = 0.2;
		elasticity = 0.2;
		friction = 0.6;
		emap = true;

		uiName = "Watering Can";
		iconName = "Add-Ons/Gamemode_CityMod/CityMod/assets/icons/WateringCan";
		iconName = "";
		doColorShift = true;
		colorShiftColor = "0.0 1.0 0.0 1";

		image = CMWaterCanImage;
		canDrop = false;
	};

	datablock ShapeBaseImageData(CMWaterCanImage : WrenchImage)
	{
		shapeFile = "Add-Ons/Gamemode_CityMod/CityMod/assets/shapes/WateringCan/WateringCan.dts";
		emap = true;

		mountPoint = 0;
		correctMuzzleVector = false;

		className = "WeaponImage";

		eyeOffset = "0.7 1.2 -0.5";

		item = CMWaterCanItem;
		ammo = " ";
		projectile = CMWaterCanProjectile;
		projectileType = Projectile;

		melee = true;
		doRetraction = false;
		armReady = true;

		doColorShift = true;
		colorShiftColor = CMWaterCanItem.colorShiftColor;

		showBricks = 0;
	};

	CM_createCrop("Wheat", 5, 3, 1);
	CM_createCrop("Corn", 5, 3, 1);
	CM_createCrop("Carrot", 3, 3, 1);
}
// ============================================================
// Section 2 - Functions
// ============================================================

function CMWaterCanImage::onFire(%this, %obj, %slot)
{
	Parent::onFire(%this, %obj, %slot);

	if(%obj.waterAmount $= "")
		%obj.waterAmount = 0;

	if(%obj.wateringCanCooldown $= "")
		%obj.wateringCanCooldown = $Sim::Time;

	if((%obj.wateringCanCooldown + 3) > $Sim::Time)
		return;

	%vector = vectorAdd(vectorScale(vectorNormalize(%obj.getEyeVector()), 2.5), %obj.getEyePoint());
	%brick = containerRayCast(%obj.getEyePoint(), %vector, $TypeMasks::FxBrickObjectType, %obj);

	if(!isObject(%brick))
		return;

	if(%obj.waterAmount <= 0)
	{
		commandToClient(%obj.client, 'centerPrint', "<font:Impact:21><color:555555>Your Watering Can does not contain any water!", 2);
		return;
	}

	if(!%brick.hasBeenWatered)
	{
		commandToClient(%obj.client, 'centerPrint', "<font:Impact:21><color:555555>This soil has already been watered!", 2);
		return;
	}

	%brick.growth += 1;
	%brick.setDatablock(%brick.getDatablock().nextStage);

	if(%brick.growth >= %brick.getDatablock().totalStages)
		commandToClient(%obj.client, 'centerPrint', "<font:Impact:21><color:FFFFFF>You watered the soil\n<font:Impact:20><color:555555>The plants on this soil can now be harvested!", 3);
	else
		commandToClient(%obj.client, 'centerPrint', "<font:Impact:21><color:FFFFFF>You watered the soil", 3);

	%brick.hasBeenWatered = true;
	for(%i = 0; %i < %brick.getNumUpBricks(); %i++)
	{
		%obj = %brick.getUpBrick(%i);

		if(%obj.getDatablock().CMBrickType $= "PLANT")
			%obj.needsWater = false;
	}

	%obj.wateringCanCooldown = $Sim::Time;
}

function fxDTSBrick::getPlantCount(%this)
{
	%count = 0;
	for(%i = 0; %i < %this.getNumUpBricks(); %i++)
	{
		%obj = %this.getUpBrick(%i);

		if(%obj.getDatablock().CMBrickType $= "PLANT")
			%count += 1;
	}

	return %count;
}

function fxDTSBrick::calculateQuality(%this)
{
	%soil = %this.getDownBrick(0);

	if(isObject(%soil) || (%soil.getDatablock().CMBrickType !$= "SOIL") || (%this.getDatablock().CMBrickType !$= "PLANT"))
		return -1;

	%plants = %soil.getPlantCount();

	return (100 - ((%plants - 1) * 5));
}

function Player::attemptHarvest(%player, %brick)
{
	if(!isObject(%player) || !isObject(%brick) || (%brick.getDatablock().CMBrickType !$= "PLANT"))
		return;

	if(%brick.brickGroup != %player.client.brickGroup)
	{
		commandToClient(%player.client, 'centerPrint', "<font:Impact:24><color:FF0000>You don't own that plant!", 3);
		return;
	}

	if(%player.client.isInventoryFull())
	{
		commandToClient(%player.client, 'centerPrint', "<font:Impact:24><color:FF0000>Your inventory is too full to harvest this plant!", 3);
		return;
	}

	%player.client.addInventory(%brick.getDatablock().CMPlantName, getRandom(1, 3), "harvested", false, "QUALITY=" @ %brick.calculateQuality());
	%brick.killBrick();
}

function fxDTSBrick::soilTick(%this)
{
	%this.hasBeenWatered = false;

	for(%i = 0; %i < %this.getNumUpBricks(); %i++)
	{
		%obj = %this.getUpBrick(%i);
		if(%obj.getDatablock().CMBrickType $= "PLANT")
		{
			if(%obj.needsWater)
				%obj.killBrick();
			else
				%obj.needsWater = true;
		}
	}
}

// ============================================================
// Section 3 - Package
// ============================================================

package CityMod_Farming
{
	function fxDTSBrick::onPlant(%this, %client)
	{
		%return = parent::onPlant(%this, %client);
		if(%plant.CMBrickType $= "SOIL")
		{
			//if(%client.getInventory("all", "soil") <= 0)
			//{
			//	commandToClient(%client, 'centerPrint', "<font:Impact:24><color:FF0000>You don't have any soil!",3);
			//	%this.schedule(0, "delete");
			//}
			//else
			//	%client.removeInventory("soil", 1, "placed", false, false);
		}
		else if(%this.getDatablock().CMBrickType $= "PLANT")
		{
			if(!isObject(%brick.getDownBrick(0)) || (%brick.getDownBrick(0).getDatablock().CMBrickType !$= "SOIL"))
			{
				commandToClient(%client, 'centerPrint', "<font:Impact:24><color:FF0000>Plants must be placed on soil!", 3);
				%this.schedule(0, "delete");
				return %return;
			}

			%seed = %plant.getDatablock().CMPlantName SPC "Seed";
			if(%client.getInventory("all", %seed) <= 0)
			{
				commandToClient(%client, 'centerPrint', "<font:Impact:24><color:FF0000>You don't have any" SPC %plant.getDatablock().CMPlantName SPC "seeds!",3);
				%this.schedule(0, "delete");
				return %return;
			}

			//%this.setDatblock(%this.getDatablock().nextStage);
			%this.growth = 0;
			%this.harvest = false;
			%client.removeInventory(%seed, 1, "planted", false, false);
		}

		return %return;
	}

	function fxDTSBrick::onActivate(%this, %player, %position, %rotation)
	{
		parent::onActivate(%this, %player, %position, %rotation);

		if(%this.getDatablock().CMBrick)
		{
			if(%this.getDatablock().CMBrickType $= "PLANT")
				%player.attemptHarvest(%this);
			else if(%this.getDatablock().CMBrickType $= "SOIL")
				commandToClient(%player.client, 'centerPrint', "<font:Impact:21><color:555555>This Soil has" SPC (%this.hasBeenWatered ? "already" : "not") SPC "been watered", 3);
		}
	}

	function CityMod_Day()
	{
		parent::CityMod_Day();
		for(%j = 0; %j < mainBrickGroup.getCount(); %j++)
		{
			%brickgroup = mainBrickGroup.getObject(%j);
			for(%i = 0; %i < %brickgroup.getCount(); %i++)
			{
				%brick = %brickgroup.getObject(%i);
				if(%brick.getDatablock().CMBrickType $= "SOIL")
					%brick.soilTick();
			}
		}
	}
};
if(isPackage(CityMod_Farming))
	deactivatepackage(CityMod_Farming);
activatepackage(CityMod_Farming);