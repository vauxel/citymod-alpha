// ============================================================
// Project				-		CityMod
// Description			-		Shopping Code
// ============================================================
// Sections
//   1: fxDTSBrick
// 	 2: Functions
//	 3: Package
// ============================================================

//NOTE: Utilizes formula functions found in Processing.cs

// ============================================================
// Section 1 - fxDTSBrick
// ============================================================

datablock fxDTSBrickData(brickCMCashRegisterData)
{
	brickFile = "./BLBs/CashRegister.blb";
	
	category = "CityMod";
	subCategory = "Shopkeeping";
	
	CMBrick = true;
	CMBrickType = "INTERACTIVE";
	CMAdminBrick = false;
	
	iconName = "Add-Ons/Gamemode_CityMod/CityMod/assets/icons/CashRegister";
	uiName = "Cash Register";
	
	orientationFix = 3;
	isCashRegister = true;

	foodPricing = "0 0 0 0 0 0";
};

datablock fxDTSBrickData(brickCMStorageZoneData : brick16x16FData)
{
	category = "CityMod";
	subCategory = "Shopkeeping";
	
	CMBrick = true;
	CMBrickType = "RESOURCE";
	CMAdminBrick = false;
	
	//iconName = "Add-Ons/Gamemode_CityMod/CityMod/assets/icons/Shelf";
	uiName = "Storage Zone";
	
	isStorageZone = true;
};

datablock fxDTSBrickData(brickCMShelfData)
{
	brickFile = "./BLBs/Shelves/Shelf_0.blb";
	
	category = "CityMod";
	subCategory = "Shopkeeping";
	
	CMBrick = true;
	CMBrickType = "INTERACTIVE";
	CMAdminBrick = false;
	
	iconName = "Add-Ons/Gamemode_CityMod/CityMod/assets/icons/Shelf";
	uiName = "Shelf";
	
	orientationFix = 3;
	isShelf = true;
	stage = 0;

	contents = "";
};

datablock fxDTSBrickData(brickCM1ThirdShelfData : brickCMShelfData)
{
	brickFile = "./BLBs/Shelves/Shelf_1.blb";

	category = "";
	subCategory = "";
	uiName = "Shelf1Third";
	
	stage = 1;
};

datablock fxDTSBrickData(brickCM2ThirdShelfData : brickCMShelfData)
{
	brickFile = "./BLBs/Shelves/Shelf_2.blb";

	category = "";
	subCategory = "";
	uiName = "Shelf2Third";
	
	stage = 2;
};

datablock fxDTSBrickData(brickCMFullShelfData : brickCMShelfData)
{
	brickFile = "./BLBs/Shelves/Shelf_3.blb";

	category = "";
	subCategory = "";
	uiName = "ShelfFull";
	
	stage = 3;
};

// ============================================================
// Section 2 - Functions
// ============================================================

function fxDTSBrick::adjustShelfDatablock(%this)
{
	if(!%this.getDatablock().isShelf || (%this.dataBlock !$= "brickCMShelfData" && %this.dataBlock !$= "brickCM1ThirdShelfData" && %this.dataBlock !$= "brickCM2ThirdShelfData" && %this.dataBlock !$= "brickCMFullShelfData"))
		return error("fxDTSBrick::adjustShelfDatablock - Brick isn't a Shelf Brick!");

	%contents = getWordCount(%this.contents);

	if(%contents == 0)
		%model = "brickCMShelfData";
	else if(%contents == 1 || %contents == 2)
		%model = "brickCM1ThirdShelfData";
	else if(%contents == 3 || %contents == 4)
		%model = "brickCM2ThirdShelfData";
	else if(%contents == 5 || %contents == 6)
		%model = "brickCMFullShelfData";
	else
		%model = "brickCMFullShelfData";

	%this.setDataBlock(%model);
}

function fxDTSBrick::updateStorageZone(%this)
{
	if(%this.dataBlock !$= "brickCMStorageZoneData")
		return error("fxDTSBrick::updateStorageZone - Brick isn't a Storage Zone!");

	if(mFloor(getWord(%this.rotation, 3)) == 90)
		%boxSize = (%this.getDataBlock().bricksizeY / 2) SPC (%this.getDataBlock().bricksizeX / 2) SPC (16);
	else
		%boxSize = (%this.getDataBlock().bricksizeX / 2) SPC (%this.getDataBlock().bricksizeY / 2) SPC (16);
				
	initContainerBoxSearch(%this.getWorldBoxCenter(), %boxSize, $TypeMasks::FxBrickObjectType);
				
	while(isObject(%found = containerSearchNext()))
	{
		if((%found.getDatablock() == CMLotTrigger.getID()) || !%found.getDatablock().CMBrick || (%found.getDatablock().CMBrick && %found.getDatablock().CMBrickType !$= "CRATE"))
			continue;

		%type = %found.getDatablock().type;

		if(%hasBeenCleared[%type] $= "")
		{
			%this.stored[%type] = 0;
			%hasBeenCleared[%type] = true;
		}

		%this.stored[%type] += $CM::Prefs::Default::CrateQuantity;
	}
}

function fxDTSBrick::getNextCrateType(%this, %type)
{
	if(%this.dataBlock !$= "brickCMStorageZoneData")
		return error("fxDTSBrick::getNextCrateType - Brick isn't a Storage Zone!");

	if(mFloor(getWord(%this.rotation, 3)) == 90)
		%boxSize = (%this.getDataBlock().bricksizeY / 2) SPC (%this.getDataBlock().bricksizeX / 2) SPC (16);
	else
		%boxSize = (%this.getDataBlock().bricksizeX / 2) SPC (%this.getDataBlock().bricksizeY / 2) SPC (16);
				
	initContainerBoxSearch(%this.getWorldBoxCenter(), %boxSize, $TypeMasks::FxBrickObjectType);
				
	while(isObject(%found = containerSearchNext()))
	{
		if((%found.getDatablock() == CMLotTrigger.getID()) || !%found.getDatablock().CMBrick || (%found.getDatablock().CMBrick && %found.getDatablock().CMBrickType !$= "CRATE") || !isObject(%found.getUpBrick(0)))
			continue;

		if(strLwr(%type) $= %found.getDatablock().type)
			return %found;
	}

	return -1;
}

// ============================================================
// Section 3 - Packages
// ============================================================

package CityMod_Shopping
{
	function gameConnection::hasStoreFoodInInventory(%client)
	{
		for(%i = 1; %i <= 6; %i++)
		{
			if(strUpr(firstWord(parseTokenString(firstWord(parseTokenString(getWord(CMPlayerData.data[%client.bl_id].Value["Inventory"], %i - 1), ":")), "-"))) $= "FOOD")
				return true;
		}

		return false;
	}

	function gameConnection::hasStoreGunsInInventory(%client)
	{
		for(%i = 1; %i <= 6; %i++)
		{
			if(strUpr(firstWord(parseTokenString(firstWord(parseTokenString(getWord(CMPlayerData.data[%client.bl_id].Value["Inventory"], %i - 1), ":")), "-"))) $= "GUN")
				return true;
		}

		return false;
	}

	function gameConnection::eatFood(%client, %foodType, %quality)
	{
		
	}

	function resolveFoodIDFromName(%name)
	{
		for(%a = 1; %a <= $CM::Prefs::Default::MaxFoodQuantities; %a++)
		{
			if($CM::Prefs::Default::FoodQuantities[%a] $= %name)
				return %a;
		}

		return "-1";
	}

	//Takes Food out from the Shelf (Left-Click)
	function fxDTSBrick::onActivate(%this, %player, %position, %rotation)
	{
		%r = parent::onActivate(%this, %player, %position, %rotation);
		if(isObject(%this))
		{
			%client = %player.client;

			if(%this.getDatablock().isShelf)
			{
				if(%client.isInventoryFull())
					return %client.CMNotify("Shopping", "Your Inventory can't hold any more food!");

				if(getWordCount(%this.contents) == 0)
					return %client.CMNotify("Shopping", "This Shelf doesn't contain any more food!");

				%food = parseTokenString(getWord(%this.contents, 0));
				%client.addInventory(getWord(%food, 0), 1, "took", true, "NOSTACK=1 QUALITY=" @ getWord(%food, 1));

				%this.contents = removeWord(%this.contents, 0);
				%this.adjustShelfDatablock();

				%foodname = properText(getWord(parseTokenString(firstWord(%food), "-"), 1));
				%client.CMNotify("Shopping", "You took some food," SPC vowelCheck(%foodname) SPC %foodname @ ", from the Shelf");
			}
			else if(%this.getDatablock().isCashRegister)
			{
				if(!%client.hasStoreFoodInInventory() && !%client.hasStoreGunsInInventory())
					return commandtoclient(%client, 'CM_pushNotifyBox', "Cash Register", "You don't have anything in your Shopping Cart to buy!");

				%player.currentCashRegister = %this;
				commandtoClient(%client, 'CM_openGUI', "CASHREGISTER");
			}
		}
		return %r;
	}

	//Puts Food onto the Shelf (Right-Click)
	function Armor::onTrigger(%this, %player, %slot, %val)
	{
		%return = parent::onTrigger(%this, %player, %slot, %val);
		if((%slot == 4) && (%val == 1))
		{
			%vector = vectorAdd(vectorScale(vectorNormalize(%player.getEyeVector()), 2.5), %player.getEyePoint());
			%brick = containerRayCast(%player.getEyePoint(), %vector, $TypeMasks::FxBrickObjectType, %player);
			%client = %player.client;

			if(isObject(%brick) && %brick.getDatablock().isShelf)
			{
				if(!%client.hasStoreFoodInInventory())
					return %client.CMNotify("Shopping", "You don't have any Food in your Inventory!");

				if(getWordCount(%brick.contents) == 6)
					return %client.CMNotify("Shopping", "This Food Shelf is too full to hold any more food!");

				%foundslot = "";
				for(%b = 1; %b <= 6; %b++)
				{
					%food = parseTokenString(getWord(CMPlayerData.data[%client.bl_id].Value["Inventory"], %b - 1), ":");

					if(getWord(parseTokenString(firstWord(%food), "-"), 0) $= "FOOD")
					{
						%foundslot = %b;
						break;
					}
				}

				if(%foundslot $= "")
					return;

				%foodtype = getWord(parseTokenString(firstWord(%food), "-"), 1);

				talk("SLOT:" SPC %foundslot);
				talk("FOOD: [" @ %food @ "]");
				talk("FOODTYPE:" SPC %foodtype);
				talk("QUALITY:" SPC getInventoryPropertyValue(getWord(%food, 2), "QUALITY"));

				%brick.contents = ltrim(%brick.contents SPC (getWord(%food, 0) @ ":" @ getInventoryPropertyValue(getWord(%food, 2), "QUALITY")));
				%client.removeInventorySlot(%foundslot, "put on the Shelf", false, true);

				%client.CMNotify("Shopping", "You stocked some food," SPC vowelCheck(%foodtype) SPC properText(%foodtype) @ ", onto Shelf");
				%brick.adjustShelfDatablock();
			}
		}
		return %return;
	}
};
if(isPackage(CityMod_Shopping))
	deactivatepackage(CityMod_Shopping);
activatepackage(CityMod_Shopping);