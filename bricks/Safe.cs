// ============================================================
// Project				-		CityMod
// Description			-		Safes Datablock
// ============================================================
// Sections
// 	 1: fxDTSBrick
// 	 2: Functions
// ============================================================

// ============================================================
// Section 1 - fxDTSBrick
// ============================================================
datablock fxDTSBrickData(brickCMSafeData)
{
	brickFile = "./BLBs/Safes/Safe_Closed.blb";
	category = "CityMod";
	subCategory = "Storage";
	uiName = "Safe";
	isDoor = 1;
	isOpen = 0;
	
	closedCW = "brickCMSafeData";
	openCW = "brickCMSafeOpenData";
	
	closedCCW = "brickCMSafeData";
	openCCW = "brickCMSafeOpenData";
	
	orientationFix = 3;
};

datablock fxDTSBrickData(brickCMWallSafeData : brickCMSafeData)
{
	brickFile = "./BLBs/Safes/Safe_Wall_Closed.blb";
	uiName = "Wall Safe";
	
	closedCW = "brickCMWallSafeData";
	openCW = "brickCMWallSafeOpenData";
	
	closedCCW = "brickCMWallSafeData";
	openCCW = "brickCMWallSafeOpenData";
};

datablock fxDTSBrickData(brickCMSafeOpenData : brickCMSafeData)
{
	brickFile = "./BLBs/Safes/Safe_Open.blb";
	uiName = "";
	isOpen = 1;
};

datablock fxDTSBrickData(brickCMWallSafeOpenData : brickCMWallSafeData)
{
	brickFile = "./BLBs/Safes/Safe_Wall_Open.blb";
	uiName = "";
	isOpen = 1;
};

// ============================================================
// Section 2 - Functions
// ============================================================
//package CityMod_Safes
//{
//	//To-Do
//};
//if(isPackage(CityMod_Safes))
//	deactivatePackage(CityMod_Safes);
//activatePackage(CityMod_Safes);