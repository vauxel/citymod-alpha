// ============================================================
// Project				-		CityMod
// Description			-		Handles Datablocks and Bricks
// ============================================================
// Sections
// 	 1: PlayerTypes
//   2: Audio/Sounds
// 	 3: fxDTSBricks
//   4: Triggers
//   5: Droppables
//   6: Food Items
//   7: Miscellaneous
// ============================================================

// ============================================================
// Section 1 - PlayerTypes
// ============================================================
datablock PlayerData(PlayerCM : PlayerStandardArmor)
{
	canJet = 0;

	uiName = "CityMod Player";
	showEnergyBar = 1;
	maxTools = 5;
	maxWeapons = 5;
	rechargeRate = 0;
};

datablock WheeledVehicleData(CorpseVehicle)
{
	shapeFile = "Add-Ons/Item_Skis/deathVehicle.dts";
	emap = true;

	isSled = true;
	maxDamage = 1;
	destroyedLevel = 1;
	maxSteeringAngle = 0.885;

	mass = 300;
	density = 0.5;

	massCenter = "0.0 -0.5 1.25";
	massBox = "1.25 1.25 2.65";

	drag = 0.6;
	bodyFriction = 1;
	bodyRestitution = 0.2;
	minImpactSpeed = 0;
	softImpactSpeed = 0;
	hardImpactSpeed = 0;
	integration = 4;
	collisionTol = 0.25;
	contactTol = 0.1;
	justcollided = 0;

	engineTorque = 0;
	engineBrake = 0;
	brakeTorque = 0;
	maxWheelSpeed = 0;

	forwardThrust = 0;
	reverseThrust = 0;
	lift = 0;
	maxForwardVel = 30;
	maxReverseVel = 30;
	horizontalSurfaceForce = 0;
	verticalSurfaceForce = 0; 
	rollForce = 0;
	yawForce = 0;
	pitchForce = 0;
	rotationalDrag = 5;
	stallSpeed = 0;

	runOverDamageScale = 0;
	runOverPushScale = 0;
};

// ============================================================
// Section 2 - Audio/Sounds
// ============================================================

datablock AudioProfile(CMS_Looting)
{
	filename = "./assets/sounds/looting.wav";
	description = AudioClose3d;
	preload = true;
};

datablock AudioProfile(CMS_AxeHit)
{
	filename = "./assets/sounds/axehit.wav";
	description = AudioClose3d;
	preload = true;
};

datablock AudioProfile(CMS_PickHit)
{
	filename = "./assets/sounds/rockhit3.wav";
	description = AudioClose3d;
	preload = true;
};

datablock AudioProfile(CMS_Activate)
{
	filename = "./assets/sounds/activate1.wav";
	description = AudioClose3d;
	preload = true;
};

datablock AudioProfile(CMS_Discovery)
{
	filename = "./assets/sounds/discovery.wav";
	description = AudioClose3d;
	preload = true;
};

datablock AudioProfile(CMS_ImpactSoft)
{
	fileName = "./assets/sounds/impactSoft.wav";
	description = AudioClose3d;
	preload = true;
};

datablock AudioProfile(CMS_ImpactHard)
{
	fileName = "./assets/sounds/impactHard.wav";
	description = AudioClose3d;
	preload = true;
};

// ============================================================
// Section 3 - fxDTSBricks
// ============================================================

//Lot Bricks
datablock fxDTSBrickData(brickCM32xResidentialLot : brick32x32FData)
{
	category = "CityMod";
	subCategory = "Admin";
	
	uiName = "Res. Lot";
	
	CMBrick = true;
	CMBrickType = "LOT";
	CMAdminBrick = true;
	
	lotType = "Residential";
};

datablock fxDTSBrickData(brickCM32xCommercialLot : brick32x32FData)
{
	category = "CityMod";
	subCategory = "Admin";
	
	uiName = "Com. Lot";
	
	CMBrick = true;
	CMBrickType = "LOT";
	CMAdminBrick = true;
	
	lotType = "Commercial";
};

datablock fxDTSBrickData(brickCM32xIndustrialLot : brick32x32FData)
{
	category = "CityMod";
	subCategory = "Admin";
	
	uiName = "Ind. Lot";
	
	CMBrick = true;
	CMBrickType = "LOT";
	CMAdminBrick = true;
	
	lotType = "Industrial";
};

datablock fxDTSBrickData(brickCM32xJailLot : brick32x32FData)
{
	category = "CityMod";
	subCategory = "Admin";
	
	uiName = "Jail. Lot";
	
	CMBrick = true;
	CMBrickType = "LOT";
	CMAdminBrick = true;
	
	lotType = "Jail";
};

//Spawns
datablock fxDTSBrickData(brickCMJailSpawnData : brickSpawnPointData)
{
	category = "CityMod";
	subCategory = "Admin";

	uiName = "Jail Spawn";

	CMBrick = true;
	CMBrickType = "SPAWN";
	CMAdminBrick = true;

	spawnType = "Jail";
};

datablock fxDTSBrickData(brickCMHomelessSpawnData : brickSpawnPointData)
{
	category = "CityMod";
	subCategory = "Admin";

	uiName = "Homeless Spawn";

	CMBrick = true;
	CMBrickType = "SPAWN";
	CMAdminBrick = true;

	spawnType = "Homeless";
};

datablock fxDTSBrickData(brickCMHomeSpawnData : brickSpawnPointData)
{
	category = "CityMod";
	subCategory = "Home";

	uiName = "Home Spawn";

	CMBrick = true;
	CMBrickType = "SPAWN";
	CMAdminBrick = false;

	spawnType = "Home";
};

//Misc Bricks
datablock fxDTSBrickData(brickCMKioskData)
{
	brickFile = "./bricks/BLBs/Kiosk.blb";
	category = "CityMod";
	subCategory = "Admin";
	iconName = "Add-Ons/Gamemode_CityMod/CityMod/assets/icons/Kiosk";
	uiName = "Kiosk";

	CMBrick = true;
	CMBrickType = "INTERACTIVE";
	CMAdminBrick = true;

	orientationFix = 3;
	isKiosk = true;
};

datablock fxDTSBrickData(brickCMBallotBoxData)
{
	brickFile = "./bricks/BLBs/BallotBox.blb";
	category = "CityMod";
	subCategory = "Admin";
	iconName = "Add-Ons/Gamemode_CityMod/CityMod/assets/icons/BallotBox";
	uiName = "Ballot Box";

	CMBrick = true;
	CMBrickType = "INTERACTIVE";
	CMAdminBrick = true;

	orientationFix = 1;
	isBallotBox = true;
};

execFolder("./bricks/");

// ============================================================
// Section 4 - Triggers
// ============================================================

datablock TriggerData(CMLotTrigger)
{
	tickPeriodMS = 500;
};

datablock TriggerData(CMSchoolTrigger)
{
	tickPeriodMS = 2000;
};

datablock TriggerData(CMJailTrigger)
{
	tickPeriodMS = 5000;
};

// ============================================================
// Section 5 - Droppables
// ============================================================

//Cash Droppable
datablock ItemData(cashItem)
{
	category = "Weapon";
	className = "Weapon";
	
	shapeFile = "./assets/shapes/Cash.dts";
	mass = 1;
	density = 0.2;
	elasticity = 0.2;
	friction = 0.5;
	emap = true;
	
	doColorShift = false;
	colorShiftColor = "0 0 0 0";
	image = cashImage;
	candrop = true;
	canPickup = true;
};
datablock ShapeBaseImageData(cashImage)
{
	shapeFile = "./assets/shapes/Cash.dts";
};
function cashItem::onPickup(%this,%obj,%player)
{
	if(isObject(%player.client))
	{
		CCMPlayerData.data[%player.client.bl_id].Value["Money"] += %obj.value;
		%player.client.CMNotify("Droppable", "You picked up $" @ %obj.value, "money");
		%obj.delete();
	}
}

//Wood Droppable
datablock ItemData(lumberItem)
{
	category = "Weapon";
	className = "Weapon";
	
	shapeFile = "./assets/shapes/Wood.dts";
	mass = 1;
	density = 0.2;
	elasticity = 0.2;
	friction = 0.5;
	emap = true;
	
	doColorShift = false;
	colorShiftColor = "0 0 0 0";
	image = lumberImage;
	candrop = true;
	canPickup = true;
};
datablock ShapeBaseImageData(lumberImage)
{
	shapeFile = "./assets/shapes/Wood.dts";
};
function lumberItem::onPickup(%this,%obj,%player)
{
	if(isObject(%player.client))
	{
		%player.client.addInventory("Wood", %obj.value, "picked up");
		%obj.delete();
	}
}

//Metal Droppable
datablock ItemData(metalItem)
{
	category = "Weapon";
	className = "Weapon";
	
	shapeFile = "./assets/shapes/RawOre.dts";
	mass = 1;
	density = 0.2;
	elasticity = 0.2;
	friction = 0.5;
	emap = true;
	
	doColorShift = true;
	colorShiftColor = "1.500 1.500 1.500 1.000";
	image = metalImage;
	candrop = true;
	canPickup = true;
};
datablock ShapeBaseImageData(metalImage)
{
	shapeFile = "./assets/shapes/RawOre.dts";
};
function metalItem::onPickup(%this,%obj,%player)
{
	if(isObject(%player.client))
	{
		%player.client.addInventory("Metal", %obj.value, "picked up");
		%obj.delete();
	}
}

//Coal Droppable
datablock ItemData(coalItem)
{
	category = "Weapon";
	className = "Weapon";
	
	shapeFile = "./assets/shapes/RawOre.dts";
	mass = 1;
	density = 0.2;
	elasticity = 0.2;
	friction = 0.5;
	emap = true;
	
	doColorShift = true;
	colorShiftColor = "0.500 0.500 0.500 1.000";
	image = coalImage;
	candrop = true;
	canPickup = true;
};
datablock ShapeBaseImageData(coalImage)
{
	shapeFile = "./assets/shapes/RawOre.dts";
};
function coalItem::onPickup(%this,%obj,%player)
{
	if(isObject(%player.client))
	{
		%player.client.addInventory("Coal", %obj.value, "picked up");
		%obj.delete();
	}
}

//Refined Metal Droppable
datablock ItemData(rmetalItem)
{
	category = "Weapon";
	className = "Weapon";
	
	shapeFile = "./assets/shapes/RawOre.dts";
	mass = 1;
	density = 0.2;
	elasticity = 0.2;
	friction = 0.5;
	emap = true;
	
	doColorShift = true;
	colorShiftColor = "0.500 0.500 0.500 1.000";
	image = coalImage;
	candrop = true;
	canPickup = true;
};
datablock ShapeBaseImageData(rmetalImage)
{
	shapeFile = "./assets/shapes/RawOre.dts";
};
function rmetalItem::onPickup(%this,%obj,%player)
{
	if(isObject(%player.client))
	{
		%player.client.addInventory("RefinedMetal", %obj.value, "picked up");
		%obj.delete();
	}
}

//Plastic Droppable
datablock ItemData(plasticItem)
{
	category = "Weapon";
	className = "Weapon";
	
	shapeFile = "./assets/shapes/RawOre.dts";
	mass = 1;
	density = 0.2;
	elasticity = 0.2;
	friction = 0.5;
	emap = true;
	
	doColorShift = true;
	colorShiftColor = "0.500 0.500 0.500 1.000";
	image = plasticImage;
	candrop = true;
	canPickup = true;
};
datablock ShapeBaseImageData(plasticImage)
{
	shapeFile = "./assets/shapes/RawOre.dts";
};
function plasticItem::onPickup(%this,%obj,%player)
{
	if(isObject(%player.client))
	{
		%player.client.addInventory("Plastic", %obj.value, "picked up");
		%obj.delete();
	}
}

// ============================================================
// Section 6 - Food Items
// ============================================================

datablock ItemData(CMTinyFood1Item)
{
	category = "Weapon";
	className = "Weapon";

	shapeFile = "./assets/shapes/FoodItems/Apple/Apple.dts";
	rotate = false;
	mass = 1;
	density = 0.2;
	elasticity = 0.2;
	friction = 0.6;
	emap = true;

	uiName = "Apple";
	iconName = "";
	doColorShift = false;

	image = CMTinyFood1Image;
	canDrop = true;
};

datablock ShapeBaseImageData(CMTinyFood1Image)
{
	shapeFile = CMTinyFood1Item.shapeFile;
	emap = true;

	mountPoint = 0;
	offset = "0 0.1 0.05";
	eyeOffset = 0;
	rotation = eulerToMatrix("0 0 0");

	className = "WeaponImage";
	item = CMTinyFood1Item;

	armReady = true;
	doColorShift = false;

	projectile = "";

	stateName[0] 										= "Ready";
	stateTransitionOnTriggerDown[0] = "Effects";
	stateAllowImageChange[0] 				= true;

	stateName[1] 										= "Effects";
	stateTimeoutValue[1] 						= 0.5;
	stateTransitionOnTimeout[1] 		= "Fire";
	stateAllowImageChange[1] 				= false;

	stateName[2] 										= "Fire";
	stateTransitionOnTimeout[2] 		= "Ready";
	stateAllowImageChange[2] 				= true;
	stateScript[2] 									= "onFire";
	stateTimeoutValue[2] 						= 1;

	foodType = "Tiny";
};
function CMTinyFood1Image::onFire(%this, %obj, %slot){ %obj.client.eatFood(%this.foodType); }

// ============================================================
// Section 7 - Miscellaneous
// ============================================================

datablock StaticShapeData(clueShape)
{
	shapeFile = "./assets/shapes/Exclamation.dts";
};

datablock StaticShapeData(ropeShape)
{
	shapeFile = "./assets/shapes/Cylinder.dts";
};